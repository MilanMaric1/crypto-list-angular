# Crypto prices


*Staging environment*: https://crypto-list-staging.web.app/


*Production environment*:  https://crypto-list-prod.firebaseapp.com/



Generated using @angular/cli

External libraries:
- NgRx
- AntDesign (ng-zorro-antd)
- Jasmine
- Karma
- Protractor
- puppeteer
- public CoinMarketCap API 


This project uses gitlab-runner as CI/CD tool
- master branch is deployed to production
- develop branch is deployed to staging
- merge requests are being tested 


## Workarounds 
- public CoinMarketCap API  prevents creating requests from browser by removing CORS headers. Therefore Google cloud function has been created which will proxy the requests to CoinMarketCap API.

- end to end tests are missing

- Gitlab repository is located at https://gitlab.com/MilanMaric1/crypto-list-angular


## Required functionality
 
###Cryptocurrency List
The cryptocurrency list should have the top 100 cryptocurrencies
The list should have a way to refresh (button)
Each item in the list should have the following info:
- rank
- symbol
- price in the selected fiat currency
- 24 hour change

If you click on the item, you are shown the cryptocurrency details
###Cryptocurrency Details
This screen should have a bit more info about the selected cryptocurrency:
- rank
- name
- symbol
- price, 24h volume and market cap in the selected fiat currency
- price in bitcoin
- 1h change, 24h change, 7d change
- total and available supply

This screen should have a button to refresh
###Settings
This screen is accessible through an icon in the toolbar that is present on all other screens
User can select one of the following fiat currencies:
- USD
- EUR
- CNY

When the user goes back, if the selected fiat currency was changed, update the results on the previous screen
Tests


Write unit tests (you do not need to test everything, choose a few components and test them)
