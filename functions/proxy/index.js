const rp = require('request-promise');

const apiKey = 'fd6ba74b-d52a-4ba2-81e4-187539424bb9';
const apiHost = 'https://pro-api.coinmarketcap.com';

const performCoinMarketRequest = (path, params) => {
  const requestOptions = {
    method: 'GET',
    uri: `${apiHost}${path}`,
    qs: params,
    headers: {
      'X-CMC_PRO_API_KEY': apiKey
    },
    json: true,
    gzip: true
  };

  return rp(requestOptions)
    .then(response => {
      return response;
    })
};


const proxy = (req, res) => {
  res.set('Access-Control-Allow-Origin', "*");
  res.set('Access-Control-Allow-Methods', 'GET, POST');
  res.set('Access-Control-Allow-Headers', '*');

  if (req.method === 'OPTIONS') {
    console.log('Handling OPTIONS request');
    // Send response to OPTIONS requests
    res.set('Access-Control-Max-Age', '3600');
    res.status(204).send('');
    return;
  }
  console.log('handling', req.method);
  const body = req.body || req;
  const {path, params} = body;
  console.log('received body', body);
  console.log('performing request', path, params);
  if (!path) {
    res.status(400)
      .json({
        message: 'Path is required'
      });
    return;
  }
  return performCoinMarketRequest(path, params)
    .then((response) => {
      res.json(response);
      return response;
    })
    .catch(error => {
      console.error(error);
      res.status(400).json(error);
    })
};

exports.proxy = proxy;

