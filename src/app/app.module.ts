import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {StoreModule} from '@ngrx/store';
import {reducers, metaReducers} from './reducers';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../environments/environment';
import {EffectsModule} from '@ngrx/effects';
import {HttpClientModule} from '@angular/common/http';
import {NgZorroAntdModule, NZ_I18N, en_US} from 'ng-zorro-antd';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {registerLocaleData} from '@angular/common';
import en from '@angular/common/locales/en';
import {CryptoSearchModule} from './crypto-search/crypto-search.module';
import {RouterStateSerializer, StoreRouterConnectingModule} from '@ngrx/router-store';
import {CustomSerializer} from './router-state.serializer';
import {SettingsComponent} from './components/settings/settings.component';
import {SettingsContainerPageComponent} from './containers/settings-container-page/settings-container-page.component';
import {CryptoRankEffects} from './effects/crypto-rank.effects';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    SettingsComponent,
    SettingsContainerPageComponent
  ],
  imports: [
    BrowserModule,
    CryptoSearchModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot(reducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
      }
    }),
    StoreRouterConnectingModule.forRoot(),
    EffectsModule.forRoot([CryptoRankEffects]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    NgZorroAntdModule,
    FormsModule,
    BrowserAnimationsModule
  ],
  providers: [
    {
      provide: NZ_I18N, useValue: en_US
    },
    {
      provide: RouterStateSerializer, useClass: CustomSerializer
    }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
