import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CryptoRankingPageComponent} from './containers/crypto-ranking-page.component';
import {CryptoDetailsPageComponent} from './containers/crypto-details-page.component';


const routes: Routes = [
  {path: '', component: CryptoRankingPageComponent},
  {path: 'crypto/:id', component: CryptoDetailsPageComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CryptoRoutingModule {
}
