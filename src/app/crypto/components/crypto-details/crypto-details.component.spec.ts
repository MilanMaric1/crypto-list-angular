import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CryptoDetailsComponent} from './crypto-details.component';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {NzIconTestModule} from 'ng-zorro-antd/icon/testing';
import {HttpClientModule} from '@angular/common/http';
import {MockData} from '../../../core/mock-data';

describe('CryptoDetailsComponent', () => {
  let component: CryptoDetailsComponent;
  let fixture: ComponentFixture<CryptoDetailsComponent>;
  let compiled;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgZorroAntdModule, NzIconTestModule, HttpClientModule],
      declarations: [CryptoDetailsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CryptoDetailsComponent);
    component = fixture.componentInstance;
    compiled = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    component.item = MockData.cyptoOneItemWithBitcoin;
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(compiled.querySelector('nz-descriptions')).toBeTruthy();
  });

  it('should contain price in bitcoiin ', () => {
    component.item = MockData.cyptoOneItemWithBitcoin;
    fixture.detectChanges();
    const bitcoinPriceRef = compiled.querySelector('.bitcoin_price');


    expect(bitcoinPriceRef)
      .toBeTruthy('bitcoinPriceRef falsy');
    expect(bitcoinPriceRef.textContent)
      .toContain('BTC');
  });

  it('should not contain price in bitcoin', () => {
    component.item = MockData.cryptoItemWithoutBitcoin;
    fixture.detectChanges();
    const bitcoinPriceRef = compiled.querySelector('.bitcoin_price');


    expect(bitcoinPriceRef)
      .toBeFalsy('bitcoinPriceRef is not falsy');
  });

});
