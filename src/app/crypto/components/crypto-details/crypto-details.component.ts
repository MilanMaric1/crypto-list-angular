import {Component, Input} from '@angular/core';
import {CryptoItem} from '../../../models/CryptoItem';
import {FiatCurrency} from '../../../models/FiatCurrency';

@Component({
  selector: 'app-crypto-details',
  templateUrl: './crypto-details.component.html',
  styleUrls: ['./crypto-details.component.less']
})
export class CryptoDetailsComponent {

  @Input()
  item: CryptoItem;

  @Input()
  loading: boolean;

  @Input()
  currency: FiatCurrency = FiatCurrency.USD;
}
