import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CryptoListComponent} from './crypto-list.component';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {cryptoRankListData} from '../../../core/mock-data';
import {NzIconTestModule} from 'ng-zorro-antd/icon/testing';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';

describe('CryptoListComponent', () => {
  let component: CryptoListComponent;
  let fixture: ComponentFixture<CryptoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgZorroAntdModule, NzIconTestModule, HttpClientModule, RouterTestingModule],
      declarations: [CryptoListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CryptoListComponent);
    component = fixture.componentInstance;
    fixture.whenStable()
      .then(() => {
        component.list = cryptoRankListData.data;
        fixture.detectChanges();
      });
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render list', () => {
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('nz-list')).toBeTruthy();
  });
});
