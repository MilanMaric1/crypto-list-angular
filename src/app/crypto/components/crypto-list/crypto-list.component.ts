import {Component, Input} from '@angular/core';
import {CryptoItem} from '../../../models/CryptoItem';
import {FiatCurrency} from '../../../models/FiatCurrency';

@Component({
  selector: 'app-crypto-list',
  templateUrl: './crypto-list.component.html',
  styleUrls: ['./crypto-list.component.less']
})
export class CryptoListComponent {

  @Input()
  list: CryptoItem[];
  @Input()
  loading: boolean;
  @Input()
  error: string;

  @Input()
  currency: FiatCurrency = FiatCurrency.USD;
}
