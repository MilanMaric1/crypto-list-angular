import {createAction, union, props} from '@ngrx/store';
import {CryptoItem} from '../../models/CryptoItem';

export const searchSuccess = createAction(
  '[CryptoItems/API] Search Success',
  props<{ items: CryptoItem[] }>()
);

export const searchFailure = createAction(
  '[CryptoItems/API] Search Failure',
  props<{ errorMsg: string }>()
);

export const rankListSuccess = createAction(
  '[CryptoItems/API] rank list Success',
  props<{ items: CryptoItem[] }>()
);

export const rankListError = createAction(
  '[CryptoItems/API] rank list Failure',
  props<{ errorMsg: string }>()
);

export const detailFetchSuccess = createAction(
  '[CryptoItems/API] details Success',
  props<{ item: CryptoItem }>()
);

export const detailFetchError = createAction(
  '[CryptoItems/API] details Failure',
  props<{ errorMsg: string }>()
);


const all = union({searchSuccess, searchFailure, rankListSuccess, rankListError, detailFetchSuccess, detailFetchError});
export type CryptoApiActionsUnion = typeof all;
