import {createAction, props, union} from '@ngrx/store';


export const getOneCrypto = createAction(
  '[Crypto Details Page] get crypto details',
  props<{
    id: string
  }>()
);

export const pageNavigated = createAction(
  '[Crypto Details Page] page navigated',
  props<{
    id: string
  }>()
);

export const noop = createAction(
  '[Crypto Details Page] noop',
  props<{}>()
);

const all = union({pageNavigated, getOneCrypto, noop});
export type CryptoDetailsActions = typeof all;
