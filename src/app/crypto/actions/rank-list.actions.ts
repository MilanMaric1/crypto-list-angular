import {createAction, props} from '@ngrx/store';
import {CryptoItem} from '../../models/CryptoItem';

export const listCrypto = createAction(
  '[CryptoItems List Page] list cryptocurrencies',
  props<{
    convert: string
  }>()
);


export type CryptoRankListActions = ReturnType<typeof listCrypto>;
