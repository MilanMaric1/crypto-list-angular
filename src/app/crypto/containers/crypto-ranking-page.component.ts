import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';

import * as fromCrypto from '../../crypto/reducers';
import * as fromRoot from '../../reducers';
import {RankListActions} from '../actions';
import {CryptoItem} from '../../models/CryptoItem';
import {FiatCurrency} from '../../models/FiatCurrency';

@Component({
  selector: 'app-list-crypto',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <button nz-button nzType="primary" nzSize="small" (click)="refresh()"><i nz-icon nzType="sync"></i></button>
    <app-crypto-list
      [list]="crypto$|async"
      [loading]="loading$|async"
      [error]="error$|async"
      [currency]="currency$|async"></app-crypto-list>
  `,
})
export class CryptoRankingPageComponent {
  crypto$: Observable<CryptoItem[]>;
  loading$: Observable<boolean>;
  error$: Observable<string>;
  currency$: Observable<FiatCurrency>;

  constructor(private store: Store<fromCrypto.State>) {
    this.crypto$ = store.pipe(select(fromCrypto.getListResults));
    this.loading$ = store.pipe(select(fromCrypto.getListLoading));
    this.error$ = store.pipe(select(fromCrypto.getListError));
    this.currency$ = store.pipe(select(fromRoot.getCurrency));
  }


  refresh() {
    this.store.dispatch(RankListActions.listCrypto({
      convert: ''
    }));
  }

}
