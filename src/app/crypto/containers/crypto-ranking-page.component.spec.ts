import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {CryptoRankingPageComponent} from './crypto-ranking-page.component';
import {MemoizedSelector, Store, StoreModule} from '@ngrx/store';
import {reducers} from '../reducers';
import {CryptoListComponent} from '../components/crypto-list/crypto-list.component';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import * as fromCrypto from '../../crypto/reducers';
import {cryptoRankListData} from '../../core/mock-data';
import {CryptoItem} from '../../models/CryptoItem';
import {HttpClientModule} from '@angular/common/http';
import {NzIconTestModule} from 'ng-zorro-antd/icon/testing';
import {provideMockActions} from '@ngrx/effects/testing';
import {RankListActions} from '../actions';
import {of} from 'rxjs';
import {CryptoRankEffects} from '../../effects/crypto-rank.effects';
import {EffectsModule} from '@ngrx/effects';
import {RouterTestingModule} from '@angular/router/testing';

describe('CryptoRankingPageComponent', () => {
  let component: CryptoRankingPageComponent;
  let fixture: ComponentFixture<CryptoRankingPageComponent>;
  let mockStore: MockStore<fromCrypto.State>;
  let mockSelector: MemoizedSelector<fromCrypto.State, CryptoItem[]>;
  let compiled;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [CryptoRankingPageComponent, CryptoListComponent],
        providers: [
          provideMockStore({
            initialState: {
              crypto: {
                list: {
                  loading: false,
                  error: '',
                  items: [],
                }
              }
            }
          }),
          provideMockActions(() => of(RankListActions)),
        ],
        imports: [
          EffectsModule.forRoot([CryptoRankEffects]),
          StoreModule.forRoot(reducers),
          NgZorroAntdModule,
          HttpClientModule,
          NzIconTestModule,
          RouterTestingModule
        ]
      })
      .compileComponents();
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(CryptoRankingPageComponent);
    mockStore = TestBed.get(Store);
    component = fixture.debugElement.componentInstance;
    compiled = fixture.debugElement.nativeElement;
    fixture.whenStable()
      .then(() => {
        mockSelector = mockStore.overrideSelector(fromCrypto.getListResults, cryptoRankListData.data);
      });
  }));

  it('should render', () => {
    component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });

  it('should render app-crypto-list', () => {
    expect(compiled.querySelector('app-crypto-list')).toBeTruthy();
  });
});
