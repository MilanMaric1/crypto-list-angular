import {Component, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import * as fromCrypto from '../reducers';
import * as fromRoot from '../../reducers';
import {Observable} from 'rxjs';
import {DetailsActions, RankListActions} from '../actions';
import {CryptoItem} from '../../models/CryptoItem';
import {FiatCurrency} from '../../models/FiatCurrency';
import {last} from 'rxjs/operators';

@Component({
  selector: 'app-crypto-details-page-component',
  template: `
    <app-crypto-details
      [item]="item$|async"
      [loading]="loading$|async"
      [currency]="currency$|async"></app-crypto-details>
    <button nz-button nzType="primary" nzSize="small" (click)="refresh()"><i nz-icon nzType="sync"></i></button>`
})

export class CryptoDetailsPageComponent implements OnInit {
  id$: Observable<string>;
  item$: Observable<CryptoItem>;
  loading$: Observable<boolean>;
  currency$: Observable<FiatCurrency>;
  id: string;

  constructor(private store: Store<fromCrypto.State>) {
    this.id$ = this.store.pipe(select(fromCrypto.getId));
    this.item$ = this.store.pipe(select(fromCrypto.getDetailsResult));
    this.loading$ = this.store.pipe(select(fromCrypto.getDetailsLoading));
    this.currency$ = store.pipe(select(fromRoot.getCurrency));
  }

  ngOnInit(): void {
    this.id$
      .subscribe((id) => {
        if (!id) {
          return;
        }
        this.id = id;
        this.refresh();
      });
  }

  refresh() {
    this.store.dispatch(DetailsActions.getOneCrypto({
      id: this.id
    }));
  }


}
