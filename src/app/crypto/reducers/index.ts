import * as fromList from './rank-list.reducer';
import * as fromDetails from './details.reducer';
import * as fromRoot from '../../reducers';

import {ActionReducerMap, createFeatureSelector, createSelector, select} from '@ngrx/store';


export interface CryptoState {
  list: fromList.State;
  details: fromDetails.State;
}

export interface State extends fromRoot.State {
  crypto: CryptoState;
}

export const reducers: ActionReducerMap<CryptoState, any> = {
  list: fromList.reducer,
  details: fromDetails.reducer,

};


export const getCryptoState = createFeatureSelector<State, CryptoState>('crypto');


export const getListState = createSelector(
  getCryptoState,
  (state: CryptoState) => state.list
);


export const getListResults = createSelector(
  getListState,
  fromList.getItems
);

export const getListLoading = createSelector(
  getListState,
  fromList.getLoading
);

export const getListError = createSelector(
  getListState,
  fromList.getError
);

export const getDetailsState = createSelector(
  getCryptoState,
  (state: CryptoState) => state.details
);

export const getDetailsResult = createSelector(
  getDetailsState,
  fromDetails.getItem
);

export const getDetailsLoading = createSelector(
  getDetailsState,
  fromDetails.getLoading
);

export const getDetailsError = createSelector(
  getDetailsState,
  fromDetails.getError
);

export const getId = createSelector(
  getDetailsState,
  fromDetails.getId
);



