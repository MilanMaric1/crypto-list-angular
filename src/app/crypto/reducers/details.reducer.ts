import {CryptoItem} from '../../models/CryptoItem';
import {CryptoApiActions, DetailsActions} from '../actions';
import {ROUTER_NAVIGATED, ROUTER_NAVIGATION, RouterAction, RouterNavigationPayload} from '@ngrx/router-store';
import * as fromRoot from '../../reducers';


export interface State {
  loading: boolean;
  error: string;
  item: CryptoItem;
  id: string;
}

const initialState: State = {
  loading: false,
  error: '',
  item: null,
  id: ''
};


export function reducer(
  state = initialState,
  action:
    | CryptoApiActions.CryptoApiActionsUnion
    | DetailsActions.CryptoDetailsActions
    | RouterAction<RouterNavigationPayload>
): State {
  switch (action.type) {
    case CryptoApiActions.detailFetchSuccess.type: {
      return {
        ...state,
        loading: false,
        item: action.item,
        error: ''
      };
    }

    case CryptoApiActions.detailFetchError.type: {
      return {
        ...state,
        loading: false,
        error: action.errorMsg
      };
    }

    case DetailsActions.getOneCrypto.type: {
      return {
        ...state,
        loading: true
      };
    }

    case DetailsActions.pageNavigated.type: {
      return {
        ...state,
        id: action.id
      };
    }

    default:
      return state;
  }
}


export const getItem = (state: State) => state.item;
export const getLoading = (state: State) => state.loading;
export const getError = (state: State) => state.error;
export const getId = (state: State) => state.id;
