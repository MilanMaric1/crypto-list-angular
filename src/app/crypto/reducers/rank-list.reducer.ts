import {CryptoItem} from '../../models/CryptoItem';
import {CryptoApiActions, RankListActions} from '../actions';


export interface State {
  loading: boolean;
  error: string;
  items: CryptoItem[];
}

const initialState: State = {
  loading: false,
  error: '',
  items: [],
};

export function reducer(
  state = initialState,
  action:
    | CryptoApiActions.CryptoApiActionsUnion
    | RankListActions.CryptoRankListActions
): State {
  switch (action.type) {
    case CryptoApiActions.rankListSuccess.type:
      return {
        loading: false,
        items: action.items,
        error: ''
      };
    case CryptoApiActions.rankListError.type:
      return {
        ...state,
        loading: false,
        error: action.errorMsg,
      };
    case  RankListActions.listCrypto.type:
      return {
        ...state,
        loading: true
      };
    default: {
      return state;
    }
  }
}

export const getItems = (state: State) => state.items;
export const getLoading = (state: State) => state.loading;
export const getError = (state: State) => state.error;

export const getBitcoinPrice = (state: State) => {
  if (!state.items) {
    return 0;
  }
  const bitcoin = state.items.find((item) => item.symbol === 'BTC');
  if (!bitcoin) {
    return 0;
  }

  return bitcoin.quote_mapped.price;
};
