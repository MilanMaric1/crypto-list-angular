import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {CryptoService} from '../../core/crypto.service';
import {asyncScheduler, Observable, of} from 'rxjs';
import {Action, Store} from '@ngrx/store';
import {CryptoApiActions, DetailsActions} from '../actions';
import {catchError, debounceTime, map, skip, switchMap, takeUntil, tap, withLatestFrom} from 'rxjs/operators';
import {CryptoItem} from '../../models/CryptoItem';
import {ROUTER_NAVIGATED, RouterAction} from '@ngrx/router-store';
import * as fromRoot from '../../reducers';

@Injectable()
export class CryptoDetailsEffects {
  @Effect()
  details$ = ({debounce = 300, scheduler = asyncScheduler} = {}): Observable<Action> =>
    this.actions$
      .pipe(
        ofType(DetailsActions.getOneCrypto.type),
        debounceTime(debounce, scheduler),
        withLatestFrom(this.store$.select(fromRoot.getCurrency)),
        switchMap(([{id}, currency]) => {
          return this.cryptoService
            .getOneCrypto(id, currency)
            .pipe(
              map((item: CryptoItem) => {
                return CryptoApiActions.detailFetchSuccess({item});
              }),
              catchError(err =>
                of(CryptoApiActions.detailFetchError({errorMsg: err}))
              )
            );
        })
      );

  @Effect()
  id$ = (): Observable<Action> =>
    this.actions$
      .pipe(
        ofType(ROUTER_NAVIGATED),
        map((action: any) => {
          const params = action.payload.routerState.params;
          if (!(params && params.id)) {
            return action;
          }
          return DetailsActions.pageNavigated({id: params.id});
        }),
        ofType(DetailsActions.pageNavigated.type)
      );

  constructor(
    private actions$: Actions<DetailsActions.CryptoDetailsActions>,
    private cryptoService: CryptoService,
    private store$: Store<fromRoot.State>
  ) {
  }
}
