import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CryptoRoutingModule} from './crypto-routing.module';
import {reducers} from './reducers';
import {StoreModule} from '@ngrx/store';
import {CryptoRankingPageComponent} from './containers/crypto-ranking-page.component';
import {EffectsModule} from '@ngrx/effects';
import {CryptoListComponent} from './components/crypto-list/crypto-list.component';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {CryptoDetailsPageComponent} from './containers/crypto-details-page.component';
import {CryptoDetailsEffects} from './effects/crypto-details.effects';
import {CryptoDetailsComponent} from './components/crypto-details/crypto-details.component';

@NgModule({
  declarations: [
    CryptoRankingPageComponent,
    CryptoListComponent,
    CryptoDetailsPageComponent,
    CryptoDetailsComponent
  ],
  imports: [
    CommonModule,
    StoreModule.forFeature('crypto', reducers),
    CryptoRoutingModule,
    EffectsModule.forFeature([CryptoDetailsEffects]),
    NgZorroAntdModule,
  ]
})
export class CryptoModule {
}
