import * as LayoutActions from './layout.actions';
import * as SettingsActions from './settings.actions';

export {
  LayoutActions,
  SettingsActions
};
