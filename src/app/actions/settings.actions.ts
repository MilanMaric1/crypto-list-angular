import {createAction, props, union} from '@ngrx/store';
import {FiatCurrency} from '../models/FiatCurrency';


export const setCurrency = createAction('[Settings Page] set currency',
  props<{
    symbol: FiatCurrency
  }>());


const all = union({setCurrency});
export type SettingsActionsUnion = typeof all;
