import {Component, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import * as fromRoot from '../../reducers';
import {from, Observable} from 'rxjs';
import {FiatCurrency} from '../../models/FiatCurrency';
import {SettingsActions} from '../../actions';

@Component({
  selector: 'app-settings-container-page',
  template: `
    <app-settings
      [currency]="currency$|async"
      (currencyChange)="onCurrencyChange($event)"></app-settings>
  `
})
export class SettingsContainerPageComponent {

  currency$: Observable<FiatCurrency>;

  constructor(private store: Store<fromRoot.State>) {
    this.currency$ = store.pipe(select(fromRoot.getCurrency));
  }


  onCurrencyChange(currency: FiatCurrency) {
    this.store.dispatch(SettingsActions.setCurrency({symbol: currency}));
  }

}
