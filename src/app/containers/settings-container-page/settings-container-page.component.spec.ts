import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SettingsContainerPageComponent} from './settings-container-page.component';
import {SettingsComponent} from '../../components/settings/settings.component';
import {FormsModule} from '@angular/forms';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {MemoizedSelector, Store, StoreModule} from '@ngrx/store';
import * as fromRoot from '../../reducers';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {HttpClientModule} from '@angular/common/http';
import {provideMockActions} from '@ngrx/effects/testing';
import {SettingsActions} from '../../actions';
import {of} from 'rxjs';

describe('SettingsContainerPageComponent', () => {
  let component: SettingsContainerPageComponent;
  let fixture: ComponentFixture<SettingsContainerPageComponent>;
  let mockStore: MockStore<fromRoot.State>;
  let mockSelector: MemoizedSelector<fromRoot.State, string>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockStore({
          initialState: {
            settings: {
              currency: 'USD'
            }
          }
        }),
        provideMockActions(() => of(SettingsActions))
      ],
      imports: [
        FormsModule,
        NgZorroAntdModule,
        StoreModule.forRoot(fromRoot.reducers),
        HttpClientModule,
      ],
      declarations: [SettingsContainerPageComponent, SettingsComponent]
    })
      .compileComponents();
  }));

  beforeEach(async () => {
    fixture = TestBed.createComponent(SettingsContainerPageComponent);
    mockStore = TestBed.get(Store);
    component = fixture.componentInstance;
    fixture.whenStable()
      .then(() => {
        mockSelector = mockStore.overrideSelector(fromRoot.getCurrency, 'USD');
      });
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
