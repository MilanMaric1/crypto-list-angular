export interface CryptoQuoteItem {
  price: number;
  volume_24h: number;
  percent_change_1h: number;
  percent_change_24h: number;
  percent_change_7d: number;
  currency: string;
  currency_symbol: string;
  market_cap: string;
}

export interface CryptoItem {
  id: string;
  name: string;
  symbol: string;
  cmc_rank: string;
  total_supply: number;
  max_supply: number;
  quote?: {
    USD: CryptoQuoteItem
  };
  quote_mapped?: CryptoQuoteItem;
  quote_bitcoin?: CryptoQuoteItem;
}

