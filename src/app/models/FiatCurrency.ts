export enum FiatCurrency {
  USD = 'USD',
  EUR = 'EUR',
  CNY = 'CNY',
  BTC = 'BTC'
}

const list = [
  {
    currency: FiatCurrency.EUR,
    symbol: '€',
    description: `${FiatCurrency.EUR} (€)`
  },
  {
    currency: FiatCurrency.USD,
    symbol: '$',
    description: `${FiatCurrency.USD} ($)`
  },
  {
    currency: FiatCurrency.CNY,
    symbol: '¥',
    description: `${FiatCurrency.CNY} (¥)`
  }
];

export const getSymbol = (currency: string) => {

  const foundItem = list.find((item) => item.currency === currency);
  if (foundItem) {
    return foundItem.symbol;
  }
  return '$';
};

export const getCurrencyList = () => {
  return list;
};
