export interface CryptoSearchItem {
  id: string;
  name: string;
  slug: string;
  status: string;
}
