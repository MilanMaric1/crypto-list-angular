import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FiatCurrency, getCurrencyList} from '../../models/FiatCurrency';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.less']
})
export class SettingsComponent {


  @Input()
  currency: FiatCurrency;

  list: any[] = getCurrencyList();

  @Output()
  currencyChange = new EventEmitter<FiatCurrency>();
}
