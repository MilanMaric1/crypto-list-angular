import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CryptoModule} from './crypto/crypto.module';
import {SettingsContainerPageComponent} from './containers/settings-container-page/settings-container-page.component';


const routes: Routes = [
  {
    path: '',
    loadChildren: './crypto/crypto.module#CryptoModule',
  },
  {
    path: 'settings',
    component: SettingsContainerPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
