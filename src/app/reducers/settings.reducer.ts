import {FiatCurrency} from '../models/FiatCurrency';
import {Action} from '@ngrx/store';
import {SettingsActions} from '../actions';

export interface State {
  currency: FiatCurrency;
}

const initialState: State = {
  currency: FiatCurrency.USD,
};


export function reducer(state: State = initialState, action: Action): State {
  const specificAction = action as SettingsActions.SettingsActionsUnion;

  switch (specificAction.type) {
    case SettingsActions.setCurrency.type: {
      return {
        currency: specificAction.symbol
      };
    }

    default:
      return state;
  }
}


export const getCurrency = (state) => state.currency;
