export const cryptoRankListData: any = {
  status: {
    timestamp: '2019-11-11T09:52:23.955Z',
    error_code: 0,
    error_message: null,
    elapsed: 50,
    credit_count: 1,
    notice: null
  },
  data: [
    {
      id: 1,
      name: 'Bitcoin',
      symbol: 'BTC',
      slug: 'bitcoin',
      num_market_pairs: 7767,
      date_added: '2013-04-28T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: 21000000,
      circulating_supply: 18047212,
      total_supply: 18047212,
      platform: null,
      cmc_rank: 1,
      last_updated: '2019-11-14T17:19:36.000Z',
      quote: {
        USD: {
          price: 8703.34700024,
          volume_24h: 19144788627.865,
          percent_change_1h: -0.000483066,
          percent_change_24h: -1.21921,
          percent_change_7d: -5.87575,
          market_cap: 157071148422.89536,
          last_updated: '2019-11-14T17:19:36.000Z'
        }
      }
    },
    {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      num_market_pairs: 5385,
      date_added: '2015-08-07T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: null,
      circulating_supply: 108551470.499,
      total_supply: 108551470.499,
      platform: null,
      cmc_rank: 2,
      last_updated: '2019-11-14T17:19:26.000Z',
      quote: {
        USD: {
          price: 185.857460251,
          volume_24h: 7886413803.79585,
          percent_change_1h: -0.118249,
          percent_change_24h: -1.36861,
          percent_change_7d: -0.783408,
          market_cap: 20175100613.45549,
          last_updated: '2019-11-14T17:19:26.000Z'
        }
      }
    },
    {
      id: 52,
      name: 'XRP',
      symbol: 'XRP',
      slug: 'ripple',
      num_market_pairs: 508,
      date_added: '2013-08-04T00:00:00.000Z',
      tags: [],
      max_supply: 100000000000,
      circulating_supply: 43298481757,
      total_supply: 99991298961,
      platform: null,
      cmc_rank: 3,
      last_updated: '2019-11-14T17:19:05.000Z',
      quote: {
        USD: {
          price: 0.270335694433,
          volume_24h: 1321699729.77276,
          percent_change_1h: 0.0789596,
          percent_change_24h: -1.42293,
          percent_change_7d: -7.199,
          market_cap: 11705125133.673176,
          last_updated: '2019-11-14T17:19:05.000Z'
        }
      }
    },
    {
      id: 1831,
      name: 'Bitcoin Cash',
      symbol: 'BCH',
      slug: 'bitcoin-cash',
      num_market_pairs: 440,
      date_added: '2017-07-23T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: 21000000,
      circulating_supply: 18112775,
      total_supply: 18112775,
      platform: null,
      cmc_rank: 4,
      last_updated: '2019-11-14T17:19:06.000Z',
      quote: {
        USD: {
          price: 279.084210305,
          volume_24h: 1797374187.76927,
          percent_change_1h: -0.154205,
          percent_change_24h: -2.69489,
          percent_change_7d: -4.43991,
          market_cap: 5054989507.307146,
          last_updated: '2019-11-14T17:19:06.000Z'
        }
      }
    },
    {
      id: 825,
      name: 'Tether',
      symbol: 'USDT',
      slug: 'tether',
      num_market_pairs: 3845,
      date_added: '2015-02-25T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 4108044456.1,
      total_supply: 4207771504.46,
      platform: {
        id: 83,
        name: 'Omni',
        symbol: 'OMNI',
        slug: 'omni',
        token_address: '31'
      },
      cmc_rank: 5,
      last_updated: '2019-11-14T17:19:22.000Z',
      quote: {
        USD: {
          price: 1.00486599223,
          volume_24h: 22098636663.3047,
          percent_change_1h: -0.0406058,
          percent_change_24h: -0.063769,
          percent_change_7d: 0.0749722,
          market_cap: 4128034168.503877,
          last_updated: '2019-11-14T17:19:22.000Z'
        }
      }
    },
    {
      id: 2,
      name: 'Litecoin',
      symbol: 'LTC',
      slug: 'litecoin',
      num_market_pairs: 559,
      date_added: '2013-04-28T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: 84000000,
      circulating_supply: 63678695.7674216,
      total_supply: 63678695.7674216,
      platform: null,
      cmc_rank: 6,
      last_updated: '2019-11-14T17:19:04.000Z',
      quote: {
        USD: {
          price: 59.7298122875,
          volume_24h: 3080733309.48578,
          percent_change_1h: -0.0397039,
          percent_change_24h: -2.47738,
          percent_change_7d: -3.53109,
          market_cap: 3803516544.9009132,
          last_updated: '2019-11-14T17:19:04.000Z'
        }
      }
    },
    {
      id: 1839,
      name: 'Binance Coin',
      symbol: 'BNB',
      slug: 'binance-coin',
      num_market_pairs: 283,
      date_added: '2017-07-25T00:00:00.000Z',
      tags: [],
      max_supply: 187536713,
      circulating_supply: 155536713,
      total_supply: 187536713,
      platform: null,
      cmc_rank: 7,
      last_updated: '2019-11-14T17:19:06.000Z',
      quote: {
        USD: {
          price: 21.2613611208,
          volume_24h: 248453918.266481,
          percent_change_1h: 0.0162822,
          percent_change_24h: -0.465434,
          percent_change_7d: 3.7068,
          market_cap: 3306922222.635228,
          last_updated: '2019-11-14T17:19:06.000Z'
        }
      }
    },
    {
      id: 1765,
      name: 'EOS',
      symbol: 'EOS',
      slug: 'eos',
      num_market_pairs: 394,
      date_added: '2017-07-01T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 940148012.1661,
      total_supply: 1036848023.6355,
      platform: null,
      cmc_rank: 8,
      last_updated: '2019-11-14T17:19:06.000Z',
      quote: {
        USD: {
          price: 3.42274415829,
          volume_24h: 1807459217.2669,
          percent_change_1h: -0.119196,
          percent_change_24h: -1.31931,
          percent_change_7d: -2.07022,
          market_cap: 3217886116.5694747,
          last_updated: '2019-11-14T17:19:06.000Z'
        }
      }
    },
    {
      id: 3602,
      name: 'Bitcoin SV',
      symbol: 'BSV',
      slug: 'bitcoin-sv',
      num_market_pairs: 158,
      date_added: '2018-11-09T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: 21000000,
      circulating_supply: 18068415,
      total_supply: 18068415,
      platform: null,
      cmc_rank: 9,
      last_updated: '2019-11-14T17:19:13.000Z',
      quote: {
        USD: {
          price: 127.491108752,
          volume_24h: 529383809.92938,
          percent_change_1h: -0.12293,
          percent_change_24h: -0.785414,
          percent_change_7d: -4.20698,
          market_cap: 2303562261.741268,
          last_updated: '2019-11-14T17:19:13.000Z'
        }
      }
    },
    {
      id: 512,
      name: 'Stellar',
      symbol: 'XLM',
      slug: 'stellar',
      num_market_pairs: 308,
      date_added: '2014-08-05T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 20054779553.7,
      total_supply: 50000000000,
      platform: null,
      cmc_rank: 10,
      last_updated: '2019-11-14T17:19:04.000Z',
      quote: {
        USD: {
          price: 0.0744336924305,
          volume_24h: 222584719.827058,
          percent_change_1h: 0.0103026,
          percent_change_24h: -3.15413,
          percent_change_7d: -1.23512,
          market_cap: 1492751293.0615861,
          last_updated: '2019-11-14T17:19:04.000Z'
        }
      }
    },
    {
      id: 1958,
      name: 'TRON',
      symbol: 'TRX',
      slug: 'tron',
      num_market_pairs: 300,
      date_added: '2017-09-13T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: null,
      circulating_supply: 66682072191.4,
      total_supply: 99281283754.3,
      platform: null,
      cmc_rank: 11,
      last_updated: '2019-11-14T17:19:07.000Z',
      quote: {
        USD: {
          price: 0.0194617276335,
          volume_24h: 1000620412.79918,
          percent_change_1h: 0.137923,
          percent_change_24h: -2.87613,
          percent_change_7d: -0.509061,
          market_cap: 1297748327.0264113,
          last_updated: '2019-11-14T17:19:07.000Z'
        }
      }
    },
    {
      id: 328,
      name: 'Monero',
      symbol: 'XMR',
      slug: 'monero',
      num_market_pairs: 128,
      date_added: '2014-05-21T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: null,
      circulating_supply: 17307468.9035555,
      total_supply: 17307468.9035555,
      platform: null,
      cmc_rank: 12,
      last_updated: '2019-11-14T17:19:02.000Z',
      quote: {
        USD: {
          price: 65.6735521974,
          volume_24h: 217424305.386937,
          percent_change_1h: 0.321149,
          percent_change_24h: 2.10306,
          percent_change_7d: 3.55709,
          market_cap: 1136642962.4425294,
          last_updated: '2019-11-14T17:19:02.000Z'
        }
      }
    },
    {
      id: 2010,
      name: 'Cardano',
      symbol: 'ADA',
      slug: 'cardano',
      num_market_pairs: 117,
      date_added: '2017-10-01T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: 45000000000,
      circulating_supply: 25927070538,
      total_supply: 31112483745,
      platform: null,
      cmc_rank: 13,
      last_updated: '2019-11-14T17:19:05.000Z',
      quote: {
        USD: {
          price: 0.0429082581227,
          volume_24h: 54305327.8499488,
          percent_change_1h: 0.239275,
          percent_change_24h: -1.84336,
          percent_change_7d: -1.32969,
          market_cap: 1112485435.0099545,
          last_updated: '2019-11-14T17:19:05.000Z'
        }
      }
    },
    {
      id: 1975,
      name: 'Chainlink',
      symbol: 'LINK',
      slug: 'chainlink',
      num_market_pairs: 124,
      date_added: '2017-09-20T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 350000000,
      total_supply: 1000000000,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x514910771af9ca656af840dff83e8264ecf986ca'
      },
      cmc_rank: 14,
      last_updated: '2019-11-14T17:19:05.000Z',
      quote: {
        USD: {
          price: 3.14118465726,
          volume_24h: 202759931.212486,
          percent_change_1h: -0.12786,
          percent_change_24h: 4.63743,
          percent_change_7d: 16.5615,
          market_cap: 1099414630.0410001,
          last_updated: '2019-11-14T17:19:05.000Z'
        }
      }
    },
    {
      id: 3957,
      name: 'UNUS SED LEO',
      symbol: 'LEO',
      slug: 'unus-sed-leo',
      num_market_pairs: 27,
      date_added: '2019-05-21T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 999498892.9,
      total_supply: 999498892.9,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x2af5d2ad76741191d15dfe7bf6ac92d4bd912ca3'
      },
      cmc_rank: 15,
      last_updated: '2019-11-14T17:19:14.000Z',
      quote: {
        USD: {
          price: 0.958404846259,
          volume_24h: 10234445.3806834,
          percent_change_1h: -0.0580285,
          percent_change_24h: -1.00714,
          percent_change_7d: -2.82432,
          market_cap: 957924582.7858652,
          last_updated: '2019-11-14T17:19:14.000Z'
        }
      }
    },
    {
      id: 2502,
      name: 'Huobi Token',
      symbol: 'HT',
      slug: 'huobi-token',
      num_market_pairs: 128,
      date_added: '2018-02-03T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 241284046.971921,
      total_supply: 500000000,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x6f259637dcd74c767781e37bc6133cd6a68aa161'
      },
      cmc_rank: 16,
      last_updated: '2019-11-14T17:19:08.000Z',
      quote: {
        USD: {
          price: 3.80102963087,
          volume_24h: 171735166.431899,
          percent_change_1h: -0.141494,
          percent_change_24h: -1.63811,
          percent_change_7d: -4.464,
          market_cap: 917127811.9965006,
          last_updated: '2019-11-14T17:19:08.000Z'
        }
      }
    },
    {
      id: 1376,
      name: 'NEO',
      symbol: 'NEO',
      slug: 'neo',
      num_market_pairs: 229,
      date_added: '2016-09-08T00:00:00.000Z',
      tags: [],
      max_supply: 100000000,
      circulating_supply: 70538831,
      total_supply: 100000000,
      platform: null,
      cmc_rank: 17,
      last_updated: '2019-11-14T17:19:04.000Z',
      quote: {
        USD: {
          price: 12.7253145793,
          volume_24h: 588710499.752798,
          percent_change_1h: -0.689308,
          percent_change_24h: -1.95051,
          percent_change_7d: 15.9959,
          market_cap: 897628814.5310788,
          last_updated: '2019-11-14T17:19:04.000Z'
        }
      }
    },
    {
      id: 2011,
      name: 'Tezos',
      symbol: 'XTZ',
      slug: 'tezos',
      num_market_pairs: 51,
      date_added: '2017-10-02T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 660373611.97278,
      total_supply: 801312599.488106,
      platform: null,
      cmc_rank: 18,
      last_updated: '2019-11-14T17:19:05.000Z',
      quote: {
        USD: {
          price: 1.22795946833,
          volume_24h: 33228052.9178442,
          percent_change_1h: 2.74473,
          percent_change_24h: 7.99419,
          percent_change_7d: 0.789334,
          market_cap: 810912029.4572566,
          last_updated: '2019-11-14T17:19:05.000Z'
        }
      }
    },
    {
      id: 3794,
      name: 'Cosmos',
      symbol: 'ATOM',
      slug: 'cosmos',
      num_market_pairs: 103,
      date_added: '2019-03-14T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 190688439.2,
      total_supply: 237928230.821588,
      platform: null,
      cmc_rank: 19,
      last_updated: '2019-11-14T17:19:13.000Z',
      quote: {
        USD: {
          price: 3.94978238439,
          volume_24h: 146490140.488516,
          percent_change_1h: 0.385089,
          percent_change_24h: -2.10058,
          percent_change_7d: 4.77145,
          market_cap: 753177838.0589836,
          last_updated: '2019-11-14T17:19:13.000Z'
        }
      }
    },
    {
      id: 1720,
      name: 'IOTA',
      symbol: 'MIOTA',
      slug: 'iota',
      num_market_pairs: 53,
      date_added: '2017-06-13T00:00:00.000Z',
      tags: [],
      max_supply: 2779530283,
      circulating_supply: 2779530283,
      total_supply: 2779530283,
      platform: null,
      cmc_rank: 20,
      last_updated: '2019-11-14T17:19:03.000Z',
      quote: {
        USD: {
          price: 0.265199718638,
          volume_24h: 11975935.5281368,
          percent_change_1h: -0.256846,
          percent_change_24h: 2.10332,
          percent_change_7d: -2.34331,
          market_cap: 737130648.9974005,
          last_updated: '2019-11-14T17:19:03.000Z'
        }
      }
    },
    {
      id: 1518,
      name: 'Maker',
      symbol: 'MKR',
      slug: 'maker',
      num_market_pairs: 79,
      date_added: '2017-01-29T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 1000000,
      total_supply: 1000000,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x9f8f72aa9304c8b593d555f12ef6589cc3a579a2'
      },
      cmc_rank: 21,
      last_updated: '2019-11-14T17:19:03.000Z',
      quote: {
        USD: {
          price: 645.301512572,
          volume_24h: 5310818.96852892,
          percent_change_1h: -0.438587,
          percent_change_24h: -2.18733,
          percent_change_7d: 3.34165,
          market_cap: 645301512.572,
          last_updated: '2019-11-14T17:19:03.000Z'
        }
      }
    },
    {
      id: 131,
      name: 'Dash',
      symbol: 'DASH',
      slug: 'dash',
      num_market_pairs: 276,
      date_added: '2014-02-14T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: 18900000,
      circulating_supply: 9153425.68719089,
      total_supply: 9153425.68719089,
      platform: null,
      cmc_rank: 22,
      last_updated: '2019-11-14T17:19:03.000Z',
      quote: {
        USD: {
          price: 69.3829322203,
          volume_24h: 352413826.45625,
          percent_change_1h: 0.25907,
          percent_change_24h: -1.25555,
          percent_change_7d: -4.73371,
          market_cap: 635091514.0379186,
          last_updated: '2019-11-14T17:19:03.000Z'
        }
      }
    },
    {
      id: 1321,
      name: 'Ethereum Classic',
      symbol: 'ETC',
      slug: 'ethereum-classic',
      num_market_pairs: 223,
      date_added: '2016-07-24T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: 210000000,
      circulating_supply: 115077737,
      total_supply: 115077737,
      platform: null,
      cmc_rank: 23,
      last_updated: '2019-11-14T17:19:04.000Z',
      quote: {
        USD: {
          price: 4.74586299657,
          volume_24h: 605290644.698795,
          percent_change_1h: -0.133463,
          percent_change_24h: -2.65263,
          percent_change_7d: -8.63452,
          market_cap: 546143173.7573143,
          last_updated: '2019-11-14T17:19:04.000Z'
        }
      }
    },
    {
      id: 2566,
      name: 'Ontology',
      symbol: 'ONT',
      slug: 'ontology',
      num_market_pairs: 107,
      date_added: '2018-03-08T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 587351170,
      total_supply: 1000000000,
      platform: null,
      cmc_rank: 24,
      last_updated: '2019-11-14T17:19:09.000Z',
      quote: {
        USD: {
          price: 0.884108420331,
          volume_24h: 158296166.456136,
          percent_change_1h: 0.168051,
          percent_change_24h: -4.22694,
          percent_change_7d: 0.289186,
          market_cap: 519282115.08826464,
          last_updated: '2019-11-14T17:19:09.000Z'
        }
      }
    },
    {
      id: 3635,
      name: 'Crypto.com Coin',
      symbol: 'CRO',
      slug: 'crypto-com-coin',
      num_market_pairs: 39,
      date_added: '2018-12-14T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 11666666666.6667,
      total_supply: 100000000000,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0xa0b73e1ff0b80914ab6fe0444e65848c4c34450b'
      },
      cmc_rank: 25,
      last_updated: '2019-11-14T17:19:13.000Z',
      quote: {
        USD: {
          price: 0.0380222304012,
          volume_24h: 23544350.1031139,
          percent_change_1h: -0.813447,
          percent_change_24h: 8.56536,
          percent_change_7d: -1.58872,
          market_cap: 443592688.0140013,
          last_updated: '2019-11-14T17:19:13.000Z'
        }
      }
    },
    {
      id: 3408,
      name: 'USD Coin',
      symbol: 'USDC',
      slug: 'usd-coin',
      num_market_pairs: 181,
      date_added: '2018-10-08T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 439016091.71118,
      total_supply: 441476124.53,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
      },
      cmc_rank: 26,
      last_updated: '2019-11-14T17:19:13.000Z',
      quote: {
        USD: {
          price: 1.00899888569,
          volume_24h: 207541951.758268,
          percent_change_1h: -0.0140131,
          percent_change_24h: 0.250654,
          percent_change_7d: 0.379812,
          market_cap: 442966747.3365595,
          last_updated: '2019-11-14T17:19:13.000Z'
        }
      }
    },
    {
      id: 3077,
      name: 'VeChain',
      symbol: 'VET',
      slug: 'vechain',
      num_market_pairs: 83,
      date_added: '2017-08-22T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 55454734800,
      total_supply: 86712634466,
      platform: null,
      cmc_rank: 27,
      last_updated: '2019-11-14T17:19:11.000Z',
      quote: {
        USD: {
          price: 0.00701749758878,
          volume_24h: 111259136.659106,
          percent_change_1h: 1.128,
          percent_change_24h: 9.18344,
          percent_change_7d: 26.1079,
          market_cap: 389153467.74543434,
          last_updated: '2019-11-14T17:19:11.000Z'
        }
      }
    },
    {
      id: 873,
      name: 'NEM',
      symbol: 'XEM',
      slug: 'nem',
      num_market_pairs: 97,
      date_added: '2015-04-01T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 8999999999,
      total_supply: 8999999999,
      platform: null,
      cmc_rank: 28,
      last_updated: '2019-11-14T17:19:02.000Z',
      quote: {
        USD: {
          price: 0.0399408080654,
          volume_24h: 8067537.1714488,
          percent_change_1h: 0.393992,
          percent_change_24h: -0.373977,
          percent_change_7d: -4.52684,
          market_cap: 359467272.5486592,
          last_updated: '2019-11-14T17:19:02.000Z'
        }
      }
    },
    {
      id: 1697,
      name: 'Basic Attention Token',
      symbol: 'BAT',
      slug: 'basic-attention-token',
      num_market_pairs: 161,
      date_added: '2017-06-01T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 1356386750.7463,
      total_supply: 1500000000,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x0d8775f648430679a709e98d2b0cb6250d2887ef'
      },
      cmc_rank: 29,
      last_updated: '2019-11-14T17:19:03.000Z',
      quote: {
        USD: {
          price: 0.261183562443,
          volume_24h: 78325937.3233626,
          percent_change_1h: -0.108171,
          percent_change_24h: 5.08739,
          percent_change_7d: 3.27686,
          market_cap: 354265923.61040413,
          last_updated: '2019-11-14T17:19:03.000Z'
        }
      }
    },
    {
      id: 74,
      name: 'Dogecoin',
      symbol: 'DOGE',
      slug: 'dogecoin',
      num_market_pairs: 285,
      date_added: '2013-12-15T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: null,
      circulating_supply: 122050253672.368,
      total_supply: 122050253672.368,
      platform: null,
      cmc_rank: 30,
      last_updated: '2019-11-14T17:19:02.000Z',
      quote: {
        USD: {
          price: 0.00267239344558,
          volume_24h: 77174751.4863165,
          percent_change_1h: 0.0976646,
          percent_change_24h: -1.49589,
          percent_change_7d: -0.905857,
          market_cap: 326166297.9454125,
          last_updated: '2019-11-14T17:19:02.000Z'
        }
      }
    },
    {
      id: 1437,
      name: 'Zcash',
      symbol: 'ZEC',
      slug: 'zcash',
      num_market_pairs: 222,
      date_added: '2016-10-29T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: null,
      circulating_supply: 7850181.25,
      total_supply: 7850181.25,
      platform: null,
      cmc_rank: 31,
      last_updated: '2019-11-14T17:19:04.000Z',
      quote: {
        USD: {
          price: 36.564082997,
          volume_24h: 128511860.592501,
          percent_change_1h: 0.0573591,
          percent_change_24h: -1.2313,
          percent_change_7d: -5.71198,
          market_cap: 287034678.7664932,
          last_updated: '2019-11-14T17:19:04.000Z'
        }
      }
    },
    {
      id: 3330,
      name: 'Paxos Standard',
      symbol: 'PAX',
      slug: 'paxos-standard',
      num_market_pairs: 100,
      date_added: '2018-09-27T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 229434803.15936,
      total_supply: 229937967.28,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x8e870d67f660d95d5be530380d0ec0bd388289e1'
      },
      cmc_rank: 32,
      last_updated: '2019-11-14T17:19:12.000Z',
      quote: {
        USD: {
          price: 1.00921318196,
          volume_24h: 225420215.927795,
          percent_change_1h: 0.01158,
          percent_change_24h: 0.194425,
          percent_change_7d: 0.324308,
          market_cap: 231548627.74882397,
          last_updated: '2019-11-14T17:19:12.000Z'
        }
      }
    },
    {
      id: 1168,
      name: 'Decred',
      symbol: 'DCR',
      slug: 'decred',
      num_market_pairs: 46,
      date_added: '2016-02-10T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: 21000000,
      circulating_supply: 10673680.3859787,
      total_supply: 10673680.3859787,
      platform: null,
      cmc_rank: 33,
      last_updated: '2019-11-14T17:19:02.000Z',
      quote: {
        USD: {
          price: 21.5742560283,
          volume_24h: 12021088.6172664,
          percent_change_1h: 0.795931,
          percent_change_24h: -2.22261,
          percent_change_7d: 2.96166,
          market_cap: 230276713.41134843,
          last_updated: '2019-11-14T17:19:02.000Z'
        }
      }
    },
    {
      id: 1684,
      name: 'Qtum',
      symbol: 'QTUM',
      slug: 'qtum',
      num_market_pairs: 173,
      date_added: '2017-05-24T00:00:00.000Z',
      tags: [],
      max_supply: 107822406,
      circulating_supply: 96163648.0484598,
      total_supply: 101913668,
      platform: null,
      cmc_rank: 34,
      last_updated: '2019-11-14T17:19:04.000Z',
      quote: {
        USD: {
          price: 2.28395259635,
          volume_24h: 340159675.490253,
          percent_change_1h: -0.082474,
          percent_change_24h: 0.113593,
          percent_change_7d: 3.51038,
          market_cap: 219633213.63476735,
          last_updated: '2019-11-14T17:19:04.000Z'
        }
      }
    },
    {
      id: 3662,
      name: 'HedgeTrade',
      symbol: 'HEDG',
      slug: 'hedgetrade',
      num_market_pairs: 8,
      date_added: '2019-01-03T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 288330855.057208,
      total_supply: 1000000000.05,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0xF1290473E210b2108A85237fbCd7b6eb42Cc654F'
      },
      cmc_rank: 35,
      last_updated: '2019-11-14T17:19:13.000Z',
      quote: {
        USD: {
          price: 0.714311759433,
          volume_24h: 202039.335637315,
          percent_change_1h: 0.278826,
          percent_change_24h: -1.02969,
          percent_change_7d: -6.22021,
          market_cap: 205958120.37473553,
          last_updated: '2019-11-14T17:19:13.000Z'
        }
      }
    },
    {
      id: 1896,
      name: '0x',
      symbol: 'ZRX',
      slug: '0x',
      num_market_pairs: 183,
      date_added: '2017-08-16T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 602005040.598496,
      total_supply: 1000000000,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0xe41d2489571d322189246dafa5ebde1f4699f498'
      },
      cmc_rank: 36,
      last_updated: '2019-11-14T17:19:05.000Z',
      quote: {
        USD: {
          price: 0.290223821244,
          volume_24h: 19264547.7837357,
          percent_change_1h: -0.0178574,
          percent_change_24h: -3.65054,
          percent_change_7d: -4.51499,
          market_cap: 174716203.29064485,
          last_updated: '2019-11-14T17:19:05.000Z'
        }
      }
    },
    {
      id: 2563,
      name: 'TrueUSD',
      symbol: 'TUSD',
      slug: 'trueusd',
      num_market_pairs: 155,
      date_added: '2018-03-06T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 168469805.5,
      total_supply: 168469805.5,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x0000000000085d4780B73119b644AE5ecd22b376'
      },
      cmc_rank: 37,
      last_updated: '2019-11-14T17:19:08.000Z',
      quote: {
        USD: {
          price: 1.00750164683,
          volume_24h: 160220250.830383,
          percent_change_1h: -0.0164196,
          percent_change_24h: 0.115517,
          percent_change_7d: 0.241001,
          market_cap: 169733606.4823798,
          last_updated: '2019-11-14T17:19:08.000Z'
        }
      }
    },
    {
      id: 2682,
      name: 'Holo',
      symbol: 'HOT',
      slug: 'holo',
      num_market_pairs: 63,
      date_added: '2018-04-29T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 161335841957.48,
      total_supply: 177619433541.141,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x6c6ee5e31d828de241282b9606c8e98ea48526e2'
      },
      cmc_rank: 38,
      last_updated: '2019-11-14T17:19:09.000Z',
      quote: {
        USD: {
          price: 0.00099335486586,
          volume_24h: 13047046.355954,
          percent_change_1h: -0.122473,
          percent_change_24h: -1.47878,
          percent_change_7d: 1.63688,
          market_cap: 160263743.6460827,
          last_updated: '2019-11-14T17:19:09.000Z'
        }
      }
    },
    {
      id: 2585,
      name: 'Centrality',
      symbol: 'CENNZ',
      slug: 'centrality',
      num_market_pairs: 2,
      date_added: '2018-03-13T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 1063737441.99351,
      total_supply: 1200000000,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x1122b6a0e00dce0563082b6e2953f3a943855c1f'
      },
      cmc_rank: 39,
      last_updated: '2019-11-14T17:19:08.000Z',
      quote: {
        USD: {
          price: 0.14922856159,
          volume_24h: 533481.655728298,
          percent_change_1h: 1.40245,
          percent_change_24h: 5.71743,
          percent_change_7d: 9.83472,
          market_cap: 158740008.37811756,
          last_updated: '2019-11-14T17:19:08.000Z'
        }
      }
    },
    {
      id: 3704,
      name: 'V Systems',
      symbol: 'VSYS',
      slug: 'v-systems',
      num_market_pairs: 33,
      date_added: '2019-03-05T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 1851441031,
      total_supply: 3766299495,
      platform: null,
      cmc_rank: 40,
      last_updated: '2019-11-14T17:19:13.000Z',
      quote: {
        USD: {
          price: 0.0788098615598,
          volume_24h: 3676659.52168286,
          percent_change_1h: -1.26127,
          percent_change_24h: -4.29563,
          percent_change_7d: 2.07716,
          market_cap: 145911811.33924338,
          last_updated: '2019-11-14T17:19:13.000Z'
        }
      }
    },
    {
      id: 2083,
      name: 'Bitcoin Gold',
      symbol: 'BTG',
      slug: 'bitcoin-gold',
      num_market_pairs: 83,
      date_added: '2017-10-23T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: 21000000,
      circulating_supply: 17513923.589,
      total_supply: 17513923.589,
      platform: null,
      cmc_rank: 41,
      last_updated: '2019-11-14T17:19:05.000Z',
      quote: {
        USD: {
          price: 8.16726142442,
          volume_24h: 13382799.0233942,
          percent_change_1h: 0.816673,
          percent_change_24h: -4.03599,
          percent_change_7d: -7.63636,
          market_cap: 143040792.51867917,
          last_updated: '2019-11-14T17:19:05.000Z'
        }
      }
    },
    {
      id: 1808,
      name: 'OmiseGO',
      symbol: 'OMG',
      slug: 'omisego',
      num_market_pairs: 209,
      date_added: '2017-07-14T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 140245398.245133,
      total_supply: 140245398.245133,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0xd26114cd6EE289AccF82350c8d8487fedB8A0C07'
      },
      cmc_rank: 42,
      last_updated: '2019-11-14T17:19:04.000Z',
      quote: {
        USD: {
          price: 1.00295188373,
          volume_24h: 86480368.3870568,
          percent_change_1h: 1.11315,
          percent_change_24h: 0.621039,
          percent_change_7d: 1.5282,
          market_cap: 140659386.35442019,
          last_updated: '2019-11-14T17:19:04.000Z'
        }
      }
    },
    {
      id: 2577,
      name: 'Ravencoin',
      symbol: 'RVN',
      slug: 'ravencoin',
      num_market_pairs: 46,
      date_added: '2018-03-10T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: 21000000000,
      circulating_supply: 4853220000,
      total_supply: 4853220000,
      platform: null,
      cmc_rank: 43,
      last_updated: '2019-11-14T17:19:08.000Z',
      quote: {
        USD: {
          price: 0.0273829413945,
          volume_24h: 8738669.67785623,
          percent_change_1h: -0.0915151,
          percent_change_24h: -3.23968,
          percent_change_7d: -6.46153,
          market_cap: 132895438.83461529,
          last_updated: '2019-11-14T17:19:08.000Z'
        }
      }
    },
    {
      id: 3351,
      name: 'ZB',
      symbol: 'ZB',
      slug: 'zb',
      num_market_pairs: 10,
      date_added: '2018-09-27T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 463288810,
      total_supply: 2100000000,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0xbd0793332e9fb844a52a205a233ef27a5b34b927'
      },
      cmc_rank: 44,
      last_updated: '2019-11-14T17:19:12.000Z',
      quote: {
        USD: {
          price: 0.284147334646,
          volume_24h: 319498073.269108,
          percent_change_1h: -0.654019,
          percent_change_24h: -3.49026,
          percent_change_7d: -5.60594,
          market_cap: 131642280.53281711,
          last_updated: '2019-11-14T17:19:12.000Z'
        }
      }
    },
    {
      id: 1567,
      name: 'Nano',
      symbol: 'NANO',
      slug: 'nano',
      num_market_pairs: 49,
      date_added: '2017-03-06T00:00:00.000Z',
      tags: [],
      max_supply: 133248297.197,
      circulating_supply: 133248297.197,
      total_supply: 133248297.197,
      platform: null,
      cmc_rank: 45,
      last_updated: '2019-11-14T17:19:03.000Z',
      quote: {
        USD: {
          price: 0.961399343929,
          volume_24h: 4643384.02193062,
          percent_change_1h: 0.0326188,
          percent_change_24h: -6.29465,
          percent_change_7d: -5.26735,
          market_cap: 128104825.5048522,
          last_updated: '2019-11-14T17:19:03.000Z'
        }
      }
    },
    {
      id: 2586,
      name: 'Synthetix Network Token',
      symbol: 'SNX',
      slug: 'synthetix-network-token',
      num_market_pairs: 7,
      date_added: '2018-03-14T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 145171922.540886,
      total_supply: 151923076.923077,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0xc011a72400e58ecd99ee497cf89e3775d4bd732f'
      },
      cmc_rank: 46,
      last_updated: '2019-11-14T17:19:08.000Z',
      quote: {
        USD: {
          price: 0.871894838106,
          volume_24h: 210069.967437464,
          percent_change_1h: 0.768054,
          percent_change_24h: 4.60924,
          percent_change_7d: 4.71075,
          market_cap: 126574649.90132259,
          last_updated: '2019-11-14T17:19:08.000Z'
        }
      }
    },
    {
      id: 3437,
      name: 'ABBC Coin',
      symbol: 'ABBC',
      slug: 'abbc-coin',
      num_market_pairs: 39,
      date_added: '2018-10-12T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: null,
      circulating_supply: 556626634.357622,
      total_supply: 1004488460.0178,
      platform: null,
      cmc_rank: 47,
      last_updated: '2019-11-14T17:19:12.000Z',
      quote: {
        USD: {
          price: 0.225848660634,
          volume_24h: 57986374.0379384,
          percent_change_1h: -0.614926,
          percent_change_24h: -1.1874,
          percent_change_7d: 7.47809,
          market_cap: 125713379.84288017,
          last_updated: '2019-11-14T17:19:12.000Z'
        }
      }
    },
    {
      id: 1104,
      name: 'Augur',
      symbol: 'REP',
      slug: 'augur',
      num_market_pairs: 74,
      date_added: '2015-10-27T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 11000000,
      total_supply: 11000000,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x1985365e9f78359a9b6ad760e32412f4a445e862'
      },
      cmc_rank: 48,
      last_updated: '2019-11-14T17:19:02.000Z',
      quote: {
        USD: {
          price: 11.3482723773,
          volume_24h: 7869870.15325117,
          percent_change_1h: -0.364333,
          percent_change_24h: -1.64031,
          percent_change_7d: 2.23878,
          market_cap: 124830996.15030001,
          last_updated: '2019-11-14T17:19:02.000Z'
        }
      }
    },
    {
      id: 4030,
      name: 'Algorand',
      symbol: 'ALGO',
      slug: 'algorand',
      num_market_pairs: 58,
      date_added: '2019-06-20T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: null,
      circulating_supply: 432847251.163547,
      total_supply: 2964119094.16355,
      platform: null,
      cmc_rank: 49,
      last_updated: '2019-11-14T17:19:14.000Z',
      quote: {
        USD: {
          price: 0.282655735188,
          volume_24h: 114924665.36425,
          percent_change_1h: -0.512688,
          percent_change_24h: -1.57599,
          percent_change_7d: 3.71412,
          market_cap: 122346758.00173727,
          last_updated: '2019-11-14T17:19:14.000Z'
        }
      }
    },
    {
      id: 1866,
      name: 'Bytom',
      symbol: 'BTM',
      slug: 'bytom',
      num_market_pairs: 50,
      date_added: '2017-08-08T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: null,
      circulating_supply: 1002499275,
      total_supply: 1407000000,
      platform: null,
      cmc_rank: 50,
      last_updated: '2019-11-14T17:19:04.000Z',
      quote: {
        USD: {
          price: 0.114387242552,
          volume_24h: 21918938.4754465,
          percent_change_1h: -0.0764098,
          percent_change_24h: -4.0292,
          percent_change_7d: -14.5533,
          market_cap: 114673127.72762915,
          last_updated: '2019-11-14T17:19:04.000Z'
        }
      }
    },
    {
      id: 4172,
      name: 'LUNA',
      symbol: 'LUNA',
      slug: 'luna',
      num_market_pairs: 10,
      date_added: '2019-07-26T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 287765804.430561,
      total_supply: 995859074.224591,
      platform: null,
      cmc_rank: 51,
      last_updated: '2019-11-14T17:19:14.000Z',
      quote: {
        USD: {
          price: 0.368644828993,
          volume_24h: 2116434.17142567,
          percent_change_1h: 2.0273,
          percent_change_24h: -2.69232,
          percent_change_7d: -17.639,
          market_cap: 106083375.76433724,
          last_updated: '2019-11-14T17:19:14.000Z'
        }
      }
    },
    {
      id: 1521,
      name: 'Komodo',
      symbol: 'KMD',
      slug: 'komodo',
      num_market_pairs: 23,
      date_added: '2017-02-05T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: 200000000,
      circulating_supply: 116924893.55032,
      total_supply: 116924893.55032,
      platform: null,
      cmc_rank: 52,
      last_updated: '2019-11-14T17:19:03.000Z',
      quote: {
        USD: {
          price: 0.890067228521,
          volume_24h: 7224248.50983621,
          percent_change_1h: 0.26343,
          percent_change_24h: -7.08347,
          percent_change_7d: 26.3397,
          market_cap: 104071015.94744627,
          last_updated: '2019-11-14T17:19:03.000Z'
        }
      }
    },
    {
      id: 2308,
      name: 'Dai',
      symbol: 'DAI',
      slug: 'dai',
      num_market_pairs: 106,
      date_added: '2017-12-24T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 102663712.635708,
      total_supply: 102663712.635708,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x89d24a6b4ccb1b6faa2625fe562bdd9a23260359'
      },
      cmc_rank: 53,
      last_updated: '2019-11-14T17:19:06.000Z',
      quote: {
        USD: {
          price: 1.00907992554,
          volume_24h: 4653475.00459962,
          percent_change_1h: -0.119954,
          percent_change_24h: 0.150679,
          percent_change_7d: 0.152193,
          market_cap: 103595891.5021002,
          last_updated: '2019-11-14T17:19:06.000Z'
        }
      }
    },
    {
      id: 2087,
      name: 'KuCoin Shares',
      symbol: 'KCS',
      slug: 'kucoin-shares',
      num_market_pairs: 15,
      date_added: '2017-10-24T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 82363551,
      total_supply: 172363551,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x039b5649a59967e3e936d7471f9c3700100ee1ab'
      },
      cmc_rank: 54,
      last_updated: '2019-11-14T17:19:05.000Z',
      quote: {
        USD: {
          price: 1.17652174502,
          volume_24h: 5131318.36036234,
          percent_change_1h: -0.169988,
          percent_change_24h: -2.1115,
          percent_change_7d: -12.2981,
          market_cap: 96902508.74856377,
          last_updated: '2019-11-14T17:19:05.000Z'
        }
      }
    },
    {
      id: 2453,
      name: 'EDUCare',
      symbol: 'EKT',
      slug: 'educare',
      num_market_pairs: 6,
      date_added: '2018-01-23T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 950000000,
      total_supply: 1000000000,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x4ecdb6385f3db3847f9c4a9bf3f9917bb27a5452'
      },
      cmc_rank: 55,
      last_updated: '2019-11-14T17:19:07.000Z',
      quote: {
        USD: {
          price: 0.101789103989,
          volume_24h: 5645027.61525481,
          percent_change_1h: -0.291799,
          percent_change_24h: -0.432927,
          percent_change_7d: -15.3125,
          market_cap: 96699648.78954999,
          last_updated: '2019-11-14T17:19:07.000Z'
        }
      }
    },
    {
      id: 4079,
      name: 'Silverway',
      symbol: 'SLV',
      slug: 'silverway',
      num_market_pairs: 5,
      date_added: '2019-07-03T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 100000000,
      total_supply: 1000000000,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x4c1c4957d22d8f373aed54d0853b090666f6f9de'
      },
      cmc_rank: 56,
      last_updated: '2019-11-14T17:19:14.000Z',
      quote: {
        USD: {
          price: 0.943169991688,
          volume_24h: 5664686.79536967,
          percent_change_1h: -0.301674,
          percent_change_24h: -4.79958,
          percent_change_7d: -13.4343,
          market_cap: 94316999.1688,
          last_updated: '2019-11-14T17:19:14.000Z'
        }
      }
    },
    {
      id: 1214,
      name: 'Lisk',
      symbol: 'LSK',
      slug: 'lisk',
      num_market_pairs: 58,
      date_added: '2016-04-06T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 121484888.512821,
      total_supply: 136517040,
      platform: null,
      cmc_rank: 57,
      last_updated: '2019-11-14T17:19:03.000Z',
      quote: {
        USD: {
          price: 0.767653458646,
          volume_24h: 1435367.87707622,
          percent_change_1h: 0.815396,
          percent_change_24h: 1.90818,
          percent_change_7d: -4.14559,
          market_cap: 93258294.84009075,
          last_updated: '2019-11-14T17:19:03.000Z'
        }
      }
    },
    {
      id: 3139,
      name: 'DxChain Token',
      symbol: 'DX',
      slug: 'dxchain-token',
      num_market_pairs: 8,
      date_added: '2018-08-10T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 49999999999.6862,
      total_supply: 100000000000,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x973e52691176d36453868D9d86572788d27041A9'
      },
      cmc_rank: 58,
      last_updated: '2019-11-14T17:19:11.000Z',
      quote: {
        USD: {
          price: 0.00185786020029,
          volume_24h: 4041163.87848336,
          percent_change_1h: -0.4364,
          percent_change_24h: -1.96124,
          percent_change_7d: 87.7603,
          market_cap: 92893010.01391701,
          last_updated: '2019-11-14T17:19:11.000Z'
        }
      }
    },
    {
      id: 3718,
      name: 'BitTorrent',
      symbol: 'BTT',
      slug: 'bittorrent',
      num_market_pairs: 131,
      date_added: '2019-01-31T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 212116500000,
      total_supply: 990000000000,
      platform: {
        id: 1958,
        name: 'TRON',
        symbol: 'TRX',
        slug: 'tron',
        token_address: '1002000'
      },
      cmc_rank: 59,
      last_updated: '2019-11-14T17:19:13.000Z',
      quote: {
        USD: {
          price: 0.000430242294731,
          volume_24h: 67703300.1067289,
          percent_change_1h: 0.474809,
          percent_change_24h: -1.76014,
          percent_change_7d: -5.70844,
          market_cap: 91261489.71030815,
          last_updated: '2019-11-14T17:19:13.000Z'
        }
      }
    },
    {
      id: 2222,
      name: 'Bitcoin Diamond',
      symbol: 'BCD',
      slug: 'bitcoin-diamond',
      num_market_pairs: 25,
      date_added: '2017-11-24T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: 210000000,
      circulating_supply: 186492897.953,
      total_supply: 189492897.953,
      platform: null,
      cmc_rank: 60,
      last_updated: '2019-11-14T17:19:05.000Z',
      quote: {
        USD: {
          price: 0.482864881857,
          volume_24h: 3386447.55642547,
          percent_change_1h: -0.782293,
          percent_change_24h: -3.64672,
          percent_change_7d: -7.37773,
          market_cap: 90050871.13724491,
          last_updated: '2019-11-14T17:19:05.000Z'
        }
      }
    },
    {
      id: 2830,
      name: 'Seele',
      symbol: 'SEELE',
      slug: 'seele',
      num_market_pairs: 15,
      date_added: '2018-05-31T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 696705193.289478,
      total_supply: 1000000000,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0xb1eef147028e9f480dbc5ccaa3277d417d1b85f0'
      },
      cmc_rank: 61,
      last_updated: '2019-11-14T17:19:10.000Z',
      quote: {
        USD: {
          price: 0.128145683794,
          volume_24h: 81790689.8384599,
          percent_change_1h: 7.61545,
          percent_change_24h: 29.8552,
          percent_change_7d: 57.7674,
          market_cap: 89279763.39691108,
          last_updated: '2019-11-14T17:19:10.000Z'
        }
      }
    },
    {
      id: 109,
      name: 'DigiByte',
      symbol: 'DGB',
      slug: 'digibyte',
      num_market_pairs: 78,
      date_added: '2014-02-06T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: 21000000000,
      circulating_supply: 12489651681.3676,
      total_supply: 12489651681.3676,
      platform: null,
      cmc_rank: 62,
      last_updated: '2019-11-14T17:19:01.000Z',
      quote: {
        USD: {
          price: 0.00706055099112,
          volume_24h: 1282496.66561493,
          percent_change_1h: 0.618961,
          percent_change_24h: -1.60286,
          percent_change_7d: -0.411486,
          market_cap: 88183822.55762358,
          last_updated: '2019-11-14T17:19:01.000Z'
        }
      }
    },
    {
      id: 1042,
      name: 'Siacoin',
      symbol: 'SC',
      slug: 'siacoin',
      num_market_pairs: 38,
      date_added: '2015-08-26T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: null,
      circulating_supply: 41817047634,
      total_supply: 41817047634,
      platform: null,
      cmc_rank: 63,
      last_updated: '2019-11-14T17:19:02.000Z',
      quote: {
        USD: {
          price: 0.00204217904664,
          volume_24h: 3194701.70379804,
          percent_change_1h: -0.336996,
          percent_change_24h: -1.61955,
          percent_change_7d: -1.27591,
          market_cap: 85397898.47050159,
          last_updated: '2019-11-14T17:19:02.000Z'
        }
      }
    },
    {
      id: 1903,
      name: 'HyperCash',
      symbol: 'HC',
      slug: 'hypercash',
      num_market_pairs: 32,
      date_added: '2017-08-20T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: 84000000,
      circulating_supply: 44429162.497052,
      total_supply: 44429162.497052,
      platform: null,
      cmc_rank: 64,
      last_updated: '2019-11-14T17:19:04.000Z',
      quote: {
        USD: {
          price: 1.84307808436,
          volume_24h: 3252735.73156128,
          percent_change_1h: 0.283898,
          percent_change_24h: -2.42423,
          percent_change_7d: -3.97697,
          market_cap: 81886415.70478575,
          last_updated: '2019-11-14T17:19:04.000Z'
        }
      }
    },
    {
      id: 2099,
      name: 'ICON',
      symbol: 'ICX',
      slug: 'icon',
      num_market_pairs: 63,
      date_added: '2017-10-27T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 504397787.879946,
      total_supply: 800460000,
      platform: null,
      cmc_rank: 65,
      last_updated: '2019-11-14T17:19:05.000Z',
      quote: {
        USD: {
          price: 0.16025727921,
          volume_24h: 8304269.69959041,
          percent_change_1h: 0.133633,
          percent_change_24h: -5.43433,
          percent_change_7d: -10.2305,
          market_cap: 80833417.12518285,
          last_updated: '2019-11-14T17:19:05.000Z'
        }
      }
    },
    {
      id: 3155,
      name: 'Quant',
      symbol: 'QNT',
      slug: 'quant',
      num_market_pairs: 18,
      date_added: '2018-08-10T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 12072738,
      total_supply: 14612493.0808262,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x4a220e6096b25eadb88358cb44068a3248254675'
      },
      cmc_rank: 66,
      last_updated: '2019-11-14T17:19:11.000Z',
      quote: {
        USD: {
          price: 6.67393219354,
          volume_24h: 21690141.5526024,
          percent_change_1h: 0.257392,
          percent_change_24h: 0.438803,
          percent_change_7d: -1.88358,
          market_cap: 80572634.8023737,
          last_updated: '2019-11-14T17:19:11.000Z'
        }
      }
    },
    {
      id: 4279,
      name: 'Swipe',
      symbol: 'SXP',
      slug: 'swipe',
      num_market_pairs: 9,
      date_added: '2019-08-26T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 61135911.166,
      total_supply: 300000000,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x8ce9137d39326ad0cd6491fb5cc0cba0e089b6a9'
      },
      cmc_rank: 67,
      last_updated: '2019-11-14T17:19:15.000Z',
      quote: {
        USD: {
          price: 1.2857071888,
          volume_24h: 28078188.4908657,
          percent_change_1h: -0.0271596,
          percent_change_24h: -0.384172,
          percent_change_7d: 4.97729,
          market_cap: 78602880.47996439,
          last_updated: '2019-11-14T17:19:15.000Z'
        }
      }
    },
    {
      id: 1274,
      name: 'Waves',
      symbol: 'WAVES',
      slug: 'waves',
      num_market_pairs: 142,
      date_added: '2016-06-02T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 100331806,
      total_supply: 100331806,
      platform: null,
      cmc_rank: 68,
      last_updated: '2019-11-14T17:19:03.000Z',
      quote: {
        USD: {
          price: 0.765414764024,
          volume_24h: 28535318.0436539,
          percent_change_1h: 0.00511462,
          percent_change_24h: -1.80687,
          percent_change_7d: -5.11944,
          market_cap: 76795445.61359175,
          last_updated: '2019-11-14T17:19:03.000Z'
        }
      }
    },
    {
      id: 2416,
      name: 'THETA',
      symbol: 'THETA',
      slug: 'theta',
      num_market_pairs: 31,
      date_added: '2018-01-17T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 870502690,
      total_supply: 1000000000,
      platform: null,
      cmc_rank: 69,
      last_updated: '2019-11-14T17:19:07.000Z',
      quote: {
        USD: {
          price: 0.088162241137,
          volume_24h: 11123365.5073573,
          percent_change_1h: 0.933474,
          percent_change_24h: -2.28919,
          percent_change_7d: -3.78064,
          market_cap: 76745468.06618716,
          last_updated: '2019-11-14T17:19:07.000Z'
        }
      }
    },
    {
      id: 2405,
      name: 'IOST',
      symbol: 'IOST',
      slug: 'iostoken',
      num_market_pairs: 81,
      date_added: '2018-01-16T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 12013965608.8475,
      total_supply: 21000000000,
      platform: null,
      cmc_rank: 70,
      last_updated: '2019-11-14T17:19:07.000Z',
      quote: {
        USD: {
          price: 0.00635686938709,
          volume_24h: 30983423.9931561,
          percent_change_1h: -0.0352974,
          percent_change_24h: -2.58813,
          percent_change_7d: -3.15057,
          market_cap: 76371210.19643475,
          last_updated: '2019-11-14T17:19:07.000Z'
        }
      }
    },
    {
      id: 4195,
      name: 'FTX Token',
      symbol: 'FTT',
      slug: 'ftx-token',
      num_market_pairs: 13,
      date_added: '2019-07-31T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 52631545.9327723,
      total_supply: 348503882,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x50d1c9771902476076ecfc8b2a83ad6b9355a4c9'
      },
      cmc_rank: 71,
      last_updated: '2019-11-14T17:19:14.000Z',
      quote: {
        USD: {
          price: 1.43951377585,
          volume_24h: 3250005.50551815,
          percent_change_1h: 0.265918,
          percent_change_24h: 2.43943,
          percent_change_7d: 5.95053,
          market_cap: 75763835.41450776,
          last_updated: '2019-11-14T17:19:14.000Z'
        }
      }
    },
    {
      id: 2907,
      name: 'Karatgold Coin',
      symbol: 'KBC',
      slug: 'karatgold-coin',
      num_market_pairs: 28,
      date_added: '2018-07-06T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 3752553044.13502,
      total_supply: 12000000000,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0xf3586684107ce0859c44aa2b2e0fb8cd8731a15a'
      },
      cmc_rank: 72,
      last_updated: '2019-11-14T17:19:10.000Z',
      quote: {
        USD: {
          price: 0.0199911295904,
          volume_24h: 2583948.55342107,
          percent_change_1h: 1.26425,
          percent_change_24h: -1.71818,
          percent_change_7d: -3.58221,
          market_cap: 75017774.20015319,
          last_updated: '2019-11-14T17:19:10.000Z'
        }
      }
    },
    {
      id: 463,
      name: 'BitShares',
      symbol: 'BTS',
      slug: 'bitshares',
      num_market_pairs: 85,
      date_added: '2014-07-21T00:00:00.000Z',
      tags: [],
      max_supply: 3600570502,
      circulating_supply: 2747920000,
      total_supply: 2747920000,
      platform: null,
      cmc_rank: 73,
      last_updated: '2019-11-14T17:19:02.000Z',
      quote: {
        USD: {
          price: 0.0265656296065,
          volume_24h: 1741773.02804758,
          percent_change_1h: 0.219969,
          percent_change_24h: -1.63621,
          percent_change_7d: -9.40575,
          market_cap: 73000224.90829349,
          last_updated: '2019-11-14T17:19:02.000Z'
        }
      }
    },
    {
      id: 213,
      name: 'MonaCoin',
      symbol: 'MONA',
      slug: 'monacoin',
      num_market_pairs: 22,
      date_added: '2014-03-20T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: null,
      circulating_supply: 65729674.8711679,
      total_supply: 65729674.8711679,
      platform: null,
      cmc_rank: 74,
      last_updated: '2019-11-14T17:19:01.000Z',
      quote: {
        USD: {
          price: 1.07921935654,
          volume_24h: 731109.928185173,
          percent_change_1h: 0.277654,
          percent_change_24h: -0.544759,
          percent_change_7d: -1.55336,
          market_cap: 70936737.42004523,
          last_updated: '2019-11-14T17:19:01.000Z'
        }
      }
    },
    {
      id: 2874,
      name: 'Aurora',
      symbol: 'AOA',
      slug: 'aurora',
      num_market_pairs: 12,
      date_added: '2018-06-26T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 6542330148.20859,
      total_supply: 10000000000,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x9ab165d795019b6d8b3e971dda91071421305e5a'
      },
      cmc_rank: 75,
      last_updated: '2019-11-14T17:19:10.000Z',
      quote: {
        USD: {
          price: 0.0104497204845,
          volume_24h: 2960410.65684044,
          percent_change_1h: -1.57197,
          percent_change_24h: 6.64127,
          percent_change_7d: 26.8791,
          market_cap: 68365521.36609723,
          last_updated: '2019-11-14T17:19:10.000Z'
        }
      }
    },
    {
      id: 1700,
      name: 'Aeternity',
      symbol: 'AE',
      slug: 'aeternity',
      num_market_pairs: 52,
      date_added: '2017-06-01T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: null,
      circulating_supply: 290876006.5,
      total_supply: 336696950.49932,
      platform: null,
      cmc_rank: 76,
      last_updated: '2019-11-14T17:19:03.000Z',
      quote: {
        USD: {
          price: 0.233948218201,
          volume_24h: 41488125.2714752,
          percent_change_1h: 0.0732815,
          percent_change_24h: -1.95495,
          percent_change_7d: -2.15766,
          market_cap: 68049923.43809749,
          last_updated: '2019-11-14T17:19:03.000Z'
        }
      }
    },
    {
      id: 1776,
      name: 'MCO',
      symbol: 'MCO',
      slug: 'crypto-com',
      num_market_pairs: 60,
      date_added: '2017-07-03T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 15793831.0949625,
      total_supply: 31587682.3632061,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0xb63b606ac810a52cca15e44bb630fd42d8d1d83d'
      },
      cmc_rank: 77,
      last_updated: '2019-11-14T17:19:04.000Z',
      quote: {
        USD: {
          price: 4.27296189469,
          volume_24h: 12366386.2423554,
          percent_change_1h: 0.393739,
          percent_change_24h: -3.35356,
          percent_change_7d: -1.37734,
          market_cap: 67486438.4399448,
          last_updated: '2019-11-14T17:19:04.000Z'
        }
      }
    },
    {
      id: 693,
      name: 'Verge',
      symbol: 'XVG',
      slug: 'verge',
      num_market_pairs: 58,
      date_added: '2014-10-25T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: 16555000000,
      circulating_supply: 16050474149.1206,
      total_supply: 16050474149.1206,
      platform: null,
      cmc_rank: 78,
      last_updated: '2019-11-14T17:19:02.000Z',
      quote: {
        USD: {
          price: 0.00420270603172,
          volume_24h: 2342166.97844985,
          percent_change_1h: 0.9058,
          percent_change_24h: 7.28472,
          percent_change_7d: 6.14418,
          market_cap: 67455424.51847509,
          last_updated: '2019-11-14T17:19:02.000Z'
        }
      }
    },
    {
      id: 372,
      name: 'Bytecoin',
      symbol: 'BCN',
      slug: 'bytecoin-bcn',
      num_market_pairs: 11,
      date_added: '2014-06-17T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: 184470000000,
      circulating_supply: 184066828814.059,
      total_supply: 184066828814.059,
      platform: null,
      cmc_rank: 79,
      last_updated: '2019-11-14T17:19:01.000Z',
      quote: {
        USD: {
          price: 0.000362773769965,
          volume_24h: 12750.6234985477,
          percent_change_1h: -11.6938,
          percent_change_24h: -10.1936,
          percent_change_7d: -12.1681,
          market_cap: 66774617.41437847,
          last_updated: '2019-11-14T17:19:01.000Z'
        }
      }
    },
    {
      id: 291,
      name: 'MaidSafeCoin',
      symbol: 'MAID',
      slug: 'maidsafecoin',
      num_market_pairs: 6,
      date_added: '2014-04-28T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 452552412,
      total_supply: 452552412,
      platform: {
        id: 83,
        name: 'Omni',
        symbol: 'OMNI',
        slug: 'omni',
        token_address: '3'
      },
      cmc_rank: 80,
      last_updated: '2019-11-14T17:19:01.000Z',
      quote: {
        USD: {
          price: 0.131300404316,
          volume_24h: 279632.797109903,
          percent_change_1h: 0.0198388,
          percent_change_24h: -1.03532,
          percent_change_7d: -2.07715,
          market_cap: 59420314.669781014,
          last_updated: '2019-11-14T17:19:01.000Z'
        }
      }
    },
    {
      id: 2694,
      name: 'Nexo',
      symbol: 'NEXO',
      slug: 'nexo',
      num_market_pairs: 23,
      date_added: '2018-05-01T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 560000011,
      total_supply: 1000000000,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0xb62132e35a6c13ee1ee0f84dc5d40bad8d815206'
      },
      cmc_rank: 81,
      last_updated: '2019-11-14T17:19:09.000Z',
      quote: {
        USD: {
          price: 0.105588363604,
          volume_24h: 10704351.2007679,
          percent_change_1h: -0.540897,
          percent_change_24h: -1.1472,
          percent_change_7d: 3.75164,
          market_cap: 59129484.779712,
          last_updated: '2019-11-14T17:19:09.000Z'
        }
      }
    },
    {
      id: 1637,
      name: 'iExec RLC',
      symbol: 'RLC',
      slug: 'rlc',
      num_market_pairs: 16,
      date_added: '2017-04-20T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 80070793.2387674,
      total_supply: 86999784.9868455,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x607f4c5bb672230e8672085532f7e901544a7375'
      },
      cmc_rank: 82,
      last_updated: '2019-11-14T17:19:03.000Z',
      quote: {
        USD: {
          price: 0.728087031603,
          volume_24h: 876820.142822289,
          percent_change_1h: 2.04017,
          percent_change_24h: -3.39512,
          percent_change_7d: 9.27549,
          market_cap: 58298506.16731172,
          last_updated: '2019-11-14T17:19:03.000Z'
        }
      }
    },
    {
      id: 4066,
      name: 'Chiliz',
      symbol: 'CHZ',
      slug: 'chiliz',
      num_market_pairs: 10,
      date_added: '2019-07-01T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 3762769182,
      total_supply: 8888888888,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x3506424f91fd33084466f402d5d97f05f8e3b4af'
      },
      cmc_rank: 83,
      last_updated: '2019-11-14T17:19:14.000Z',
      quote: {
        USD: {
          price: 0.0146632441092,
          volume_24h: 10959563.0568137,
          percent_change_1h: 1.53995,
          percent_change_24h: -0.853687,
          percent_change_7d: 6.22108,
          market_cap: 55174403.042240806,
          last_updated: '2019-11-14T17:19:14.000Z'
        }
      }
    },
    {
      id: 3673,
      name: 'BitMax Token',
      symbol: 'BTMX',
      slug: 'bitmax-token',
      num_market_pairs: 5,
      date_added: '2019-01-08T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 787683608,
      total_supply: 787683608,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x1c289a12a8552b314d0d153d6991fd27a54aa640'
      },
      cmc_rank: 84,
      last_updated: '2019-11-14T17:19:13.000Z',
      quote: {
        USD: {
          price: 0.069590220441,
          volume_24h: 3415981.73886417,
          percent_change_1h: -0.0422521,
          percent_change_24h: 0.69243,
          percent_change_7d: 0.900964,
          market_cap: 54815075.91848223,
          last_updated: '2019-11-14T17:19:13.000Z'
        }
      }
    },
    {
      id: 1320,
      name: 'Ardor',
      symbol: 'ARDR',
      slug: 'ardor',
      num_market_pairs: 25,
      date_added: '2016-07-23T00:00:00.000Z',
      tags: [],
      max_supply: 998999495,
      circulating_supply: 998999495,
      total_supply: 998999495,
      platform: null,
      cmc_rank: 85,
      last_updated: '2019-11-14T17:19:02.000Z',
      quote: {
        USD: {
          price: 0.0539407005825,
          volume_24h: 2429053.59180746,
          percent_change_1h: 0.521378,
          percent_change_24h: -3.33477,
          percent_change_7d: -1.95035,
          market_cap: 53886732.641863704,
          last_updated: '2019-11-14T17:19:02.000Z'
        }
      }
    },
    {
      id: 1230,
      name: 'Steem',
      symbol: 'STEEM',
      slug: 'steem',
      num_market_pairs: 33,
      date_added: '2016-04-18T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 350975727.118,
      total_supply: 367949821.118,
      platform: null,
      cmc_rank: 86,
      last_updated: '2019-11-14T17:19:02.000Z',
      quote: {
        USD: {
          price: 0.147945547793,
          volume_24h: 1418555.95885934,
          percent_change_1h: 0.72755,
          percent_change_24h: 5.04298,
          percent_change_7d: 3.2383,
          market_cap: 51925296.21051899,
          last_updated: '2019-11-14T17:19:02.000Z'
        }
      }
    },
    {
      id: 2469,
      name: 'Zilliqa',
      symbol: 'ZIL',
      slug: 'zilliqa',
      num_market_pairs: 107,
      date_added: '2018-01-25T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: null,
      circulating_supply: 8687360058.0875,
      total_supply: 12533042434.6081,
      platform: null,
      cmc_rank: 87,
      last_updated: '2019-11-14T17:19:08.000Z',
      quote: {
        USD: {
          price: 0.00589419753286,
          volume_24h: 7779578.43360119,
          percent_change_1h: -0.121498,
          percent_change_24h: 0.430599,
          percent_change_7d: -8.89826,
          market_cap: 51205016.22144585,
          last_updated: '2019-11-14T17:19:08.000Z'
        }
      }
    },
    {
      id: 3218,
      name: 'Energi',
      symbol: 'NRG',
      slug: 'energi',
      num_market_pairs: 6,
      date_added: '2018-08-23T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: null,
      circulating_supply: 22632391.3599914,
      total_supply: 22632391.3599914,
      platform: null,
      cmc_rank: 88,
      last_updated: '2019-11-14T17:19:11.000Z',
      quote: {
        USD: {
          price: 2.25167379469,
          volume_24h: 493927.275462965,
          percent_change_1h: -0.0852351,
          percent_change_24h: -2.54587,
          percent_change_7d: -9.49503,
          market_cap: 50960762.536461,
          last_updated: '2019-11-14T17:19:11.000Z'
        }
      }
    },
    {
      id: 2130,
      name: 'Enjin Coin',
      symbol: 'ENJ',
      slug: 'enjin-coin',
      num_market_pairs: 52,
      date_added: '2017-11-01T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 784614641.564264,
      total_supply: 1000000000,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0xf629cbd94d3791c9250152bd8dfbdf380e2a3b9c'
      },
      cmc_rank: 89,
      last_updated: '2019-11-14T17:19:05.000Z',
      quote: {
        USD: {
          price: 0.0633864844482,
          volume_24h: 2429688.52784567,
          percent_change_1h: 0.844008,
          percent_change_24h: 3.18679,
          percent_change_7d: -2.81667,
          market_cap: 49733963.77534324,
          last_updated: '2019-11-14T17:19:05.000Z'
        }
      }
    },
    {
      id: 3701,
      name: 'RIF Token',
      symbol: 'RIF',
      slug: 'rif-token',
      num_market_pairs: 17,
      date_added: '2019-01-16T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 536980014.838103,
      total_supply: 1000000000,
      platform: {
        id: 3626,
        name: 'RSK Smart Bitcoin',
        symbol: 'RBTC',
        slug: 'rsk-smart-bitcoin',
        token_address: '0x2acc95758f8b5f583470ba265eb685a8f45fc9d5'
      },
      cmc_rank: 90,
      last_updated: '2019-11-14T17:19:13.000Z',
      quote: {
        USD: {
          price: 0.0890658213612,
          volume_24h: 24153342.1966185,
          percent_change_1h: 0.265002,
          percent_change_24h: 1.19472,
          percent_change_7d: -11.2476,
          market_cap: 47826566.07610501,
          last_updated: '2019-11-14T17:19:13.000Z'
        }
      }
    },
    {
      id: 2539,
      name: 'Ren',
      symbol: 'REN',
      slug: 'ren',
      num_market_pairs: 18,
      date_added: '2018-02-21T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 800503651.320919,
      total_supply: 1000000000,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x408e41876cccdc0f92210600ef50372656052a38'
      },
      cmc_rank: 91,
      last_updated: '2019-11-14T17:19:08.000Z',
      quote: {
        USD: {
          price: 0.0584977621429,
          volume_24h: 3250574.71617045,
          percent_change_1h: 0.457993,
          percent_change_24h: 0.616851,
          percent_change_7d: 1.50624,
          market_cap: 46827672.18949408,
          last_updated: '2019-11-14T17:19:08.000Z'
        }
      }
    },
    {
      id: 2299,
      name: 'aelf',
      symbol: 'ELF',
      slug: 'aelf',
      num_market_pairs: 80,
      date_added: '2017-12-21T00:00:00.000Z',
      tags: [],
      max_supply: 1000000000,
      circulating_supply: 544480199.986466,
      total_supply: 879999999.986466,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0xbf2179859fc6d5bee9bf9158632dc51678a4100e'
      },
      cmc_rank: 92,
      last_updated: '2019-11-14T17:19:06.000Z',
      quote: {
        USD: {
          price: 0.082925343063,
          volume_24h: 9980226.1311487,
          percent_change_1h: 0.0586024,
          percent_change_24h: 3.03105,
          percent_change_7d: -3.6135,
          market_cap: 45151207.37488855,
          last_updated: '2019-11-14T17:19:06.000Z'
        }
      }
    },
    {
      id: 1759,
      name: 'Status',
      symbol: 'SNT',
      slug: 'status',
      num_market_pairs: 90,
      date_added: '2017-06-28T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 3470483788,
      total_supply: 6804870174,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x744d70fdbe2ba4cf95131626614a1763df805b9e'
      },
      cmc_rank: 93,
      last_updated: '2019-11-14T17:19:04.000Z',
      quote: {
        USD: {
          price: 0.0128636778943,
          volume_24h: 189295378.477668,
          percent_change_1h: 0.166859,
          percent_change_24h: -2.47046,
          percent_change_7d: -4.63737,
          market_cap: 44643185.58622213,
          last_updated: '2019-11-14T17:19:04.000Z'
        }
      }
    },
    {
      id: 1455,
      name: 'Golem',
      symbol: 'GNT',
      slug: 'golem-network-tokens',
      num_market_pairs: 102,
      date_added: '2016-11-18T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 980050000,
      total_supply: 1000000000,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0xa74476443119A942dE498590Fe1f2454d7D4aC0d'
      },
      cmc_rank: 94,
      last_updated: '2019-11-14T17:19:03.000Z',
      quote: {
        USD: {
          price: 0.0451861933239,
          volume_24h: 4073738.21724285,
          percent_change_1h: 0.454571,
          percent_change_24h: -0.746898,
          percent_change_7d: 0.809992,
          market_cap: 44284728.76708819,
          last_updated: '2019-11-14T17:19:03.000Z'
        }
      }
    },
    {
      id: 2447,
      name: 'Crypterium',
      symbol: 'CRPT',
      slug: 'crpt',
      num_market_pairs: 9,
      date_added: '2018-01-22T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 84787740.0000001,
      total_supply: 99785291.1428572,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0x80a7e048f37a50500351c204cb407766fa3bae7f'
      },
      cmc_rank: 95,
      last_updated: '2019-11-14T17:19:07.000Z',
      quote: {
        USD: {
          price: 0.516717907413,
          volume_24h: 327114.003067805,
          percent_change_1h: 0.335653,
          percent_change_24h: -3.66475,
          percent_change_7d: -1.6735,
          market_cap: 43811343.58707757,
          last_updated: '2019-11-14T17:19:07.000Z'
        }
      }
    },
    {
      id: 4113,
      name: 'UNI COIN',
      symbol: 'UNI',
      slug: 'uni-coin',
      num_market_pairs: 1,
      date_added: '2019-07-17T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 12908691.9524922,
      total_supply: 1000000000,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0xe6877ea9c28fbdec631ffbc087956d0023a76bf2'
      },
      cmc_rank: 96,
      last_updated: '2019-11-14T17:19:14.000Z',
      quote: {
        USD: {
          price: 3.34693437722,
          volume_24h: 13293.6498873613,
          percent_change_1h: -0.109523,
          percent_change_24h: 51.3473,
          percent_change_7d: 134.263,
          market_cap: 43204544.860739306,
          last_updated: '2019-11-14T17:19:14.000Z'
        }
      }
    },
    {
      id: 3871,
      name: 'Newton',
      symbol: 'NEW',
      slug: 'newton',
      num_market_pairs: 12,
      date_added: '2019-04-17T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: null,
      circulating_supply: 11948333332,
      total_supply: 100000000000,
      platform: null,
      cmc_rank: 97,
      last_updated: '2019-11-14T17:19:13.000Z',
      quote: {
        USD: {
          price: 0.00357996439448,
          volume_24h: 4279643.015967,
          percent_change_1h: -0.401838,
          percent_change_24h: -1.27445,
          percent_change_7d: -0.645697,
          market_cap: 42774607.90193858,
          last_updated: '2019-11-14T17:19:13.000Z'
        }
      }
    },
    {
      id: 3617,
      name: 'ILCoin',
      symbol: 'ILC',
      slug: 'ilcoin',
      num_market_pairs: 26,
      date_added: '2018-11-26T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: 2500000000,
      circulating_supply: 333602487.370894,
      total_supply: 1287298907.43547,
      platform: null,
      cmc_rank: 98,
      last_updated: '2019-11-14T17:19:13.000Z',
      quote: {
        USD: {
          price: 0.124003876275,
          volume_24h: 516257.141577553,
          percent_change_1h: -0.629019,
          percent_change_24h: 1.3123,
          percent_change_7d: -3.73013,
          market_cap: 41368001.56897259,
          last_updated: '2019-11-14T17:19:13.000Z'
        }
      }
    },
    {
      id: 1414,
      name: 'Zcoin',
      symbol: 'XZC',
      slug: 'zcoin',
      num_market_pairs: 42,
      date_added: '2016-10-06T00:00:00.000Z',
      tags: [
        'mineable'
      ],
      max_supply: 21400000,
      circulating_supply: 8806168.32977799,
      total_supply: 21400000,
      platform: null,
      cmc_rank: 99,
      last_updated: '2019-11-14T17:19:03.000Z',
      quote: {
        USD: {
          price: 4.64103016906,
          volume_24h: 4136948.79433068,
          percent_change_1h: 0.25189,
          percent_change_24h: -2.9214,
          percent_change_7d: -7.31797,
          market_cap: 40869692.892320365,
          last_updated: '2019-11-14T17:19:03.000Z'
        }
      }
    },
    {
      id: 2603,
      name: 'Pundi X',
      symbol: 'NPXS',
      slug: 'pundi-x',
      num_market_pairs: 69,
      date_added: '2018-03-22T00:00:00.000Z',
      tags: [],
      max_supply: null,
      circulating_supply: 234162817276.377,
      total_supply: 259810708832.63,
      platform: {
        id: 1027,
        name: 'Ethereum',
        symbol: 'ETH',
        slug: 'ethereum',
        token_address: '0xa15c7ebe1f07caf6bff097d8a589fb8ac49ae5b3'
      },
      cmc_rank: 100,
      last_updated: '2019-11-14T17:19:08.000Z',
      quote: {
        USD: {
          price: 0.000173707576204,
          volume_24h: 1048281.88707193,
          percent_change_1h: -0.325988,
          percent_change_24h: -5.03868,
          percent_change_7d: -8.15014,
          market_cap: 40675855.42617959,
          last_updated: '2019-11-14T17:19:08.000Z'
        }
      }
    }
  ]
};

export const cyptoRankListDataMapped: any = [
  {
    id: 1,
    name: 'Bitcoin',
    symbol: 'BTC',
    slug: 'bitcoin',
    num_market_pairs: 7767,
    date_added: '2013-04-28T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: 21000000,
    circulating_supply: 18047212,
    total_supply: 18047212,
    platform: null,
    cmc_rank: 1,
    last_updated: '2019-11-14T17:19:36.000Z',
    quote: {
      USD: {
        price: 8703.34700024,
        volume_24h: 19144788627.865,
        percent_change_1h: -0.000483066,
        percent_change_24h: -1.21921,
        percent_change_7d: -5.87575,
        market_cap: 157071148422.89536,
        last_updated: '2019-11-14T17:19:36.000Z'
      }
    },
    quote_mapped: {
      price: 8703.34700024,
      volume_24h: 19144788627.865,
      percent_change_1h: -0.000483066,
      percent_change_24h: -1.21921,
      percent_change_7d: -5.87575,
      market_cap: 157071148422.89536,
      last_updated: '2019-11-14T17:19:36.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1027,
    name: 'Ethereum',
    symbol: 'ETH',
    slug: 'ethereum',
    num_market_pairs: 5385,
    date_added: '2015-08-07T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: null,
    circulating_supply: 108551470.499,
    total_supply: 108551470.499,
    platform: null,
    cmc_rank: 2,
    last_updated: '2019-11-14T17:19:26.000Z',
    quote: {
      USD: {
        price: 185.857460251,
        volume_24h: 7886413803.79585,
        percent_change_1h: -0.118249,
        percent_change_24h: -1.36861,
        percent_change_7d: -0.783408,
        market_cap: 20175100613.45549,
        last_updated: '2019-11-14T17:19:26.000Z'
      }
    },
    quote_mapped: {
      price: 185.857460251,
      volume_24h: 7886413803.79585,
      percent_change_1h: -0.118249,
      percent_change_24h: -1.36861,
      percent_change_7d: -0.783408,
      market_cap: 20175100613.45549,
      last_updated: '2019-11-14T17:19:26.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 52,
    name: 'XRP',
    symbol: 'XRP',
    slug: 'ripple',
    num_market_pairs: 508,
    date_added: '2013-08-04T00:00:00.000Z',
    tags: [],
    max_supply: 100000000000,
    circulating_supply: 43298481757,
    total_supply: 99991298961,
    platform: null,
    cmc_rank: 3,
    last_updated: '2019-11-14T17:19:05.000Z',
    quote: {
      USD: {
        price: 0.270335694433,
        volume_24h: 1321699729.77276,
        percent_change_1h: 0.0789596,
        percent_change_24h: -1.42293,
        percent_change_7d: -7.199,
        market_cap: 11705125133.673176,
        last_updated: '2019-11-14T17:19:05.000Z'
      }
    },
    quote_mapped: {
      price: 0.270335694433,
      volume_24h: 1321699729.77276,
      percent_change_1h: 0.0789596,
      percent_change_24h: -1.42293,
      percent_change_7d: -7.199,
      market_cap: 11705125133.673176,
      last_updated: '2019-11-14T17:19:05.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1831,
    name: 'Bitcoin Cash',
    symbol: 'BCH',
    slug: 'bitcoin-cash',
    num_market_pairs: 440,
    date_added: '2017-07-23T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: 21000000,
    circulating_supply: 18112775,
    total_supply: 18112775,
    platform: null,
    cmc_rank: 4,
    last_updated: '2019-11-14T17:19:06.000Z',
    quote: {
      USD: {
        price: 279.084210305,
        volume_24h: 1797374187.76927,
        percent_change_1h: -0.154205,
        percent_change_24h: -2.69489,
        percent_change_7d: -4.43991,
        market_cap: 5054989507.307146,
        last_updated: '2019-11-14T17:19:06.000Z'
      }
    },
    quote_mapped: {
      price: 279.084210305,
      volume_24h: 1797374187.76927,
      percent_change_1h: -0.154205,
      percent_change_24h: -2.69489,
      percent_change_7d: -4.43991,
      market_cap: 5054989507.307146,
      last_updated: '2019-11-14T17:19:06.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 825,
    name: 'Tether',
    symbol: 'USDT',
    slug: 'tether',
    num_market_pairs: 3845,
    date_added: '2015-02-25T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 4108044456.1,
    total_supply: 4207771504.46,
    platform: {
      id: 83,
      name: 'Omni',
      symbol: 'OMNI',
      slug: 'omni',
      token_address: '31'
    },
    cmc_rank: 5,
    last_updated: '2019-11-14T17:19:22.000Z',
    quote: {
      USD: {
        price: 1.00486599223,
        volume_24h: 22098636663.3047,
        percent_change_1h: -0.0406058,
        percent_change_24h: -0.063769,
        percent_change_7d: 0.0749722,
        market_cap: 4128034168.503877,
        last_updated: '2019-11-14T17:19:22.000Z'
      }
    },
    quote_mapped: {
      price: 1.00486599223,
      volume_24h: 22098636663.3047,
      percent_change_1h: -0.0406058,
      percent_change_24h: -0.063769,
      percent_change_7d: 0.0749722,
      market_cap: 4128034168.503877,
      last_updated: '2019-11-14T17:19:22.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2,
    name: 'Litecoin',
    symbol: 'LTC',
    slug: 'litecoin',
    num_market_pairs: 559,
    date_added: '2013-04-28T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: 84000000,
    circulating_supply: 63678695.7674216,
    total_supply: 63678695.7674216,
    platform: null,
    cmc_rank: 6,
    last_updated: '2019-11-14T17:19:04.000Z',
    quote: {
      USD: {
        price: 59.7298122875,
        volume_24h: 3080733309.48578,
        percent_change_1h: -0.0397039,
        percent_change_24h: -2.47738,
        percent_change_7d: -3.53109,
        market_cap: 3803516544.9009132,
        last_updated: '2019-11-14T17:19:04.000Z'
      }
    },
    quote_mapped: {
      price: 59.7298122875,
      volume_24h: 3080733309.48578,
      percent_change_1h: -0.0397039,
      percent_change_24h: -2.47738,
      percent_change_7d: -3.53109,
      market_cap: 3803516544.9009132,
      last_updated: '2019-11-14T17:19:04.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1839,
    name: 'Binance Coin',
    symbol: 'BNB',
    slug: 'binance-coin',
    num_market_pairs: 283,
    date_added: '2017-07-25T00:00:00.000Z',
    tags: [],
    max_supply: 187536713,
    circulating_supply: 155536713,
    total_supply: 187536713,
    platform: null,
    cmc_rank: 7,
    last_updated: '2019-11-14T17:19:06.000Z',
    quote: {
      USD: {
        price: 21.2613611208,
        volume_24h: 248453918.266481,
        percent_change_1h: 0.0162822,
        percent_change_24h: -0.465434,
        percent_change_7d: 3.7068,
        market_cap: 3306922222.635228,
        last_updated: '2019-11-14T17:19:06.000Z'
      }
    },
    quote_mapped: {
      price: 21.2613611208,
      volume_24h: 248453918.266481,
      percent_change_1h: 0.0162822,
      percent_change_24h: -0.465434,
      percent_change_7d: 3.7068,
      market_cap: 3306922222.635228,
      last_updated: '2019-11-14T17:19:06.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1765,
    name: 'EOS',
    symbol: 'EOS',
    slug: 'eos',
    num_market_pairs: 394,
    date_added: '2017-07-01T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 940148012.1661,
    total_supply: 1036848023.6355,
    platform: null,
    cmc_rank: 8,
    last_updated: '2019-11-14T17:19:06.000Z',
    quote: {
      USD: {
        price: 3.42274415829,
        volume_24h: 1807459217.2669,
        percent_change_1h: -0.119196,
        percent_change_24h: -1.31931,
        percent_change_7d: -2.07022,
        market_cap: 3217886116.5694747,
        last_updated: '2019-11-14T17:19:06.000Z'
      }
    },
    quote_mapped: {
      price: 3.42274415829,
      volume_24h: 1807459217.2669,
      percent_change_1h: -0.119196,
      percent_change_24h: -1.31931,
      percent_change_7d: -2.07022,
      market_cap: 3217886116.5694747,
      last_updated: '2019-11-14T17:19:06.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 3602,
    name: 'Bitcoin SV',
    symbol: 'BSV',
    slug: 'bitcoin-sv',
    num_market_pairs: 158,
    date_added: '2018-11-09T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: 21000000,
    circulating_supply: 18068415,
    total_supply: 18068415,
    platform: null,
    cmc_rank: 9,
    last_updated: '2019-11-14T17:19:13.000Z',
    quote: {
      USD: {
        price: 127.491108752,
        volume_24h: 529383809.92938,
        percent_change_1h: -0.12293,
        percent_change_24h: -0.785414,
        percent_change_7d: -4.20698,
        market_cap: 2303562261.741268,
        last_updated: '2019-11-14T17:19:13.000Z'
      }
    },
    quote_mapped: {
      price: 127.491108752,
      volume_24h: 529383809.92938,
      percent_change_1h: -0.12293,
      percent_change_24h: -0.785414,
      percent_change_7d: -4.20698,
      market_cap: 2303562261.741268,
      last_updated: '2019-11-14T17:19:13.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 512,
    name: 'Stellar',
    symbol: 'XLM',
    slug: 'stellar',
    num_market_pairs: 308,
    date_added: '2014-08-05T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 20054779553.7,
    total_supply: 50000000000,
    platform: null,
    cmc_rank: 10,
    last_updated: '2019-11-14T17:19:04.000Z',
    quote: {
      USD: {
        price: 0.0744336924305,
        volume_24h: 222584719.827058,
        percent_change_1h: 0.0103026,
        percent_change_24h: -3.15413,
        percent_change_7d: -1.23512,
        market_cap: 1492751293.0615861,
        last_updated: '2019-11-14T17:19:04.000Z'
      }
    },
    quote_mapped: {
      price: 0.0744336924305,
      volume_24h: 222584719.827058,
      percent_change_1h: 0.0103026,
      percent_change_24h: -3.15413,
      percent_change_7d: -1.23512,
      market_cap: 1492751293.0615861,
      last_updated: '2019-11-14T17:19:04.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1958,
    name: 'TRON',
    symbol: 'TRX',
    slug: 'tron',
    num_market_pairs: 300,
    date_added: '2017-09-13T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: null,
    circulating_supply: 66682072191.4,
    total_supply: 99281283754.3,
    platform: null,
    cmc_rank: 11,
    last_updated: '2019-11-14T17:19:07.000Z',
    quote: {
      USD: {
        price: 0.0194617276335,
        volume_24h: 1000620412.79918,
        percent_change_1h: 0.137923,
        percent_change_24h: -2.87613,
        percent_change_7d: -0.509061,
        market_cap: 1297748327.0264113,
        last_updated: '2019-11-14T17:19:07.000Z'
      }
    },
    quote_mapped: {
      price: 0.0194617276335,
      volume_24h: 1000620412.79918,
      percent_change_1h: 0.137923,
      percent_change_24h: -2.87613,
      percent_change_7d: -0.509061,
      market_cap: 1297748327.0264113,
      last_updated: '2019-11-14T17:19:07.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 328,
    name: 'Monero',
    symbol: 'XMR',
    slug: 'monero',
    num_market_pairs: 128,
    date_added: '2014-05-21T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: null,
    circulating_supply: 17307468.9035555,
    total_supply: 17307468.9035555,
    platform: null,
    cmc_rank: 12,
    last_updated: '2019-11-14T17:19:02.000Z',
    quote: {
      USD: {
        price: 65.6735521974,
        volume_24h: 217424305.386937,
        percent_change_1h: 0.321149,
        percent_change_24h: 2.10306,
        percent_change_7d: 3.55709,
        market_cap: 1136642962.4425294,
        last_updated: '2019-11-14T17:19:02.000Z'
      }
    },
    quote_mapped: {
      price: 65.6735521974,
      volume_24h: 217424305.386937,
      percent_change_1h: 0.321149,
      percent_change_24h: 2.10306,
      percent_change_7d: 3.55709,
      market_cap: 1136642962.4425294,
      last_updated: '2019-11-14T17:19:02.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2010,
    name: 'Cardano',
    symbol: 'ADA',
    slug: 'cardano',
    num_market_pairs: 117,
    date_added: '2017-10-01T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: 45000000000,
    circulating_supply: 25927070538,
    total_supply: 31112483745,
    platform: null,
    cmc_rank: 13,
    last_updated: '2019-11-14T17:19:05.000Z',
    quote: {
      USD: {
        price: 0.0429082581227,
        volume_24h: 54305327.8499488,
        percent_change_1h: 0.239275,
        percent_change_24h: -1.84336,
        percent_change_7d: -1.32969,
        market_cap: 1112485435.0099545,
        last_updated: '2019-11-14T17:19:05.000Z'
      }
    },
    quote_mapped: {
      price: 0.0429082581227,
      volume_24h: 54305327.8499488,
      percent_change_1h: 0.239275,
      percent_change_24h: -1.84336,
      percent_change_7d: -1.32969,
      market_cap: 1112485435.0099545,
      last_updated: '2019-11-14T17:19:05.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1975,
    name: 'Chainlink',
    symbol: 'LINK',
    slug: 'chainlink',
    num_market_pairs: 124,
    date_added: '2017-09-20T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 350000000,
    total_supply: 1000000000,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x514910771af9ca656af840dff83e8264ecf986ca'
    },
    cmc_rank: 14,
    last_updated: '2019-11-14T17:19:05.000Z',
    quote: {
      USD: {
        price: 3.14118465726,
        volume_24h: 202759931.212486,
        percent_change_1h: -0.12786,
        percent_change_24h: 4.63743,
        percent_change_7d: 16.5615,
        market_cap: 1099414630.0410001,
        last_updated: '2019-11-14T17:19:05.000Z'
      }
    },
    quote_mapped: {
      price: 3.14118465726,
      volume_24h: 202759931.212486,
      percent_change_1h: -0.12786,
      percent_change_24h: 4.63743,
      percent_change_7d: 16.5615,
      market_cap: 1099414630.0410001,
      last_updated: '2019-11-14T17:19:05.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 3957,
    name: 'UNUS SED LEO',
    symbol: 'LEO',
    slug: 'unus-sed-leo',
    num_market_pairs: 27,
    date_added: '2019-05-21T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 999498892.9,
    total_supply: 999498892.9,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x2af5d2ad76741191d15dfe7bf6ac92d4bd912ca3'
    },
    cmc_rank: 15,
    last_updated: '2019-11-14T17:19:14.000Z',
    quote: {
      USD: {
        price: 0.958404846259,
        volume_24h: 10234445.3806834,
        percent_change_1h: -0.0580285,
        percent_change_24h: -1.00714,
        percent_change_7d: -2.82432,
        market_cap: 957924582.7858652,
        last_updated: '2019-11-14T17:19:14.000Z'
      }
    },
    quote_mapped: {
      price: 0.958404846259,
      volume_24h: 10234445.3806834,
      percent_change_1h: -0.0580285,
      percent_change_24h: -1.00714,
      percent_change_7d: -2.82432,
      market_cap: 957924582.7858652,
      last_updated: '2019-11-14T17:19:14.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2502,
    name: 'Huobi Token',
    symbol: 'HT',
    slug: 'huobi-token',
    num_market_pairs: 128,
    date_added: '2018-02-03T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 241284046.971921,
    total_supply: 500000000,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x6f259637dcd74c767781e37bc6133cd6a68aa161'
    },
    cmc_rank: 16,
    last_updated: '2019-11-14T17:19:08.000Z',
    quote: {
      USD: {
        price: 3.80102963087,
        volume_24h: 171735166.431899,
        percent_change_1h: -0.141494,
        percent_change_24h: -1.63811,
        percent_change_7d: -4.464,
        market_cap: 917127811.9965006,
        last_updated: '2019-11-14T17:19:08.000Z'
      }
    },
    quote_mapped: {
      price: 3.80102963087,
      volume_24h: 171735166.431899,
      percent_change_1h: -0.141494,
      percent_change_24h: -1.63811,
      percent_change_7d: -4.464,
      market_cap: 917127811.9965006,
      last_updated: '2019-11-14T17:19:08.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1376,
    name: 'NEO',
    symbol: 'NEO',
    slug: 'neo',
    num_market_pairs: 229,
    date_added: '2016-09-08T00:00:00.000Z',
    tags: [],
    max_supply: 100000000,
    circulating_supply: 70538831,
    total_supply: 100000000,
    platform: null,
    cmc_rank: 17,
    last_updated: '2019-11-14T17:19:04.000Z',
    quote: {
      USD: {
        price: 12.7253145793,
        volume_24h: 588710499.752798,
        percent_change_1h: -0.689308,
        percent_change_24h: -1.95051,
        percent_change_7d: 15.9959,
        market_cap: 897628814.5310788,
        last_updated: '2019-11-14T17:19:04.000Z'
      }
    },
    quote_mapped: {
      price: 12.7253145793,
      volume_24h: 588710499.752798,
      percent_change_1h: -0.689308,
      percent_change_24h: -1.95051,
      percent_change_7d: 15.9959,
      market_cap: 897628814.5310788,
      last_updated: '2019-11-14T17:19:04.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2011,
    name: 'Tezos',
    symbol: 'XTZ',
    slug: 'tezos',
    num_market_pairs: 51,
    date_added: '2017-10-02T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 660373611.97278,
    total_supply: 801312599.488106,
    platform: null,
    cmc_rank: 18,
    last_updated: '2019-11-14T17:19:05.000Z',
    quote: {
      USD: {
        price: 1.22795946833,
        volume_24h: 33228052.9178442,
        percent_change_1h: 2.74473,
        percent_change_24h: 7.99419,
        percent_change_7d: 0.789334,
        market_cap: 810912029.4572566,
        last_updated: '2019-11-14T17:19:05.000Z'
      }
    },
    quote_mapped: {
      price: 1.22795946833,
      volume_24h: 33228052.9178442,
      percent_change_1h: 2.74473,
      percent_change_24h: 7.99419,
      percent_change_7d: 0.789334,
      market_cap: 810912029.4572566,
      last_updated: '2019-11-14T17:19:05.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 3794,
    name: 'Cosmos',
    symbol: 'ATOM',
    slug: 'cosmos',
    num_market_pairs: 103,
    date_added: '2019-03-14T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 190688439.2,
    total_supply: 237928230.821588,
    platform: null,
    cmc_rank: 19,
    last_updated: '2019-11-14T17:19:13.000Z',
    quote: {
      USD: {
        price: 3.94978238439,
        volume_24h: 146490140.488516,
        percent_change_1h: 0.385089,
        percent_change_24h: -2.10058,
        percent_change_7d: 4.77145,
        market_cap: 753177838.0589836,
        last_updated: '2019-11-14T17:19:13.000Z'
      }
    },
    quote_mapped: {
      price: 3.94978238439,
      volume_24h: 146490140.488516,
      percent_change_1h: 0.385089,
      percent_change_24h: -2.10058,
      percent_change_7d: 4.77145,
      market_cap: 753177838.0589836,
      last_updated: '2019-11-14T17:19:13.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1720,
    name: 'IOTA',
    symbol: 'MIOTA',
    slug: 'iota',
    num_market_pairs: 53,
    date_added: '2017-06-13T00:00:00.000Z',
    tags: [],
    max_supply: 2779530283,
    circulating_supply: 2779530283,
    total_supply: 2779530283,
    platform: null,
    cmc_rank: 20,
    last_updated: '2019-11-14T17:19:03.000Z',
    quote: {
      USD: {
        price: 0.265199718638,
        volume_24h: 11975935.5281368,
        percent_change_1h: -0.256846,
        percent_change_24h: 2.10332,
        percent_change_7d: -2.34331,
        market_cap: 737130648.9974005,
        last_updated: '2019-11-14T17:19:03.000Z'
      }
    },
    quote_mapped: {
      price: 0.265199718638,
      volume_24h: 11975935.5281368,
      percent_change_1h: -0.256846,
      percent_change_24h: 2.10332,
      percent_change_7d: -2.34331,
      market_cap: 737130648.9974005,
      last_updated: '2019-11-14T17:19:03.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1518,
    name: 'Maker',
    symbol: 'MKR',
    slug: 'maker',
    num_market_pairs: 79,
    date_added: '2017-01-29T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 1000000,
    total_supply: 1000000,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x9f8f72aa9304c8b593d555f12ef6589cc3a579a2'
    },
    cmc_rank: 21,
    last_updated: '2019-11-14T17:19:03.000Z',
    quote: {
      USD: {
        price: 645.301512572,
        volume_24h: 5310818.96852892,
        percent_change_1h: -0.438587,
        percent_change_24h: -2.18733,
        percent_change_7d: 3.34165,
        market_cap: 645301512.572,
        last_updated: '2019-11-14T17:19:03.000Z'
      }
    },
    quote_mapped: {
      price: 645.301512572,
      volume_24h: 5310818.96852892,
      percent_change_1h: -0.438587,
      percent_change_24h: -2.18733,
      percent_change_7d: 3.34165,
      market_cap: 645301512.572,
      last_updated: '2019-11-14T17:19:03.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 131,
    name: 'Dash',
    symbol: 'DASH',
    slug: 'dash',
    num_market_pairs: 276,
    date_added: '2014-02-14T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: 18900000,
    circulating_supply: 9153425.68719089,
    total_supply: 9153425.68719089,
    platform: null,
    cmc_rank: 22,
    last_updated: '2019-11-14T17:19:03.000Z',
    quote: {
      USD: {
        price: 69.3829322203,
        volume_24h: 352413826.45625,
        percent_change_1h: 0.25907,
        percent_change_24h: -1.25555,
        percent_change_7d: -4.73371,
        market_cap: 635091514.0379186,
        last_updated: '2019-11-14T17:19:03.000Z'
      }
    },
    quote_mapped: {
      price: 69.3829322203,
      volume_24h: 352413826.45625,
      percent_change_1h: 0.25907,
      percent_change_24h: -1.25555,
      percent_change_7d: -4.73371,
      market_cap: 635091514.0379186,
      last_updated: '2019-11-14T17:19:03.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1321,
    name: 'Ethereum Classic',
    symbol: 'ETC',
    slug: 'ethereum-classic',
    num_market_pairs: 223,
    date_added: '2016-07-24T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: 210000000,
    circulating_supply: 115077737,
    total_supply: 115077737,
    platform: null,
    cmc_rank: 23,
    last_updated: '2019-11-14T17:19:04.000Z',
    quote: {
      USD: {
        price: 4.74586299657,
        volume_24h: 605290644.698795,
        percent_change_1h: -0.133463,
        percent_change_24h: -2.65263,
        percent_change_7d: -8.63452,
        market_cap: 546143173.7573143,
        last_updated: '2019-11-14T17:19:04.000Z'
      }
    },
    quote_mapped: {
      price: 4.74586299657,
      volume_24h: 605290644.698795,
      percent_change_1h: -0.133463,
      percent_change_24h: -2.65263,
      percent_change_7d: -8.63452,
      market_cap: 546143173.7573143,
      last_updated: '2019-11-14T17:19:04.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2566,
    name: 'Ontology',
    symbol: 'ONT',
    slug: 'ontology',
    num_market_pairs: 107,
    date_added: '2018-03-08T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 587351170,
    total_supply: 1000000000,
    platform: null,
    cmc_rank: 24,
    last_updated: '2019-11-14T17:19:09.000Z',
    quote: {
      USD: {
        price: 0.884108420331,
        volume_24h: 158296166.456136,
        percent_change_1h: 0.168051,
        percent_change_24h: -4.22694,
        percent_change_7d: 0.289186,
        market_cap: 519282115.08826464,
        last_updated: '2019-11-14T17:19:09.000Z'
      }
    },
    quote_mapped: {
      price: 0.884108420331,
      volume_24h: 158296166.456136,
      percent_change_1h: 0.168051,
      percent_change_24h: -4.22694,
      percent_change_7d: 0.289186,
      market_cap: 519282115.08826464,
      last_updated: '2019-11-14T17:19:09.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 3635,
    name: 'Crypto.com Coin',
    symbol: 'CRO',
    slug: 'crypto-com-coin',
    num_market_pairs: 39,
    date_added: '2018-12-14T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 11666666666.6667,
    total_supply: 100000000000,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0xa0b73e1ff0b80914ab6fe0444e65848c4c34450b'
    },
    cmc_rank: 25,
    last_updated: '2019-11-14T17:19:13.000Z',
    quote: {
      USD: {
        price: 0.0380222304012,
        volume_24h: 23544350.1031139,
        percent_change_1h: -0.813447,
        percent_change_24h: 8.56536,
        percent_change_7d: -1.58872,
        market_cap: 443592688.0140013,
        last_updated: '2019-11-14T17:19:13.000Z'
      }
    },
    quote_mapped: {
      price: 0.0380222304012,
      volume_24h: 23544350.1031139,
      percent_change_1h: -0.813447,
      percent_change_24h: 8.56536,
      percent_change_7d: -1.58872,
      market_cap: 443592688.0140013,
      last_updated: '2019-11-14T17:19:13.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 3408,
    name: 'USD Coin',
    symbol: 'USDC',
    slug: 'usd-coin',
    num_market_pairs: 181,
    date_added: '2018-10-08T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 439016091.71118,
    total_supply: 441476124.53,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
    },
    cmc_rank: 26,
    last_updated: '2019-11-14T17:19:13.000Z',
    quote: {
      USD: {
        price: 1.00899888569,
        volume_24h: 207541951.758268,
        percent_change_1h: -0.0140131,
        percent_change_24h: 0.250654,
        percent_change_7d: 0.379812,
        market_cap: 442966747.3365595,
        last_updated: '2019-11-14T17:19:13.000Z'
      }
    },
    quote_mapped: {
      price: 1.00899888569,
      volume_24h: 207541951.758268,
      percent_change_1h: -0.0140131,
      percent_change_24h: 0.250654,
      percent_change_7d: 0.379812,
      market_cap: 442966747.3365595,
      last_updated: '2019-11-14T17:19:13.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 3077,
    name: 'VeChain',
    symbol: 'VET',
    slug: 'vechain',
    num_market_pairs: 83,
    date_added: '2017-08-22T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 55454734800,
    total_supply: 86712634466,
    platform: null,
    cmc_rank: 27,
    last_updated: '2019-11-14T17:19:11.000Z',
    quote: {
      USD: {
        price: 0.00701749758878,
        volume_24h: 111259136.659106,
        percent_change_1h: 1.128,
        percent_change_24h: 9.18344,
        percent_change_7d: 26.1079,
        market_cap: 389153467.74543434,
        last_updated: '2019-11-14T17:19:11.000Z'
      }
    },
    quote_mapped: {
      price: 0.00701749758878,
      volume_24h: 111259136.659106,
      percent_change_1h: 1.128,
      percent_change_24h: 9.18344,
      percent_change_7d: 26.1079,
      market_cap: 389153467.74543434,
      last_updated: '2019-11-14T17:19:11.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 873,
    name: 'NEM',
    symbol: 'XEM',
    slug: 'nem',
    num_market_pairs: 97,
    date_added: '2015-04-01T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 8999999999,
    total_supply: 8999999999,
    platform: null,
    cmc_rank: 28,
    last_updated: '2019-11-14T17:19:02.000Z',
    quote: {
      USD: {
        price: 0.0399408080654,
        volume_24h: 8067537.1714488,
        percent_change_1h: 0.393992,
        percent_change_24h: -0.373977,
        percent_change_7d: -4.52684,
        market_cap: 359467272.5486592,
        last_updated: '2019-11-14T17:19:02.000Z'
      }
    },
    quote_mapped: {
      price: 0.0399408080654,
      volume_24h: 8067537.1714488,
      percent_change_1h: 0.393992,
      percent_change_24h: -0.373977,
      percent_change_7d: -4.52684,
      market_cap: 359467272.5486592,
      last_updated: '2019-11-14T17:19:02.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1697,
    name: 'Basic Attention Token',
    symbol: 'BAT',
    slug: 'basic-attention-token',
    num_market_pairs: 161,
    date_added: '2017-06-01T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 1356386750.7463,
    total_supply: 1500000000,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x0d8775f648430679a709e98d2b0cb6250d2887ef'
    },
    cmc_rank: 29,
    last_updated: '2019-11-14T17:19:03.000Z',
    quote: {
      USD: {
        price: 0.261183562443,
        volume_24h: 78325937.3233626,
        percent_change_1h: -0.108171,
        percent_change_24h: 5.08739,
        percent_change_7d: 3.27686,
        market_cap: 354265923.61040413,
        last_updated: '2019-11-14T17:19:03.000Z'
      }
    },
    quote_mapped: {
      price: 0.261183562443,
      volume_24h: 78325937.3233626,
      percent_change_1h: -0.108171,
      percent_change_24h: 5.08739,
      percent_change_7d: 3.27686,
      market_cap: 354265923.61040413,
      last_updated: '2019-11-14T17:19:03.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 74,
    name: 'Dogecoin',
    symbol: 'DOGE',
    slug: 'dogecoin',
    num_market_pairs: 285,
    date_added: '2013-12-15T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: null,
    circulating_supply: 122050253672.368,
    total_supply: 122050253672.368,
    platform: null,
    cmc_rank: 30,
    last_updated: '2019-11-14T17:19:02.000Z',
    quote: {
      USD: {
        price: 0.00267239344558,
        volume_24h: 77174751.4863165,
        percent_change_1h: 0.0976646,
        percent_change_24h: -1.49589,
        percent_change_7d: -0.905857,
        market_cap: 326166297.9454125,
        last_updated: '2019-11-14T17:19:02.000Z'
      }
    },
    quote_mapped: {
      price: 0.00267239344558,
      volume_24h: 77174751.4863165,
      percent_change_1h: 0.0976646,
      percent_change_24h: -1.49589,
      percent_change_7d: -0.905857,
      market_cap: 326166297.9454125,
      last_updated: '2019-11-14T17:19:02.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1437,
    name: 'Zcash',
    symbol: 'ZEC',
    slug: 'zcash',
    num_market_pairs: 222,
    date_added: '2016-10-29T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: null,
    circulating_supply: 7850181.25,
    total_supply: 7850181.25,
    platform: null,
    cmc_rank: 31,
    last_updated: '2019-11-14T17:19:04.000Z',
    quote: {
      USD: {
        price: 36.564082997,
        volume_24h: 128511860.592501,
        percent_change_1h: 0.0573591,
        percent_change_24h: -1.2313,
        percent_change_7d: -5.71198,
        market_cap: 287034678.7664932,
        last_updated: '2019-11-14T17:19:04.000Z'
      }
    },
    quote_mapped: {
      price: 36.564082997,
      volume_24h: 128511860.592501,
      percent_change_1h: 0.0573591,
      percent_change_24h: -1.2313,
      percent_change_7d: -5.71198,
      market_cap: 287034678.7664932,
      last_updated: '2019-11-14T17:19:04.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 3330,
    name: 'Paxos Standard',
    symbol: 'PAX',
    slug: 'paxos-standard',
    num_market_pairs: 100,
    date_added: '2018-09-27T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 229434803.15936,
    total_supply: 229937967.28,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x8e870d67f660d95d5be530380d0ec0bd388289e1'
    },
    cmc_rank: 32,
    last_updated: '2019-11-14T17:19:12.000Z',
    quote: {
      USD: {
        price: 1.00921318196,
        volume_24h: 225420215.927795,
        percent_change_1h: 0.01158,
        percent_change_24h: 0.194425,
        percent_change_7d: 0.324308,
        market_cap: 231548627.74882397,
        last_updated: '2019-11-14T17:19:12.000Z'
      }
    },
    quote_mapped: {
      price: 1.00921318196,
      volume_24h: 225420215.927795,
      percent_change_1h: 0.01158,
      percent_change_24h: 0.194425,
      percent_change_7d: 0.324308,
      market_cap: 231548627.74882397,
      last_updated: '2019-11-14T17:19:12.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1168,
    name: 'Decred',
    symbol: 'DCR',
    slug: 'decred',
    num_market_pairs: 46,
    date_added: '2016-02-10T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: 21000000,
    circulating_supply: 10673680.3859787,
    total_supply: 10673680.3859787,
    platform: null,
    cmc_rank: 33,
    last_updated: '2019-11-14T17:19:02.000Z',
    quote: {
      USD: {
        price: 21.5742560283,
        volume_24h: 12021088.6172664,
        percent_change_1h: 0.795931,
        percent_change_24h: -2.22261,
        percent_change_7d: 2.96166,
        market_cap: 230276713.41134843,
        last_updated: '2019-11-14T17:19:02.000Z'
      }
    },
    quote_mapped: {
      price: 21.5742560283,
      volume_24h: 12021088.6172664,
      percent_change_1h: 0.795931,
      percent_change_24h: -2.22261,
      percent_change_7d: 2.96166,
      market_cap: 230276713.41134843,
      last_updated: '2019-11-14T17:19:02.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1684,
    name: 'Qtum',
    symbol: 'QTUM',
    slug: 'qtum',
    num_market_pairs: 173,
    date_added: '2017-05-24T00:00:00.000Z',
    tags: [],
    max_supply: 107822406,
    circulating_supply: 96163648.0484598,
    total_supply: 101913668,
    platform: null,
    cmc_rank: 34,
    last_updated: '2019-11-14T17:19:04.000Z',
    quote: {
      USD: {
        price: 2.28395259635,
        volume_24h: 340159675.490253,
        percent_change_1h: -0.082474,
        percent_change_24h: 0.113593,
        percent_change_7d: 3.51038,
        market_cap: 219633213.63476735,
        last_updated: '2019-11-14T17:19:04.000Z'
      }
    },
    quote_mapped: {
      price: 2.28395259635,
      volume_24h: 340159675.490253,
      percent_change_1h: -0.082474,
      percent_change_24h: 0.113593,
      percent_change_7d: 3.51038,
      market_cap: 219633213.63476735,
      last_updated: '2019-11-14T17:19:04.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 3662,
    name: 'HedgeTrade',
    symbol: 'HEDG',
    slug: 'hedgetrade',
    num_market_pairs: 8,
    date_added: '2019-01-03T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 288330855.057208,
    total_supply: 1000000000.05,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0xF1290473E210b2108A85237fbCd7b6eb42Cc654F'
    },
    cmc_rank: 35,
    last_updated: '2019-11-14T17:19:13.000Z',
    quote: {
      USD: {
        price: 0.714311759433,
        volume_24h: 202039.335637315,
        percent_change_1h: 0.278826,
        percent_change_24h: -1.02969,
        percent_change_7d: -6.22021,
        market_cap: 205958120.37473553,
        last_updated: '2019-11-14T17:19:13.000Z'
      }
    },
    quote_mapped: {
      price: 0.714311759433,
      volume_24h: 202039.335637315,
      percent_change_1h: 0.278826,
      percent_change_24h: -1.02969,
      percent_change_7d: -6.22021,
      market_cap: 205958120.37473553,
      last_updated: '2019-11-14T17:19:13.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1896,
    name: '0x',
    symbol: 'ZRX',
    slug: '0x',
    num_market_pairs: 183,
    date_added: '2017-08-16T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 602005040.598496,
    total_supply: 1000000000,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0xe41d2489571d322189246dafa5ebde1f4699f498'
    },
    cmc_rank: 36,
    last_updated: '2019-11-14T17:19:05.000Z',
    quote: {
      USD: {
        price: 0.290223821244,
        volume_24h: 19264547.7837357,
        percent_change_1h: -0.0178574,
        percent_change_24h: -3.65054,
        percent_change_7d: -4.51499,
        market_cap: 174716203.29064485,
        last_updated: '2019-11-14T17:19:05.000Z'
      }
    },
    quote_mapped: {
      price: 0.290223821244,
      volume_24h: 19264547.7837357,
      percent_change_1h: -0.0178574,
      percent_change_24h: -3.65054,
      percent_change_7d: -4.51499,
      market_cap: 174716203.29064485,
      last_updated: '2019-11-14T17:19:05.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2563,
    name: 'TrueUSD',
    symbol: 'TUSD',
    slug: 'trueusd',
    num_market_pairs: 155,
    date_added: '2018-03-06T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 168469805.5,
    total_supply: 168469805.5,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x0000000000085d4780B73119b644AE5ecd22b376'
    },
    cmc_rank: 37,
    last_updated: '2019-11-14T17:19:08.000Z',
    quote: {
      USD: {
        price: 1.00750164683,
        volume_24h: 160220250.830383,
        percent_change_1h: -0.0164196,
        percent_change_24h: 0.115517,
        percent_change_7d: 0.241001,
        market_cap: 169733606.4823798,
        last_updated: '2019-11-14T17:19:08.000Z'
      }
    },
    quote_mapped: {
      price: 1.00750164683,
      volume_24h: 160220250.830383,
      percent_change_1h: -0.0164196,
      percent_change_24h: 0.115517,
      percent_change_7d: 0.241001,
      market_cap: 169733606.4823798,
      last_updated: '2019-11-14T17:19:08.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2682,
    name: 'Holo',
    symbol: 'HOT',
    slug: 'holo',
    num_market_pairs: 63,
    date_added: '2018-04-29T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 161335841957.48,
    total_supply: 177619433541.141,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x6c6ee5e31d828de241282b9606c8e98ea48526e2'
    },
    cmc_rank: 38,
    last_updated: '2019-11-14T17:19:09.000Z',
    quote: {
      USD: {
        price: 0.00099335486586,
        volume_24h: 13047046.355954,
        percent_change_1h: -0.122473,
        percent_change_24h: -1.47878,
        percent_change_7d: 1.63688,
        market_cap: 160263743.6460827,
        last_updated: '2019-11-14T17:19:09.000Z'
      }
    },
    quote_mapped: {
      price: 0.00099335486586,
      volume_24h: 13047046.355954,
      percent_change_1h: -0.122473,
      percent_change_24h: -1.47878,
      percent_change_7d: 1.63688,
      market_cap: 160263743.6460827,
      last_updated: '2019-11-14T17:19:09.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2585,
    name: 'Centrality',
    symbol: 'CENNZ',
    slug: 'centrality',
    num_market_pairs: 2,
    date_added: '2018-03-13T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 1063737441.99351,
    total_supply: 1200000000,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x1122b6a0e00dce0563082b6e2953f3a943855c1f'
    },
    cmc_rank: 39,
    last_updated: '2019-11-14T17:19:08.000Z',
    quote: {
      USD: {
        price: 0.14922856159,
        volume_24h: 533481.655728298,
        percent_change_1h: 1.40245,
        percent_change_24h: 5.71743,
        percent_change_7d: 9.83472,
        market_cap: 158740008.37811756,
        last_updated: '2019-11-14T17:19:08.000Z'
      }
    },
    quote_mapped: {
      price: 0.14922856159,
      volume_24h: 533481.655728298,
      percent_change_1h: 1.40245,
      percent_change_24h: 5.71743,
      percent_change_7d: 9.83472,
      market_cap: 158740008.37811756,
      last_updated: '2019-11-14T17:19:08.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 3704,
    name: 'V Systems',
    symbol: 'VSYS',
    slug: 'v-systems',
    num_market_pairs: 33,
    date_added: '2019-03-05T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 1851441031,
    total_supply: 3766299495,
    platform: null,
    cmc_rank: 40,
    last_updated: '2019-11-14T17:19:13.000Z',
    quote: {
      USD: {
        price: 0.0788098615598,
        volume_24h: 3676659.52168286,
        percent_change_1h: -1.26127,
        percent_change_24h: -4.29563,
        percent_change_7d: 2.07716,
        market_cap: 145911811.33924338,
        last_updated: '2019-11-14T17:19:13.000Z'
      }
    },
    quote_mapped: {
      price: 0.0788098615598,
      volume_24h: 3676659.52168286,
      percent_change_1h: -1.26127,
      percent_change_24h: -4.29563,
      percent_change_7d: 2.07716,
      market_cap: 145911811.33924338,
      last_updated: '2019-11-14T17:19:13.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2083,
    name: 'Bitcoin Gold',
    symbol: 'BTG',
    slug: 'bitcoin-gold',
    num_market_pairs: 83,
    date_added: '2017-10-23T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: 21000000,
    circulating_supply: 17513923.589,
    total_supply: 17513923.589,
    platform: null,
    cmc_rank: 41,
    last_updated: '2019-11-14T17:19:05.000Z',
    quote: {
      USD: {
        price: 8.16726142442,
        volume_24h: 13382799.0233942,
        percent_change_1h: 0.816673,
        percent_change_24h: -4.03599,
        percent_change_7d: -7.63636,
        market_cap: 143040792.51867917,
        last_updated: '2019-11-14T17:19:05.000Z'
      }
    },
    quote_mapped: {
      price: 8.16726142442,
      volume_24h: 13382799.0233942,
      percent_change_1h: 0.816673,
      percent_change_24h: -4.03599,
      percent_change_7d: -7.63636,
      market_cap: 143040792.51867917,
      last_updated: '2019-11-14T17:19:05.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1808,
    name: 'OmiseGO',
    symbol: 'OMG',
    slug: 'omisego',
    num_market_pairs: 209,
    date_added: '2017-07-14T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 140245398.245133,
    total_supply: 140245398.245133,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0xd26114cd6EE289AccF82350c8d8487fedB8A0C07'
    },
    cmc_rank: 42,
    last_updated: '2019-11-14T17:19:04.000Z',
    quote: {
      USD: {
        price: 1.00295188373,
        volume_24h: 86480368.3870568,
        percent_change_1h: 1.11315,
        percent_change_24h: 0.621039,
        percent_change_7d: 1.5282,
        market_cap: 140659386.35442019,
        last_updated: '2019-11-14T17:19:04.000Z'
      }
    },
    quote_mapped: {
      price: 1.00295188373,
      volume_24h: 86480368.3870568,
      percent_change_1h: 1.11315,
      percent_change_24h: 0.621039,
      percent_change_7d: 1.5282,
      market_cap: 140659386.35442019,
      last_updated: '2019-11-14T17:19:04.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2577,
    name: 'Ravencoin',
    symbol: 'RVN',
    slug: 'ravencoin',
    num_market_pairs: 46,
    date_added: '2018-03-10T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: 21000000000,
    circulating_supply: 4853220000,
    total_supply: 4853220000,
    platform: null,
    cmc_rank: 43,
    last_updated: '2019-11-14T17:19:08.000Z',
    quote: {
      USD: {
        price: 0.0273829413945,
        volume_24h: 8738669.67785623,
        percent_change_1h: -0.0915151,
        percent_change_24h: -3.23968,
        percent_change_7d: -6.46153,
        market_cap: 132895438.83461529,
        last_updated: '2019-11-14T17:19:08.000Z'
      }
    },
    quote_mapped: {
      price: 0.0273829413945,
      volume_24h: 8738669.67785623,
      percent_change_1h: -0.0915151,
      percent_change_24h: -3.23968,
      percent_change_7d: -6.46153,
      market_cap: 132895438.83461529,
      last_updated: '2019-11-14T17:19:08.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 3351,
    name: 'ZB',
    symbol: 'ZB',
    slug: 'zb',
    num_market_pairs: 10,
    date_added: '2018-09-27T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 463288810,
    total_supply: 2100000000,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0xbd0793332e9fb844a52a205a233ef27a5b34b927'
    },
    cmc_rank: 44,
    last_updated: '2019-11-14T17:19:12.000Z',
    quote: {
      USD: {
        price: 0.284147334646,
        volume_24h: 319498073.269108,
        percent_change_1h: -0.654019,
        percent_change_24h: -3.49026,
        percent_change_7d: -5.60594,
        market_cap: 131642280.53281711,
        last_updated: '2019-11-14T17:19:12.000Z'
      }
    },
    quote_mapped: {
      price: 0.284147334646,
      volume_24h: 319498073.269108,
      percent_change_1h: -0.654019,
      percent_change_24h: -3.49026,
      percent_change_7d: -5.60594,
      market_cap: 131642280.53281711,
      last_updated: '2019-11-14T17:19:12.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1567,
    name: 'Nano',
    symbol: 'NANO',
    slug: 'nano',
    num_market_pairs: 49,
    date_added: '2017-03-06T00:00:00.000Z',
    tags: [],
    max_supply: 133248297.197,
    circulating_supply: 133248297.197,
    total_supply: 133248297.197,
    platform: null,
    cmc_rank: 45,
    last_updated: '2019-11-14T17:19:03.000Z',
    quote: {
      USD: {
        price: 0.961399343929,
        volume_24h: 4643384.02193062,
        percent_change_1h: 0.0326188,
        percent_change_24h: -6.29465,
        percent_change_7d: -5.26735,
        market_cap: 128104825.5048522,
        last_updated: '2019-11-14T17:19:03.000Z'
      }
    },
    quote_mapped: {
      price: 0.961399343929,
      volume_24h: 4643384.02193062,
      percent_change_1h: 0.0326188,
      percent_change_24h: -6.29465,
      percent_change_7d: -5.26735,
      market_cap: 128104825.5048522,
      last_updated: '2019-11-14T17:19:03.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2586,
    name: 'Synthetix Network Token',
    symbol: 'SNX',
    slug: 'synthetix-network-token',
    num_market_pairs: 7,
    date_added: '2018-03-14T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 145171922.540886,
    total_supply: 151923076.923077,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0xc011a72400e58ecd99ee497cf89e3775d4bd732f'
    },
    cmc_rank: 46,
    last_updated: '2019-11-14T17:19:08.000Z',
    quote: {
      USD: {
        price: 0.871894838106,
        volume_24h: 210069.967437464,
        percent_change_1h: 0.768054,
        percent_change_24h: 4.60924,
        percent_change_7d: 4.71075,
        market_cap: 126574649.90132259,
        last_updated: '2019-11-14T17:19:08.000Z'
      }
    },
    quote_mapped: {
      price: 0.871894838106,
      volume_24h: 210069.967437464,
      percent_change_1h: 0.768054,
      percent_change_24h: 4.60924,
      percent_change_7d: 4.71075,
      market_cap: 126574649.90132259,
      last_updated: '2019-11-14T17:19:08.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 3437,
    name: 'ABBC Coin',
    symbol: 'ABBC',
    slug: 'abbc-coin',
    num_market_pairs: 39,
    date_added: '2018-10-12T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: null,
    circulating_supply: 556626634.357622,
    total_supply: 1004488460.0178,
    platform: null,
    cmc_rank: 47,
    last_updated: '2019-11-14T17:19:12.000Z',
    quote: {
      USD: {
        price: 0.225848660634,
        volume_24h: 57986374.0379384,
        percent_change_1h: -0.614926,
        percent_change_24h: -1.1874,
        percent_change_7d: 7.47809,
        market_cap: 125713379.84288017,
        last_updated: '2019-11-14T17:19:12.000Z'
      }
    },
    quote_mapped: {
      price: 0.225848660634,
      volume_24h: 57986374.0379384,
      percent_change_1h: -0.614926,
      percent_change_24h: -1.1874,
      percent_change_7d: 7.47809,
      market_cap: 125713379.84288017,
      last_updated: '2019-11-14T17:19:12.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1104,
    name: 'Augur',
    symbol: 'REP',
    slug: 'augur',
    num_market_pairs: 74,
    date_added: '2015-10-27T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 11000000,
    total_supply: 11000000,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x1985365e9f78359a9b6ad760e32412f4a445e862'
    },
    cmc_rank: 48,
    last_updated: '2019-11-14T17:19:02.000Z',
    quote: {
      USD: {
        price: 11.3482723773,
        volume_24h: 7869870.15325117,
        percent_change_1h: -0.364333,
        percent_change_24h: -1.64031,
        percent_change_7d: 2.23878,
        market_cap: 124830996.15030001,
        last_updated: '2019-11-14T17:19:02.000Z'
      }
    },
    quote_mapped: {
      price: 11.3482723773,
      volume_24h: 7869870.15325117,
      percent_change_1h: -0.364333,
      percent_change_24h: -1.64031,
      percent_change_7d: 2.23878,
      market_cap: 124830996.15030001,
      last_updated: '2019-11-14T17:19:02.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 4030,
    name: 'Algorand',
    symbol: 'ALGO',
    slug: 'algorand',
    num_market_pairs: 58,
    date_added: '2019-06-20T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: null,
    circulating_supply: 432847251.163547,
    total_supply: 2964119094.16355,
    platform: null,
    cmc_rank: 49,
    last_updated: '2019-11-14T17:19:14.000Z',
    quote: {
      USD: {
        price: 0.282655735188,
        volume_24h: 114924665.36425,
        percent_change_1h: -0.512688,
        percent_change_24h: -1.57599,
        percent_change_7d: 3.71412,
        market_cap: 122346758.00173727,
        last_updated: '2019-11-14T17:19:14.000Z'
      }
    },
    quote_mapped: {
      price: 0.282655735188,
      volume_24h: 114924665.36425,
      percent_change_1h: -0.512688,
      percent_change_24h: -1.57599,
      percent_change_7d: 3.71412,
      market_cap: 122346758.00173727,
      last_updated: '2019-11-14T17:19:14.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1866,
    name: 'Bytom',
    symbol: 'BTM',
    slug: 'bytom',
    num_market_pairs: 50,
    date_added: '2017-08-08T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: null,
    circulating_supply: 1002499275,
    total_supply: 1407000000,
    platform: null,
    cmc_rank: 50,
    last_updated: '2019-11-14T17:19:04.000Z',
    quote: {
      USD: {
        price: 0.114387242552,
        volume_24h: 21918938.4754465,
        percent_change_1h: -0.0764098,
        percent_change_24h: -4.0292,
        percent_change_7d: -14.5533,
        market_cap: 114673127.72762915,
        last_updated: '2019-11-14T17:19:04.000Z'
      }
    },
    quote_mapped: {
      price: 0.114387242552,
      volume_24h: 21918938.4754465,
      percent_change_1h: -0.0764098,
      percent_change_24h: -4.0292,
      percent_change_7d: -14.5533,
      market_cap: 114673127.72762915,
      last_updated: '2019-11-14T17:19:04.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 4172,
    name: 'LUNA',
    symbol: 'LUNA',
    slug: 'luna',
    num_market_pairs: 10,
    date_added: '2019-07-26T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 287765804.430561,
    total_supply: 995859074.224591,
    platform: null,
    cmc_rank: 51,
    last_updated: '2019-11-14T17:19:14.000Z',
    quote: {
      USD: {
        price: 0.368644828993,
        volume_24h: 2116434.17142567,
        percent_change_1h: 2.0273,
        percent_change_24h: -2.69232,
        percent_change_7d: -17.639,
        market_cap: 106083375.76433724,
        last_updated: '2019-11-14T17:19:14.000Z'
      }
    },
    quote_mapped: {
      price: 0.368644828993,
      volume_24h: 2116434.17142567,
      percent_change_1h: 2.0273,
      percent_change_24h: -2.69232,
      percent_change_7d: -17.639,
      market_cap: 106083375.76433724,
      last_updated: '2019-11-14T17:19:14.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1521,
    name: 'Komodo',
    symbol: 'KMD',
    slug: 'komodo',
    num_market_pairs: 23,
    date_added: '2017-02-05T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: 200000000,
    circulating_supply: 116924893.55032,
    total_supply: 116924893.55032,
    platform: null,
    cmc_rank: 52,
    last_updated: '2019-11-14T17:19:03.000Z',
    quote: {
      USD: {
        price: 0.890067228521,
        volume_24h: 7224248.50983621,
        percent_change_1h: 0.26343,
        percent_change_24h: -7.08347,
        percent_change_7d: 26.3397,
        market_cap: 104071015.94744627,
        last_updated: '2019-11-14T17:19:03.000Z'
      }
    },
    quote_mapped: {
      price: 0.890067228521,
      volume_24h: 7224248.50983621,
      percent_change_1h: 0.26343,
      percent_change_24h: -7.08347,
      percent_change_7d: 26.3397,
      market_cap: 104071015.94744627,
      last_updated: '2019-11-14T17:19:03.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2308,
    name: 'Dai',
    symbol: 'DAI',
    slug: 'dai',
    num_market_pairs: 106,
    date_added: '2017-12-24T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 102663712.635708,
    total_supply: 102663712.635708,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x89d24a6b4ccb1b6faa2625fe562bdd9a23260359'
    },
    cmc_rank: 53,
    last_updated: '2019-11-14T17:19:06.000Z',
    quote: {
      USD: {
        price: 1.00907992554,
        volume_24h: 4653475.00459962,
        percent_change_1h: -0.119954,
        percent_change_24h: 0.150679,
        percent_change_7d: 0.152193,
        market_cap: 103595891.5021002,
        last_updated: '2019-11-14T17:19:06.000Z'
      }
    },
    quote_mapped: {
      price: 1.00907992554,
      volume_24h: 4653475.00459962,
      percent_change_1h: -0.119954,
      percent_change_24h: 0.150679,
      percent_change_7d: 0.152193,
      market_cap: 103595891.5021002,
      last_updated: '2019-11-14T17:19:06.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2087,
    name: 'KuCoin Shares',
    symbol: 'KCS',
    slug: 'kucoin-shares',
    num_market_pairs: 15,
    date_added: '2017-10-24T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 82363551,
    total_supply: 172363551,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x039b5649a59967e3e936d7471f9c3700100ee1ab'
    },
    cmc_rank: 54,
    last_updated: '2019-11-14T17:19:05.000Z',
    quote: {
      USD: {
        price: 1.17652174502,
        volume_24h: 5131318.36036234,
        percent_change_1h: -0.169988,
        percent_change_24h: -2.1115,
        percent_change_7d: -12.2981,
        market_cap: 96902508.74856377,
        last_updated: '2019-11-14T17:19:05.000Z'
      }
    },
    quote_mapped: {
      price: 1.17652174502,
      volume_24h: 5131318.36036234,
      percent_change_1h: -0.169988,
      percent_change_24h: -2.1115,
      percent_change_7d: -12.2981,
      market_cap: 96902508.74856377,
      last_updated: '2019-11-14T17:19:05.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2453,
    name: 'EDUCare',
    symbol: 'EKT',
    slug: 'educare',
    num_market_pairs: 6,
    date_added: '2018-01-23T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 950000000,
    total_supply: 1000000000,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x4ecdb6385f3db3847f9c4a9bf3f9917bb27a5452'
    },
    cmc_rank: 55,
    last_updated: '2019-11-14T17:19:07.000Z',
    quote: {
      USD: {
        price: 0.101789103989,
        volume_24h: 5645027.61525481,
        percent_change_1h: -0.291799,
        percent_change_24h: -0.432927,
        percent_change_7d: -15.3125,
        market_cap: 96699648.78954999,
        last_updated: '2019-11-14T17:19:07.000Z'
      }
    },
    quote_mapped: {
      price: 0.101789103989,
      volume_24h: 5645027.61525481,
      percent_change_1h: -0.291799,
      percent_change_24h: -0.432927,
      percent_change_7d: -15.3125,
      market_cap: 96699648.78954999,
      last_updated: '2019-11-14T17:19:07.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 4079,
    name: 'Silverway',
    symbol: 'SLV',
    slug: 'silverway',
    num_market_pairs: 5,
    date_added: '2019-07-03T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 100000000,
    total_supply: 1000000000,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x4c1c4957d22d8f373aed54d0853b090666f6f9de'
    },
    cmc_rank: 56,
    last_updated: '2019-11-14T17:19:14.000Z',
    quote: {
      USD: {
        price: 0.943169991688,
        volume_24h: 5664686.79536967,
        percent_change_1h: -0.301674,
        percent_change_24h: -4.79958,
        percent_change_7d: -13.4343,
        market_cap: 94316999.1688,
        last_updated: '2019-11-14T17:19:14.000Z'
      }
    },
    quote_mapped: {
      price: 0.943169991688,
      volume_24h: 5664686.79536967,
      percent_change_1h: -0.301674,
      percent_change_24h: -4.79958,
      percent_change_7d: -13.4343,
      market_cap: 94316999.1688,
      last_updated: '2019-11-14T17:19:14.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1214,
    name: 'Lisk',
    symbol: 'LSK',
    slug: 'lisk',
    num_market_pairs: 58,
    date_added: '2016-04-06T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 121484888.512821,
    total_supply: 136517040,
    platform: null,
    cmc_rank: 57,
    last_updated: '2019-11-14T17:19:03.000Z',
    quote: {
      USD: {
        price: 0.767653458646,
        volume_24h: 1435367.87707622,
        percent_change_1h: 0.815396,
        percent_change_24h: 1.90818,
        percent_change_7d: -4.14559,
        market_cap: 93258294.84009075,
        last_updated: '2019-11-14T17:19:03.000Z'
      }
    },
    quote_mapped: {
      price: 0.767653458646,
      volume_24h: 1435367.87707622,
      percent_change_1h: 0.815396,
      percent_change_24h: 1.90818,
      percent_change_7d: -4.14559,
      market_cap: 93258294.84009075,
      last_updated: '2019-11-14T17:19:03.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 3139,
    name: 'DxChain Token',
    symbol: 'DX',
    slug: 'dxchain-token',
    num_market_pairs: 8,
    date_added: '2018-08-10T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 49999999999.6862,
    total_supply: 100000000000,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x973e52691176d36453868D9d86572788d27041A9'
    },
    cmc_rank: 58,
    last_updated: '2019-11-14T17:19:11.000Z',
    quote: {
      USD: {
        price: 0.00185786020029,
        volume_24h: 4041163.87848336,
        percent_change_1h: -0.4364,
        percent_change_24h: -1.96124,
        percent_change_7d: 87.7603,
        market_cap: 92893010.01391701,
        last_updated: '2019-11-14T17:19:11.000Z'
      }
    },
    quote_mapped: {
      price: 0.00185786020029,
      volume_24h: 4041163.87848336,
      percent_change_1h: -0.4364,
      percent_change_24h: -1.96124,
      percent_change_7d: 87.7603,
      market_cap: 92893010.01391701,
      last_updated: '2019-11-14T17:19:11.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 3718,
    name: 'BitTorrent',
    symbol: 'BTT',
    slug: 'bittorrent',
    num_market_pairs: 131,
    date_added: '2019-01-31T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 212116500000,
    total_supply: 990000000000,
    platform: {
      id: 1958,
      name: 'TRON',
      symbol: 'TRX',
      slug: 'tron',
      token_address: '1002000'
    },
    cmc_rank: 59,
    last_updated: '2019-11-14T17:19:13.000Z',
    quote: {
      USD: {
        price: 0.000430242294731,
        volume_24h: 67703300.1067289,
        percent_change_1h: 0.474809,
        percent_change_24h: -1.76014,
        percent_change_7d: -5.70844,
        market_cap: 91261489.71030815,
        last_updated: '2019-11-14T17:19:13.000Z'
      }
    },
    quote_mapped: {
      price: 0.000430242294731,
      volume_24h: 67703300.1067289,
      percent_change_1h: 0.474809,
      percent_change_24h: -1.76014,
      percent_change_7d: -5.70844,
      market_cap: 91261489.71030815,
      last_updated: '2019-11-14T17:19:13.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2222,
    name: 'Bitcoin Diamond',
    symbol: 'BCD',
    slug: 'bitcoin-diamond',
    num_market_pairs: 25,
    date_added: '2017-11-24T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: 210000000,
    circulating_supply: 186492897.953,
    total_supply: 189492897.953,
    platform: null,
    cmc_rank: 60,
    last_updated: '2019-11-14T17:19:05.000Z',
    quote: {
      USD: {
        price: 0.482864881857,
        volume_24h: 3386447.55642547,
        percent_change_1h: -0.782293,
        percent_change_24h: -3.64672,
        percent_change_7d: -7.37773,
        market_cap: 90050871.13724491,
        last_updated: '2019-11-14T17:19:05.000Z'
      }
    },
    quote_mapped: {
      price: 0.482864881857,
      volume_24h: 3386447.55642547,
      percent_change_1h: -0.782293,
      percent_change_24h: -3.64672,
      percent_change_7d: -7.37773,
      market_cap: 90050871.13724491,
      last_updated: '2019-11-14T17:19:05.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2830,
    name: 'Seele',
    symbol: 'SEELE',
    slug: 'seele',
    num_market_pairs: 15,
    date_added: '2018-05-31T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 696705193.289478,
    total_supply: 1000000000,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0xb1eef147028e9f480dbc5ccaa3277d417d1b85f0'
    },
    cmc_rank: 61,
    last_updated: '2019-11-14T17:19:10.000Z',
    quote: {
      USD: {
        price: 0.128145683794,
        volume_24h: 81790689.8384599,
        percent_change_1h: 7.61545,
        percent_change_24h: 29.8552,
        percent_change_7d: 57.7674,
        market_cap: 89279763.39691108,
        last_updated: '2019-11-14T17:19:10.000Z'
      }
    },
    quote_mapped: {
      price: 0.128145683794,
      volume_24h: 81790689.8384599,
      percent_change_1h: 7.61545,
      percent_change_24h: 29.8552,
      percent_change_7d: 57.7674,
      market_cap: 89279763.39691108,
      last_updated: '2019-11-14T17:19:10.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 109,
    name: 'DigiByte',
    symbol: 'DGB',
    slug: 'digibyte',
    num_market_pairs: 78,
    date_added: '2014-02-06T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: 21000000000,
    circulating_supply: 12489651681.3676,
    total_supply: 12489651681.3676,
    platform: null,
    cmc_rank: 62,
    last_updated: '2019-11-14T17:19:01.000Z',
    quote: {
      USD: {
        price: 0.00706055099112,
        volume_24h: 1282496.66561493,
        percent_change_1h: 0.618961,
        percent_change_24h: -1.60286,
        percent_change_7d: -0.411486,
        market_cap: 88183822.55762358,
        last_updated: '2019-11-14T17:19:01.000Z'
      }
    },
    quote_mapped: {
      price: 0.00706055099112,
      volume_24h: 1282496.66561493,
      percent_change_1h: 0.618961,
      percent_change_24h: -1.60286,
      percent_change_7d: -0.411486,
      market_cap: 88183822.55762358,
      last_updated: '2019-11-14T17:19:01.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1042,
    name: 'Siacoin',
    symbol: 'SC',
    slug: 'siacoin',
    num_market_pairs: 38,
    date_added: '2015-08-26T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: null,
    circulating_supply: 41817047634,
    total_supply: 41817047634,
    platform: null,
    cmc_rank: 63,
    last_updated: '2019-11-14T17:19:02.000Z',
    quote: {
      USD: {
        price: 0.00204217904664,
        volume_24h: 3194701.70379804,
        percent_change_1h: -0.336996,
        percent_change_24h: -1.61955,
        percent_change_7d: -1.27591,
        market_cap: 85397898.47050159,
        last_updated: '2019-11-14T17:19:02.000Z'
      }
    },
    quote_mapped: {
      price: 0.00204217904664,
      volume_24h: 3194701.70379804,
      percent_change_1h: -0.336996,
      percent_change_24h: -1.61955,
      percent_change_7d: -1.27591,
      market_cap: 85397898.47050159,
      last_updated: '2019-11-14T17:19:02.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1903,
    name: 'HyperCash',
    symbol: 'HC',
    slug: 'hypercash',
    num_market_pairs: 32,
    date_added: '2017-08-20T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: 84000000,
    circulating_supply: 44429162.497052,
    total_supply: 44429162.497052,
    platform: null,
    cmc_rank: 64,
    last_updated: '2019-11-14T17:19:04.000Z',
    quote: {
      USD: {
        price: 1.84307808436,
        volume_24h: 3252735.73156128,
        percent_change_1h: 0.283898,
        percent_change_24h: -2.42423,
        percent_change_7d: -3.97697,
        market_cap: 81886415.70478575,
        last_updated: '2019-11-14T17:19:04.000Z'
      }
    },
    quote_mapped: {
      price: 1.84307808436,
      volume_24h: 3252735.73156128,
      percent_change_1h: 0.283898,
      percent_change_24h: -2.42423,
      percent_change_7d: -3.97697,
      market_cap: 81886415.70478575,
      last_updated: '2019-11-14T17:19:04.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2099,
    name: 'ICON',
    symbol: 'ICX',
    slug: 'icon',
    num_market_pairs: 63,
    date_added: '2017-10-27T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 504397787.879946,
    total_supply: 800460000,
    platform: null,
    cmc_rank: 65,
    last_updated: '2019-11-14T17:19:05.000Z',
    quote: {
      USD: {
        price: 0.16025727921,
        volume_24h: 8304269.69959041,
        percent_change_1h: 0.133633,
        percent_change_24h: -5.43433,
        percent_change_7d: -10.2305,
        market_cap: 80833417.12518285,
        last_updated: '2019-11-14T17:19:05.000Z'
      }
    },
    quote_mapped: {
      price: 0.16025727921,
      volume_24h: 8304269.69959041,
      percent_change_1h: 0.133633,
      percent_change_24h: -5.43433,
      percent_change_7d: -10.2305,
      market_cap: 80833417.12518285,
      last_updated: '2019-11-14T17:19:05.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 3155,
    name: 'Quant',
    symbol: 'QNT',
    slug: 'quant',
    num_market_pairs: 18,
    date_added: '2018-08-10T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 12072738,
    total_supply: 14612493.0808262,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x4a220e6096b25eadb88358cb44068a3248254675'
    },
    cmc_rank: 66,
    last_updated: '2019-11-14T17:19:11.000Z',
    quote: {
      USD: {
        price: 6.67393219354,
        volume_24h: 21690141.5526024,
        percent_change_1h: 0.257392,
        percent_change_24h: 0.438803,
        percent_change_7d: -1.88358,
        market_cap: 80572634.8023737,
        last_updated: '2019-11-14T17:19:11.000Z'
      }
    },
    quote_mapped: {
      price: 6.67393219354,
      volume_24h: 21690141.5526024,
      percent_change_1h: 0.257392,
      percent_change_24h: 0.438803,
      percent_change_7d: -1.88358,
      market_cap: 80572634.8023737,
      last_updated: '2019-11-14T17:19:11.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 4279,
    name: 'Swipe',
    symbol: 'SXP',
    slug: 'swipe',
    num_market_pairs: 9,
    date_added: '2019-08-26T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 61135911.166,
    total_supply: 300000000,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x8ce9137d39326ad0cd6491fb5cc0cba0e089b6a9'
    },
    cmc_rank: 67,
    last_updated: '2019-11-14T17:19:15.000Z',
    quote: {
      USD: {
        price: 1.2857071888,
        volume_24h: 28078188.4908657,
        percent_change_1h: -0.0271596,
        percent_change_24h: -0.384172,
        percent_change_7d: 4.97729,
        market_cap: 78602880.47996439,
        last_updated: '2019-11-14T17:19:15.000Z'
      }
    },
    quote_mapped: {
      price: 1.2857071888,
      volume_24h: 28078188.4908657,
      percent_change_1h: -0.0271596,
      percent_change_24h: -0.384172,
      percent_change_7d: 4.97729,
      market_cap: 78602880.47996439,
      last_updated: '2019-11-14T17:19:15.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1274,
    name: 'Waves',
    symbol: 'WAVES',
    slug: 'waves',
    num_market_pairs: 142,
    date_added: '2016-06-02T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 100331806,
    total_supply: 100331806,
    platform: null,
    cmc_rank: 68,
    last_updated: '2019-11-14T17:19:03.000Z',
    quote: {
      USD: {
        price: 0.765414764024,
        volume_24h: 28535318.0436539,
        percent_change_1h: 0.00511462,
        percent_change_24h: -1.80687,
        percent_change_7d: -5.11944,
        market_cap: 76795445.61359175,
        last_updated: '2019-11-14T17:19:03.000Z'
      }
    },
    quote_mapped: {
      price: 0.765414764024,
      volume_24h: 28535318.0436539,
      percent_change_1h: 0.00511462,
      percent_change_24h: -1.80687,
      percent_change_7d: -5.11944,
      market_cap: 76795445.61359175,
      last_updated: '2019-11-14T17:19:03.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2416,
    name: 'THETA',
    symbol: 'THETA',
    slug: 'theta',
    num_market_pairs: 31,
    date_added: '2018-01-17T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 870502690,
    total_supply: 1000000000,
    platform: null,
    cmc_rank: 69,
    last_updated: '2019-11-14T17:19:07.000Z',
    quote: {
      USD: {
        price: 0.088162241137,
        volume_24h: 11123365.5073573,
        percent_change_1h: 0.933474,
        percent_change_24h: -2.28919,
        percent_change_7d: -3.78064,
        market_cap: 76745468.06618716,
        last_updated: '2019-11-14T17:19:07.000Z'
      }
    },
    quote_mapped: {
      price: 0.088162241137,
      volume_24h: 11123365.5073573,
      percent_change_1h: 0.933474,
      percent_change_24h: -2.28919,
      percent_change_7d: -3.78064,
      market_cap: 76745468.06618716,
      last_updated: '2019-11-14T17:19:07.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2405,
    name: 'IOST',
    symbol: 'IOST',
    slug: 'iostoken',
    num_market_pairs: 81,
    date_added: '2018-01-16T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 12013965608.8475,
    total_supply: 21000000000,
    platform: null,
    cmc_rank: 70,
    last_updated: '2019-11-14T17:19:07.000Z',
    quote: {
      USD: {
        price: 0.00635686938709,
        volume_24h: 30983423.9931561,
        percent_change_1h: -0.0352974,
        percent_change_24h: -2.58813,
        percent_change_7d: -3.15057,
        market_cap: 76371210.19643475,
        last_updated: '2019-11-14T17:19:07.000Z'
      }
    },
    quote_mapped: {
      price: 0.00635686938709,
      volume_24h: 30983423.9931561,
      percent_change_1h: -0.0352974,
      percent_change_24h: -2.58813,
      percent_change_7d: -3.15057,
      market_cap: 76371210.19643475,
      last_updated: '2019-11-14T17:19:07.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 4195,
    name: 'FTX Token',
    symbol: 'FTT',
    slug: 'ftx-token',
    num_market_pairs: 13,
    date_added: '2019-07-31T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 52631545.9327723,
    total_supply: 348503882,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x50d1c9771902476076ecfc8b2a83ad6b9355a4c9'
    },
    cmc_rank: 71,
    last_updated: '2019-11-14T17:19:14.000Z',
    quote: {
      USD: {
        price: 1.43951377585,
        volume_24h: 3250005.50551815,
        percent_change_1h: 0.265918,
        percent_change_24h: 2.43943,
        percent_change_7d: 5.95053,
        market_cap: 75763835.41450776,
        last_updated: '2019-11-14T17:19:14.000Z'
      }
    },
    quote_mapped: {
      price: 1.43951377585,
      volume_24h: 3250005.50551815,
      percent_change_1h: 0.265918,
      percent_change_24h: 2.43943,
      percent_change_7d: 5.95053,
      market_cap: 75763835.41450776,
      last_updated: '2019-11-14T17:19:14.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2907,
    name: 'Karatgold Coin',
    symbol: 'KBC',
    slug: 'karatgold-coin',
    num_market_pairs: 28,
    date_added: '2018-07-06T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 3752553044.13502,
    total_supply: 12000000000,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0xf3586684107ce0859c44aa2b2e0fb8cd8731a15a'
    },
    cmc_rank: 72,
    last_updated: '2019-11-14T17:19:10.000Z',
    quote: {
      USD: {
        price: 0.0199911295904,
        volume_24h: 2583948.55342107,
        percent_change_1h: 1.26425,
        percent_change_24h: -1.71818,
        percent_change_7d: -3.58221,
        market_cap: 75017774.20015319,
        last_updated: '2019-11-14T17:19:10.000Z'
      }
    },
    quote_mapped: {
      price: 0.0199911295904,
      volume_24h: 2583948.55342107,
      percent_change_1h: 1.26425,
      percent_change_24h: -1.71818,
      percent_change_7d: -3.58221,
      market_cap: 75017774.20015319,
      last_updated: '2019-11-14T17:19:10.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 463,
    name: 'BitShares',
    symbol: 'BTS',
    slug: 'bitshares',
    num_market_pairs: 85,
    date_added: '2014-07-21T00:00:00.000Z',
    tags: [],
    max_supply: 3600570502,
    circulating_supply: 2747920000,
    total_supply: 2747920000,
    platform: null,
    cmc_rank: 73,
    last_updated: '2019-11-14T17:19:02.000Z',
    quote: {
      USD: {
        price: 0.0265656296065,
        volume_24h: 1741773.02804758,
        percent_change_1h: 0.219969,
        percent_change_24h: -1.63621,
        percent_change_7d: -9.40575,
        market_cap: 73000224.90829349,
        last_updated: '2019-11-14T17:19:02.000Z'
      }
    },
    quote_mapped: {
      price: 0.0265656296065,
      volume_24h: 1741773.02804758,
      percent_change_1h: 0.219969,
      percent_change_24h: -1.63621,
      percent_change_7d: -9.40575,
      market_cap: 73000224.90829349,
      last_updated: '2019-11-14T17:19:02.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 213,
    name: 'MonaCoin',
    symbol: 'MONA',
    slug: 'monacoin',
    num_market_pairs: 22,
    date_added: '2014-03-20T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: null,
    circulating_supply: 65729674.8711679,
    total_supply: 65729674.8711679,
    platform: null,
    cmc_rank: 74,
    last_updated: '2019-11-14T17:19:01.000Z',
    quote: {
      USD: {
        price: 1.07921935654,
        volume_24h: 731109.928185173,
        percent_change_1h: 0.277654,
        percent_change_24h: -0.544759,
        percent_change_7d: -1.55336,
        market_cap: 70936737.42004523,
        last_updated: '2019-11-14T17:19:01.000Z'
      }
    },
    quote_mapped: {
      price: 1.07921935654,
      volume_24h: 731109.928185173,
      percent_change_1h: 0.277654,
      percent_change_24h: -0.544759,
      percent_change_7d: -1.55336,
      market_cap: 70936737.42004523,
      last_updated: '2019-11-14T17:19:01.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2874,
    name: 'Aurora',
    symbol: 'AOA',
    slug: 'aurora',
    num_market_pairs: 12,
    date_added: '2018-06-26T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 6542330148.20859,
    total_supply: 10000000000,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x9ab165d795019b6d8b3e971dda91071421305e5a'
    },
    cmc_rank: 75,
    last_updated: '2019-11-14T17:19:10.000Z',
    quote: {
      USD: {
        price: 0.0104497204845,
        volume_24h: 2960410.65684044,
        percent_change_1h: -1.57197,
        percent_change_24h: 6.64127,
        percent_change_7d: 26.8791,
        market_cap: 68365521.36609723,
        last_updated: '2019-11-14T17:19:10.000Z'
      }
    },
    quote_mapped: {
      price: 0.0104497204845,
      volume_24h: 2960410.65684044,
      percent_change_1h: -1.57197,
      percent_change_24h: 6.64127,
      percent_change_7d: 26.8791,
      market_cap: 68365521.36609723,
      last_updated: '2019-11-14T17:19:10.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1700,
    name: 'Aeternity',
    symbol: 'AE',
    slug: 'aeternity',
    num_market_pairs: 52,
    date_added: '2017-06-01T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: null,
    circulating_supply: 290876006.5,
    total_supply: 336696950.49932,
    platform: null,
    cmc_rank: 76,
    last_updated: '2019-11-14T17:19:03.000Z',
    quote: {
      USD: {
        price: 0.233948218201,
        volume_24h: 41488125.2714752,
        percent_change_1h: 0.0732815,
        percent_change_24h: -1.95495,
        percent_change_7d: -2.15766,
        market_cap: 68049923.43809749,
        last_updated: '2019-11-14T17:19:03.000Z'
      }
    },
    quote_mapped: {
      price: 0.233948218201,
      volume_24h: 41488125.2714752,
      percent_change_1h: 0.0732815,
      percent_change_24h: -1.95495,
      percent_change_7d: -2.15766,
      market_cap: 68049923.43809749,
      last_updated: '2019-11-14T17:19:03.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1776,
    name: 'MCO',
    symbol: 'MCO',
    slug: 'crypto-com',
    num_market_pairs: 60,
    date_added: '2017-07-03T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 15793831.0949625,
    total_supply: 31587682.3632061,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0xb63b606ac810a52cca15e44bb630fd42d8d1d83d'
    },
    cmc_rank: 77,
    last_updated: '2019-11-14T17:19:04.000Z',
    quote: {
      USD: {
        price: 4.27296189469,
        volume_24h: 12366386.2423554,
        percent_change_1h: 0.393739,
        percent_change_24h: -3.35356,
        percent_change_7d: -1.37734,
        market_cap: 67486438.4399448,
        last_updated: '2019-11-14T17:19:04.000Z'
      }
    },
    quote_mapped: {
      price: 4.27296189469,
      volume_24h: 12366386.2423554,
      percent_change_1h: 0.393739,
      percent_change_24h: -3.35356,
      percent_change_7d: -1.37734,
      market_cap: 67486438.4399448,
      last_updated: '2019-11-14T17:19:04.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 693,
    name: 'Verge',
    symbol: 'XVG',
    slug: 'verge',
    num_market_pairs: 58,
    date_added: '2014-10-25T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: 16555000000,
    circulating_supply: 16050474149.1206,
    total_supply: 16050474149.1206,
    platform: null,
    cmc_rank: 78,
    last_updated: '2019-11-14T17:19:02.000Z',
    quote: {
      USD: {
        price: 0.00420270603172,
        volume_24h: 2342166.97844985,
        percent_change_1h: 0.9058,
        percent_change_24h: 7.28472,
        percent_change_7d: 6.14418,
        market_cap: 67455424.51847509,
        last_updated: '2019-11-14T17:19:02.000Z'
      }
    },
    quote_mapped: {
      price: 0.00420270603172,
      volume_24h: 2342166.97844985,
      percent_change_1h: 0.9058,
      percent_change_24h: 7.28472,
      percent_change_7d: 6.14418,
      market_cap: 67455424.51847509,
      last_updated: '2019-11-14T17:19:02.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 372,
    name: 'Bytecoin',
    symbol: 'BCN',
    slug: 'bytecoin-bcn',
    num_market_pairs: 11,
    date_added: '2014-06-17T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: 184470000000,
    circulating_supply: 184066828814.059,
    total_supply: 184066828814.059,
    platform: null,
    cmc_rank: 79,
    last_updated: '2019-11-14T17:19:01.000Z',
    quote: {
      USD: {
        price: 0.000362773769965,
        volume_24h: 12750.6234985477,
        percent_change_1h: -11.6938,
        percent_change_24h: -10.1936,
        percent_change_7d: -12.1681,
        market_cap: 66774617.41437847,
        last_updated: '2019-11-14T17:19:01.000Z'
      }
    },
    quote_mapped: {
      price: 0.000362773769965,
      volume_24h: 12750.6234985477,
      percent_change_1h: -11.6938,
      percent_change_24h: -10.1936,
      percent_change_7d: -12.1681,
      market_cap: 66774617.41437847,
      last_updated: '2019-11-14T17:19:01.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 291,
    name: 'MaidSafeCoin',
    symbol: 'MAID',
    slug: 'maidsafecoin',
    num_market_pairs: 6,
    date_added: '2014-04-28T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 452552412,
    total_supply: 452552412,
    platform: {
      id: 83,
      name: 'Omni',
      symbol: 'OMNI',
      slug: 'omni',
      token_address: '3'
    },
    cmc_rank: 80,
    last_updated: '2019-11-14T17:19:01.000Z',
    quote: {
      USD: {
        price: 0.131300404316,
        volume_24h: 279632.797109903,
        percent_change_1h: 0.0198388,
        percent_change_24h: -1.03532,
        percent_change_7d: -2.07715,
        market_cap: 59420314.669781014,
        last_updated: '2019-11-14T17:19:01.000Z'
      }
    },
    quote_mapped: {
      price: 0.131300404316,
      volume_24h: 279632.797109903,
      percent_change_1h: 0.0198388,
      percent_change_24h: -1.03532,
      percent_change_7d: -2.07715,
      market_cap: 59420314.669781014,
      last_updated: '2019-11-14T17:19:01.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2694,
    name: 'Nexo',
    symbol: 'NEXO',
    slug: 'nexo',
    num_market_pairs: 23,
    date_added: '2018-05-01T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 560000011,
    total_supply: 1000000000,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0xb62132e35a6c13ee1ee0f84dc5d40bad8d815206'
    },
    cmc_rank: 81,
    last_updated: '2019-11-14T17:19:09.000Z',
    quote: {
      USD: {
        price: 0.105588363604,
        volume_24h: 10704351.2007679,
        percent_change_1h: -0.540897,
        percent_change_24h: -1.1472,
        percent_change_7d: 3.75164,
        market_cap: 59129484.779712,
        last_updated: '2019-11-14T17:19:09.000Z'
      }
    },
    quote_mapped: {
      price: 0.105588363604,
      volume_24h: 10704351.2007679,
      percent_change_1h: -0.540897,
      percent_change_24h: -1.1472,
      percent_change_7d: 3.75164,
      market_cap: 59129484.779712,
      last_updated: '2019-11-14T17:19:09.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1637,
    name: 'iExec RLC',
    symbol: 'RLC',
    slug: 'rlc',
    num_market_pairs: 16,
    date_added: '2017-04-20T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 80070793.2387674,
    total_supply: 86999784.9868455,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x607f4c5bb672230e8672085532f7e901544a7375'
    },
    cmc_rank: 82,
    last_updated: '2019-11-14T17:19:03.000Z',
    quote: {
      USD: {
        price: 0.728087031603,
        volume_24h: 876820.142822289,
        percent_change_1h: 2.04017,
        percent_change_24h: -3.39512,
        percent_change_7d: 9.27549,
        market_cap: 58298506.16731172,
        last_updated: '2019-11-14T17:19:03.000Z'
      }
    },
    quote_mapped: {
      price: 0.728087031603,
      volume_24h: 876820.142822289,
      percent_change_1h: 2.04017,
      percent_change_24h: -3.39512,
      percent_change_7d: 9.27549,
      market_cap: 58298506.16731172,
      last_updated: '2019-11-14T17:19:03.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 4066,
    name: 'Chiliz',
    symbol: 'CHZ',
    slug: 'chiliz',
    num_market_pairs: 10,
    date_added: '2019-07-01T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 3762769182,
    total_supply: 8888888888,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x3506424f91fd33084466f402d5d97f05f8e3b4af'
    },
    cmc_rank: 83,
    last_updated: '2019-11-14T17:19:14.000Z',
    quote: {
      USD: {
        price: 0.0146632441092,
        volume_24h: 10959563.0568137,
        percent_change_1h: 1.53995,
        percent_change_24h: -0.853687,
        percent_change_7d: 6.22108,
        market_cap: 55174403.042240806,
        last_updated: '2019-11-14T17:19:14.000Z'
      }
    },
    quote_mapped: {
      price: 0.0146632441092,
      volume_24h: 10959563.0568137,
      percent_change_1h: 1.53995,
      percent_change_24h: -0.853687,
      percent_change_7d: 6.22108,
      market_cap: 55174403.042240806,
      last_updated: '2019-11-14T17:19:14.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 3673,
    name: 'BitMax Token',
    symbol: 'BTMX',
    slug: 'bitmax-token',
    num_market_pairs: 5,
    date_added: '2019-01-08T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 787683608,
    total_supply: 787683608,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x1c289a12a8552b314d0d153d6991fd27a54aa640'
    },
    cmc_rank: 84,
    last_updated: '2019-11-14T17:19:13.000Z',
    quote: {
      USD: {
        price: 0.069590220441,
        volume_24h: 3415981.73886417,
        percent_change_1h: -0.0422521,
        percent_change_24h: 0.69243,
        percent_change_7d: 0.900964,
        market_cap: 54815075.91848223,
        last_updated: '2019-11-14T17:19:13.000Z'
      }
    },
    quote_mapped: {
      price: 0.069590220441,
      volume_24h: 3415981.73886417,
      percent_change_1h: -0.0422521,
      percent_change_24h: 0.69243,
      percent_change_7d: 0.900964,
      market_cap: 54815075.91848223,
      last_updated: '2019-11-14T17:19:13.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1320,
    name: 'Ardor',
    symbol: 'ARDR',
    slug: 'ardor',
    num_market_pairs: 25,
    date_added: '2016-07-23T00:00:00.000Z',
    tags: [],
    max_supply: 998999495,
    circulating_supply: 998999495,
    total_supply: 998999495,
    platform: null,
    cmc_rank: 85,
    last_updated: '2019-11-14T17:19:02.000Z',
    quote: {
      USD: {
        price: 0.0539407005825,
        volume_24h: 2429053.59180746,
        percent_change_1h: 0.521378,
        percent_change_24h: -3.33477,
        percent_change_7d: -1.95035,
        market_cap: 53886732.641863704,
        last_updated: '2019-11-14T17:19:02.000Z'
      }
    },
    quote_mapped: {
      price: 0.0539407005825,
      volume_24h: 2429053.59180746,
      percent_change_1h: 0.521378,
      percent_change_24h: -3.33477,
      percent_change_7d: -1.95035,
      market_cap: 53886732.641863704,
      last_updated: '2019-11-14T17:19:02.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1230,
    name: 'Steem',
    symbol: 'STEEM',
    slug: 'steem',
    num_market_pairs: 33,
    date_added: '2016-04-18T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 350975727.118,
    total_supply: 367949821.118,
    platform: null,
    cmc_rank: 86,
    last_updated: '2019-11-14T17:19:02.000Z',
    quote: {
      USD: {
        price: 0.147945547793,
        volume_24h: 1418555.95885934,
        percent_change_1h: 0.72755,
        percent_change_24h: 5.04298,
        percent_change_7d: 3.2383,
        market_cap: 51925296.21051899,
        last_updated: '2019-11-14T17:19:02.000Z'
      }
    },
    quote_mapped: {
      price: 0.147945547793,
      volume_24h: 1418555.95885934,
      percent_change_1h: 0.72755,
      percent_change_24h: 5.04298,
      percent_change_7d: 3.2383,
      market_cap: 51925296.21051899,
      last_updated: '2019-11-14T17:19:02.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2469,
    name: 'Zilliqa',
    symbol: 'ZIL',
    slug: 'zilliqa',
    num_market_pairs: 107,
    date_added: '2018-01-25T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: null,
    circulating_supply: 8687360058.0875,
    total_supply: 12533042434.6081,
    platform: null,
    cmc_rank: 87,
    last_updated: '2019-11-14T17:19:08.000Z',
    quote: {
      USD: {
        price: 0.00589419753286,
        volume_24h: 7779578.43360119,
        percent_change_1h: -0.121498,
        percent_change_24h: 0.430599,
        percent_change_7d: -8.89826,
        market_cap: 51205016.22144585,
        last_updated: '2019-11-14T17:19:08.000Z'
      }
    },
    quote_mapped: {
      price: 0.00589419753286,
      volume_24h: 7779578.43360119,
      percent_change_1h: -0.121498,
      percent_change_24h: 0.430599,
      percent_change_7d: -8.89826,
      market_cap: 51205016.22144585,
      last_updated: '2019-11-14T17:19:08.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 3218,
    name: 'Energi',
    symbol: 'NRG',
    slug: 'energi',
    num_market_pairs: 6,
    date_added: '2018-08-23T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: null,
    circulating_supply: 22632391.3599914,
    total_supply: 22632391.3599914,
    platform: null,
    cmc_rank: 88,
    last_updated: '2019-11-14T17:19:11.000Z',
    quote: {
      USD: {
        price: 2.25167379469,
        volume_24h: 493927.275462965,
        percent_change_1h: -0.0852351,
        percent_change_24h: -2.54587,
        percent_change_7d: -9.49503,
        market_cap: 50960762.536461,
        last_updated: '2019-11-14T17:19:11.000Z'
      }
    },
    quote_mapped: {
      price: 2.25167379469,
      volume_24h: 493927.275462965,
      percent_change_1h: -0.0852351,
      percent_change_24h: -2.54587,
      percent_change_7d: -9.49503,
      market_cap: 50960762.536461,
      last_updated: '2019-11-14T17:19:11.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2130,
    name: 'Enjin Coin',
    symbol: 'ENJ',
    slug: 'enjin-coin',
    num_market_pairs: 52,
    date_added: '2017-11-01T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 784614641.564264,
    total_supply: 1000000000,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0xf629cbd94d3791c9250152bd8dfbdf380e2a3b9c'
    },
    cmc_rank: 89,
    last_updated: '2019-11-14T17:19:05.000Z',
    quote: {
      USD: {
        price: 0.0633864844482,
        volume_24h: 2429688.52784567,
        percent_change_1h: 0.844008,
        percent_change_24h: 3.18679,
        percent_change_7d: -2.81667,
        market_cap: 49733963.77534324,
        last_updated: '2019-11-14T17:19:05.000Z'
      }
    },
    quote_mapped: {
      price: 0.0633864844482,
      volume_24h: 2429688.52784567,
      percent_change_1h: 0.844008,
      percent_change_24h: 3.18679,
      percent_change_7d: -2.81667,
      market_cap: 49733963.77534324,
      last_updated: '2019-11-14T17:19:05.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 3701,
    name: 'RIF Token',
    symbol: 'RIF',
    slug: 'rif-token',
    num_market_pairs: 17,
    date_added: '2019-01-16T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 536980014.838103,
    total_supply: 1000000000,
    platform: {
      id: 3626,
      name: 'RSK Smart Bitcoin',
      symbol: 'RBTC',
      slug: 'rsk-smart-bitcoin',
      token_address: '0x2acc95758f8b5f583470ba265eb685a8f45fc9d5'
    },
    cmc_rank: 90,
    last_updated: '2019-11-14T17:19:13.000Z',
    quote: {
      USD: {
        price: 0.0890658213612,
        volume_24h: 24153342.1966185,
        percent_change_1h: 0.265002,
        percent_change_24h: 1.19472,
        percent_change_7d: -11.2476,
        market_cap: 47826566.07610501,
        last_updated: '2019-11-14T17:19:13.000Z'
      }
    },
    quote_mapped: {
      price: 0.0890658213612,
      volume_24h: 24153342.1966185,
      percent_change_1h: 0.265002,
      percent_change_24h: 1.19472,
      percent_change_7d: -11.2476,
      market_cap: 47826566.07610501,
      last_updated: '2019-11-14T17:19:13.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2539,
    name: 'Ren',
    symbol: 'REN',
    slug: 'ren',
    num_market_pairs: 18,
    date_added: '2018-02-21T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 800503651.320919,
    total_supply: 1000000000,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x408e41876cccdc0f92210600ef50372656052a38'
    },
    cmc_rank: 91,
    last_updated: '2019-11-14T17:19:08.000Z',
    quote: {
      USD: {
        price: 0.0584977621429,
        volume_24h: 3250574.71617045,
        percent_change_1h: 0.457993,
        percent_change_24h: 0.616851,
        percent_change_7d: 1.50624,
        market_cap: 46827672.18949408,
        last_updated: '2019-11-14T17:19:08.000Z'
      }
    },
    quote_mapped: {
      price: 0.0584977621429,
      volume_24h: 3250574.71617045,
      percent_change_1h: 0.457993,
      percent_change_24h: 0.616851,
      percent_change_7d: 1.50624,
      market_cap: 46827672.18949408,
      last_updated: '2019-11-14T17:19:08.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2299,
    name: 'aelf',
    symbol: 'ELF',
    slug: 'aelf',
    num_market_pairs: 80,
    date_added: '2017-12-21T00:00:00.000Z',
    tags: [],
    max_supply: 1000000000,
    circulating_supply: 544480199.986466,
    total_supply: 879999999.986466,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0xbf2179859fc6d5bee9bf9158632dc51678a4100e'
    },
    cmc_rank: 92,
    last_updated: '2019-11-14T17:19:06.000Z',
    quote: {
      USD: {
        price: 0.082925343063,
        volume_24h: 9980226.1311487,
        percent_change_1h: 0.0586024,
        percent_change_24h: 3.03105,
        percent_change_7d: -3.6135,
        market_cap: 45151207.37488855,
        last_updated: '2019-11-14T17:19:06.000Z'
      }
    },
    quote_mapped: {
      price: 0.082925343063,
      volume_24h: 9980226.1311487,
      percent_change_1h: 0.0586024,
      percent_change_24h: 3.03105,
      percent_change_7d: -3.6135,
      market_cap: 45151207.37488855,
      last_updated: '2019-11-14T17:19:06.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1759,
    name: 'Status',
    symbol: 'SNT',
    slug: 'status',
    num_market_pairs: 90,
    date_added: '2017-06-28T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 3470483788,
    total_supply: 6804870174,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x744d70fdbe2ba4cf95131626614a1763df805b9e'
    },
    cmc_rank: 93,
    last_updated: '2019-11-14T17:19:04.000Z',
    quote: {
      USD: {
        price: 0.0128636778943,
        volume_24h: 189295378.477668,
        percent_change_1h: 0.166859,
        percent_change_24h: -2.47046,
        percent_change_7d: -4.63737,
        market_cap: 44643185.58622213,
        last_updated: '2019-11-14T17:19:04.000Z'
      }
    },
    quote_mapped: {
      price: 0.0128636778943,
      volume_24h: 189295378.477668,
      percent_change_1h: 0.166859,
      percent_change_24h: -2.47046,
      percent_change_7d: -4.63737,
      market_cap: 44643185.58622213,
      last_updated: '2019-11-14T17:19:04.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1455,
    name: 'Golem',
    symbol: 'GNT',
    slug: 'golem-network-tokens',
    num_market_pairs: 102,
    date_added: '2016-11-18T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 980050000,
    total_supply: 1000000000,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0xa74476443119A942dE498590Fe1f2454d7D4aC0d'
    },
    cmc_rank: 94,
    last_updated: '2019-11-14T17:19:03.000Z',
    quote: {
      USD: {
        price: 0.0451861933239,
        volume_24h: 4073738.21724285,
        percent_change_1h: 0.454571,
        percent_change_24h: -0.746898,
        percent_change_7d: 0.809992,
        market_cap: 44284728.76708819,
        last_updated: '2019-11-14T17:19:03.000Z'
      }
    },
    quote_mapped: {
      price: 0.0451861933239,
      volume_24h: 4073738.21724285,
      percent_change_1h: 0.454571,
      percent_change_24h: -0.746898,
      percent_change_7d: 0.809992,
      market_cap: 44284728.76708819,
      last_updated: '2019-11-14T17:19:03.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2447,
    name: 'Crypterium',
    symbol: 'CRPT',
    slug: 'crpt',
    num_market_pairs: 9,
    date_added: '2018-01-22T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 84787740.0000001,
    total_supply: 99785291.1428572,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0x80a7e048f37a50500351c204cb407766fa3bae7f'
    },
    cmc_rank: 95,
    last_updated: '2019-11-14T17:19:07.000Z',
    quote: {
      USD: {
        price: 0.516717907413,
        volume_24h: 327114.003067805,
        percent_change_1h: 0.335653,
        percent_change_24h: -3.66475,
        percent_change_7d: -1.6735,
        market_cap: 43811343.58707757,
        last_updated: '2019-11-14T17:19:07.000Z'
      }
    },
    quote_mapped: {
      price: 0.516717907413,
      volume_24h: 327114.003067805,
      percent_change_1h: 0.335653,
      percent_change_24h: -3.66475,
      percent_change_7d: -1.6735,
      market_cap: 43811343.58707757,
      last_updated: '2019-11-14T17:19:07.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 4113,
    name: 'UNI COIN',
    symbol: 'UNI',
    slug: 'uni-coin',
    num_market_pairs: 1,
    date_added: '2019-07-17T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 12908691.9524922,
    total_supply: 1000000000,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0xe6877ea9c28fbdec631ffbc087956d0023a76bf2'
    },
    cmc_rank: 96,
    last_updated: '2019-11-14T17:19:14.000Z',
    quote: {
      USD: {
        price: 3.34693437722,
        volume_24h: 13293.6498873613,
        percent_change_1h: -0.109523,
        percent_change_24h: 51.3473,
        percent_change_7d: 134.263,
        market_cap: 43204544.860739306,
        last_updated: '2019-11-14T17:19:14.000Z'
      }
    },
    quote_mapped: {
      price: 3.34693437722,
      volume_24h: 13293.6498873613,
      percent_change_1h: -0.109523,
      percent_change_24h: 51.3473,
      percent_change_7d: 134.263,
      market_cap: 43204544.860739306,
      last_updated: '2019-11-14T17:19:14.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 3871,
    name: 'Newton',
    symbol: 'NEW',
    slug: 'newton',
    num_market_pairs: 12,
    date_added: '2019-04-17T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: null,
    circulating_supply: 11948333332,
    total_supply: 100000000000,
    platform: null,
    cmc_rank: 97,
    last_updated: '2019-11-14T17:19:13.000Z',
    quote: {
      USD: {
        price: 0.00357996439448,
        volume_24h: 4279643.015967,
        percent_change_1h: -0.401838,
        percent_change_24h: -1.27445,
        percent_change_7d: -0.645697,
        market_cap: 42774607.90193858,
        last_updated: '2019-11-14T17:19:13.000Z'
      }
    },
    quote_mapped: {
      price: 0.00357996439448,
      volume_24h: 4279643.015967,
      percent_change_1h: -0.401838,
      percent_change_24h: -1.27445,
      percent_change_7d: -0.645697,
      market_cap: 42774607.90193858,
      last_updated: '2019-11-14T17:19:13.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 3617,
    name: 'ILCoin',
    symbol: 'ILC',
    slug: 'ilcoin',
    num_market_pairs: 26,
    date_added: '2018-11-26T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: 2500000000,
    circulating_supply: 333602487.370894,
    total_supply: 1287298907.43547,
    platform: null,
    cmc_rank: 98,
    last_updated: '2019-11-14T17:19:13.000Z',
    quote: {
      USD: {
        price: 0.124003876275,
        volume_24h: 516257.141577553,
        percent_change_1h: -0.629019,
        percent_change_24h: 1.3123,
        percent_change_7d: -3.73013,
        market_cap: 41368001.56897259,
        last_updated: '2019-11-14T17:19:13.000Z'
      }
    },
    quote_mapped: {
      price: 0.124003876275,
      volume_24h: 516257.141577553,
      percent_change_1h: -0.629019,
      percent_change_24h: 1.3123,
      percent_change_7d: -3.73013,
      market_cap: 41368001.56897259,
      last_updated: '2019-11-14T17:19:13.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 1414,
    name: 'Zcoin',
    symbol: 'XZC',
    slug: 'zcoin',
    num_market_pairs: 42,
    date_added: '2016-10-06T00:00:00.000Z',
    tags: [
      'mineable'
    ],
    max_supply: 21400000,
    circulating_supply: 8806168.32977799,
    total_supply: 21400000,
    platform: null,
    cmc_rank: 99,
    last_updated: '2019-11-14T17:19:03.000Z',
    quote: {
      USD: {
        price: 4.64103016906,
        volume_24h: 4136948.79433068,
        percent_change_1h: 0.25189,
        percent_change_24h: -2.9214,
        percent_change_7d: -7.31797,
        market_cap: 40869692.892320365,
        last_updated: '2019-11-14T17:19:03.000Z'
      }
    },
    quote_mapped: {
      price: 4.64103016906,
      volume_24h: 4136948.79433068,
      percent_change_1h: 0.25189,
      percent_change_24h: -2.9214,
      percent_change_7d: -7.31797,
      market_cap: 40869692.892320365,
      last_updated: '2019-11-14T17:19:03.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  },
  {
    id: 2603,
    name: 'Pundi X',
    symbol: 'NPXS',
    slug: 'pundi-x',
    num_market_pairs: 69,
    date_added: '2018-03-22T00:00:00.000Z',
    tags: [],
    max_supply: null,
    circulating_supply: 234162817276.377,
    total_supply: 259810708832.63,
    platform: {
      id: 1027,
      name: 'Ethereum',
      symbol: 'ETH',
      slug: 'ethereum',
      token_address: '0xa15c7ebe1f07caf6bff097d8a589fb8ac49ae5b3'
    },
    cmc_rank: 100,
    last_updated: '2019-11-14T17:19:08.000Z',
    quote: {
      USD: {
        price: 0.000173707576204,
        volume_24h: 1048281.88707193,
        percent_change_1h: -0.325988,
        percent_change_24h: -5.03868,
        percent_change_7d: -8.15014,
        market_cap: 40675855.42617959,
        last_updated: '2019-11-14T17:19:08.000Z'
      }
    },
    quote_mapped: {
      price: 0.000173707576204,
      volume_24h: 1048281.88707193,
      percent_change_1h: -0.325988,
      percent_change_24h: -5.03868,
      percent_change_7d: -8.15014,
      market_cap: 40675855.42617959,
      last_updated: '2019-11-14T17:19:08.000Z',
      currency: 'USD',
      currency_symbol: '$'
    }
  }
];


export const cyptoOneItemWithBitcoin: any = {
  id: 1,
  name: 'Bitcoin',
  symbol: 'BTC',
  slug: 'bitcoin',
  num_market_pairs: 7767,
  date_added: '2013-04-28T00:00:00.000Z',
  tags: [
    'mineable'
  ],
  max_supply: 21000000,
  circulating_supply: 18047212,
  total_supply: 18047212,
  platform: null,
  cmc_rank: 1,
  last_updated: '2019-11-14T17:19:36.000Z',
  quote: {
    USD: {
      price: 8703.34700024,
      volume_24h: 19144788627.865,
      percent_change_1h: -0.000483066,
      percent_change_24h: -1.21921,
      percent_change_7d: -5.87575,
      market_cap: 157071148422.89536,
      last_updated: '2019-11-14T17:19:36.000Z'
    }
  },
  quote_mapped: {
    price: 8703.34700024,
    volume_24h: 19144788627.865,
    percent_change_1h: -0.000483066,
    percent_change_24h: -1.21921,
    percent_change_7d: -5.87575,
    market_cap: 157071148422.89536,
    last_updated: '2019-11-14T17:19:36.000Z',
    currency: 'USD',
    currency_symbol: '$'
  },
  quote_bitcoin: {
    price: 1,
    volume_24h: 7886413803.79585,
    percent_change_1h: -0.118249,
    percent_change_24h: -1.36861,
    percent_change_7d: -0.783408,
    market_cap: 20175100613.45549,
    last_updated: '2019-11-14T17:19:26.000Z',
    currency: 'BTC',
    currency_symbol: 'BTC'
  }
};
export const cryptoItemWithoutBitcoin: any = {
  id: 1,
  name: 'Bitcoin',
  symbol: 'BTC',
  slug: 'bitcoin',
  num_market_pairs: 7767,
  date_added: '2013-04-28T00:00:00.000Z',
  tags: [
    'mineable'
  ],
  max_supply: 21000000,
  circulating_supply: 18047212,
  total_supply: 18047212,
  platform: null,
  cmc_rank: 1,
  last_updated: '2019-11-14T17:19:36.000Z',
  quote: {
    USD: {
      price: 8703.34700024,
      volume_24h: 19144788627.865,
      percent_change_1h: -0.000483066,
      percent_change_24h: -1.21921,
      percent_change_7d: -5.87575,
      market_cap: 157071148422.89536,
      last_updated: '2019-11-14T17:19:36.000Z'
    }
  },
  quote_mapped: {
    price: 8703.34700024,
    volume_24h: 19144788627.865,
    percent_change_1h: -0.000483066,
    percent_change_24h: -1.21921,
    percent_change_7d: -5.87575,
    market_cap: 157071148422.89536,
    last_updated: '2019-11-14T17:19:36.000Z',
    currency: 'USD',
    currency_symbol: '$'
  },
};

export const cryptoItemUnmapped: any = {
  id: 1,
  name: 'Bitcoin',
  symbol: 'BTC',
  slug: 'bitcoin',
  num_market_pairs: 7767,
  date_added: '2013-04-28T00:00:00.000Z',
  tags: [
    'mineable'
  ],
  max_supply: 21000000,
  circulating_supply: 18047212,
  total_supply: 18047212,
  platform: null,
  cmc_rank: 1,
  last_updated: '2019-11-14T17:19:36.000Z',
  quote: {
    USD: {
      price: 8703.34700024,
      volume_24h: 19144788627.865,
      percent_change_1h: -0.000483066,
      percent_change_24h: -1.21921,
      percent_change_7d: -5.87575,
      market_cap: 157071148422.89536,
      last_updated: '2019-11-14T17:19:36.000Z'
    }
  }
};
export const MockData = {
  cryptoRankListData,
  cyptoRankListDataMapped,
  cyptoOneItemWithBitcoin,
  cryptoItemWithoutBitcoin,
  cryptoItemUnmapped
};


