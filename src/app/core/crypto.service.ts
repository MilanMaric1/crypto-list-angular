import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CryptoItem} from '../models/CryptoItem';
import {environment} from '../../environments/environment';
import {flatMap, map, mergeMap} from 'rxjs/operators';
import {FiatCurrency, getSymbol} from '../models/FiatCurrency';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CryptoService {

  convert = 'USD,EUR,CNY';

  constructor(private http: HttpClient) {
  }

  mapCrypto() {
    return this.http
      .post<{ items: CryptoItem[] }>(`${environment.cloudFunctionUrl}`, {
        path: '/v1/cryptocurrency/map',
        params: {
          sort: 'cmc_rank',
          aux: 'status',
          listing_status: 'active,untracked'
        }
      })
      .pipe(map((crypto: any) => crypto.data || []));
  }

  listCrypto(currency: FiatCurrency = FiatCurrency.USD) {
    return this.http
      .post<{ items: CryptoItem[] }>(`${environment.cloudFunctionUrl}`, {
        path: '/v1/cryptocurrency/listings/latest',
        params: {
          start: '1',
          sort: 'market_cap',
          sort_dir: 'desc',
          convert: currency
        }
      })
      .pipe(
        map((crypto: any) => crypto.data || []),
        map((list: any) => list.map(this.mapCryptoListingItem)));
  }

  mapCryptoListingItem(item) {

    const keys = Object
      .keys(item.quote);

    if (!keys.length) {
      return item;
    }

    if (keys.length === 1) {
      const key = keys[0];
      return {
        ...item,
        quote_mapped: {
          ...item.quote[key],
          currency: key,
          currency_symbol: getSymbol(key)
        }
      };
    }

    if (keys.length > 1) {
      //  cannot be a case in basic plan since only one convert item is allowed
      return {
        ...item,
        quote_mapped_array: keys.reduce((arr, key) => {
          arr.push({
            ...item.quote[key],
            currency: key,
            currency_symbol: getSymbol(key)
          });
          return arr;
        }, [])
      };
    }

  }


  private getOneWithPrice(id, currency: FiatCurrency = FiatCurrency.USD): Observable<CryptoItem> {
    return this.http
      .post<{ item: CryptoItem }>(`${environment.cloudFunctionUrl}`, {
        path: '/v1/cryptocurrency/quotes/latest',
        params: {
          id,
          convert: currency
        }
      })
      .pipe(
        map((crypto: any) => crypto.data || {}),
        map((obj: any) => {
          return obj[id + ''] || {};
        }),
        map(this.mapCryptoListingItem));
  }

  getPriceInBitcoin(id) {
    return this.getOneWithPrice(id, FiatCurrency.BTC);
  }

  getOneCrypto(id, currency: FiatCurrency = FiatCurrency.USD): Observable<CryptoItem> {
    return this.getOneWithPrice(id, currency)
      .pipe(
        mergeMap((item) => {
          return this.getPriceInBitcoin(id)
            .pipe(
              map((bitcoinItem) => {
                return {
                  ...item,
                  quote_bitcoin: bitcoinItem.quote_mapped
                };
              }));
        })
      );
  }
}
