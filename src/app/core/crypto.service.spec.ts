import {getTestBed, TestBed} from '@angular/core/testing';

import {CryptoService} from './crypto.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {environment} from '../../environments/environment';
import {cryptoRankListData, cyptoRankListDataMapped, MockData} from './mock-data';

describe('CryptoService', () => {
  let injector: TestBed;
  let httpMock: HttpTestingController;
  let service: CryptoService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CryptoService]
    });
    injector = getTestBed();
    service = injector.get(CryptoService);
    httpMock = injector.get(HttpTestingController);

  });


  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  it('listCrypto should return data', () => {
    service.listCrypto()
      .subscribe((res) => {
        expect(res).toEqual(cyptoRankListDataMapped);
      });


    const reqMock = httpMock.expectOne((req) =>
      req.method === 'POST' && req.url === `${environment.cloudFunctionUrl}`);
    expect(reqMock.request.method).toBe('POST');
    reqMock.flush(cryptoRankListData);
  });

  it('mapCryptoListingItem should map quote', () => {
    const mappedItem = service.mapCryptoListingItem(MockData.cryptoItemUnmapped);
    expect(mappedItem).toBeTruthy();
    expect(mappedItem.quote_mapped).toBeTruthy();
    expect(mappedItem.quote_mapped.currency).toBeTruthy();
  });

});
