import {Inject, Injectable, InjectionToken, Optional} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {asyncScheduler, Observable, of} from 'rxjs';
import {
  catchError,
  debounceTime,
  map,
  skip, skipUntil,
  switchMap, take,
  takeUntil,
} from 'rxjs/operators';
import {SearchCryptoActions} from '../actions';
import {CryptoService} from '../../core/crypto.service';
import {CryptoSearchItem} from '../../models/CryptoSearchItem';


@Injectable()
export class CryptoSearchEffects {

  @Effect()
  map$ = ({debounce = 300, scheduler = asyncScheduler} = {}): Observable<Action> =>
    this.actions$.pipe(
      ofType(SearchCryptoActions.mapCrypto.type),
      debounceTime(debounce, scheduler),
      switchMap(({}) => {
        const nextMap = this.actions$.pipe(
          ofType(SearchCryptoActions.mapCrypto.type),
          skip(100)
        );
        return this.cryptoService.mapCrypto()
          .pipe(
            takeUntil(nextMap),
            map((items: CryptoSearchItem[]) => SearchCryptoActions.cryptoMapActionSuccess({items})),
            catchError(err =>
              of(SearchCryptoActions.cryptoMapActionError({errorMsg: err}))
            )
          );
      })
    );

  constructor(
    private actions$: Actions<SearchCryptoActions.CryptoSearchActions>,
    private cryptoService: CryptoService
  ) {
  }
}
