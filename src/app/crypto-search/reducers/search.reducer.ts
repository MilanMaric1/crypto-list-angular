import {SearchCryptoActions} from '../actions';
import {CryptoSearchItem} from '../../models/CryptoSearchItem';

export interface State {
  loading: boolean;
  error: string;
  query: string;
  items: CryptoSearchItem[];
  filteredItems: CryptoSearchItem[];
}

const initialState: State = {
  loading: false,
  error: '',
  items: [],
  query: '',
  filteredItems: [],
};

export function reducer(
  state = initialState,
  action:
    SearchCryptoActions.CryptoSearchActions
): State {
  switch (action.type) {
    case SearchCryptoActions.searchCrypto.type: {
      const query = action.query.toLowerCase();

      return {
        ...state,
        loading: true,
        filteredItems: state.items.filter((item) => item.name.toLowerCase().search(query) >= 0).slice(0, 10),
        error: '',
        query,
      };
    }

    case SearchCryptoActions.cryptoMapActionSuccess.type: {
      return {
        ...state,
        items: action.items,
        loading: false,
        error: '',
        filteredItems: [],
        query: state.query,
      };
    }

    case SearchCryptoActions.cryptoMapActionError.type: {
      return {
        ...state,
        loading: false,
        filteredItems: [],
        error: action.errorMsg,
      };
    }

    default: {
      return state;
    }
  }
}


export const getQuery = (state: State) => state.query;

export const getLoading = (state: State) => state.loading;

export const getError = (state: State) => state.error;
export const getItems = (state: State) => state.filteredItems;
