import * as fromSearch from './search.reducer';
import * as fromRoot from '../../reducers';
import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';


export interface CryptoSearchState {
  search: fromSearch.State;
}

export interface State extends fromRoot.State {
  cryptoSearch: CryptoSearchState;
}

export const reducers: ActionReducerMap<CryptoSearchState, any> = {
  search: fromSearch.reducer
};

export const getState = createFeatureSelector<State, CryptoSearchState>('cryptoSearch');

export const getSearchState = createSelector(
  getState,
  (state: CryptoSearchState) => state.search
);

export const getSearchResults = createSelector(
  getSearchState,
  fromSearch.getItems
);
export const getSearchQuery = createSelector(
  getSearchState,
  fromSearch.getQuery
);
export const getSearchLoading = createSelector(
  getSearchState,
  fromSearch.getLoading
);
export const getSearchError = createSelector(
  getSearchState,
  fromSearch.getError
);

