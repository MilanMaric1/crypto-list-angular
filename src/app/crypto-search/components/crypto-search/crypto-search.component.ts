import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {CryptoSearchItem} from '../../../models/CryptoSearchItem';

@Component({
  selector: 'app-crypto-search',
  templateUrl: './crypto-search.component.html',
  styleUrls: ['./crypto-search.component.less']
})
export class CryptoSearchComponent {
  @Input() query = '';
  @Input() searching = false;
  @Input() error = '';
  @Input() list: CryptoSearchItem[] = [];
  @Output() search = new EventEmitter<string>();
  @Output() cryptoSelected = new EventEmitter<string>();
}

