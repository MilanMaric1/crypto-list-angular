import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Observable} from 'rxjs';
import {select, Store} from '@ngrx/store';
import * as fromCrypto from '../reducers';
import {take} from 'rxjs/operators';
import {SearchCryptoActions} from '../actions';
import {CryptoSearchItem} from '../../models/CryptoSearchItem';
import {Router} from '@angular/router';

@Component({
  selector: 'app-search-crypto-container',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <app-crypto-search
      [query]="searchQuery$ | async"
      [searching]="loading$ | async"
      [error]="error$ | async"
      (search)="search($event)"
      [list]="list$|async"
      (cryptoSelected)="navigateToDetails($event)">
    </app-crypto-search>`,
})
export class CryptoSearchContainerComponent {
  list$: Observable<CryptoSearchItem[]>;
  searchQuery$: Observable<string>;
  loading$: Observable<boolean>;
  error$: Observable<string>;

  constructor(private store: Store<fromCrypto.State>, private router: Router) {
    this.searchQuery$ = store.pipe(
      select(fromCrypto.getSearchQuery),
      take(1)
    );
    this.list$ = store.pipe(select(fromCrypto.getSearchResults));
    this.loading$ = store.pipe(select(fromCrypto.getSearchLoading));
    this.error$ = store.pipe(select(fromCrypto.getSearchError));
    this.store.dispatch(SearchCryptoActions.mapCrypto({}));
  }

  search(query: string) {
    this.store.dispatch(SearchCryptoActions.searchCrypto({query}));
  }

  navigateToDetails(id: string) {
    this.router.navigate(['/crypto', id]);
    this.store.dispatch(SearchCryptoActions.searchCrypto({query: ''}));
  }
}
