import {createAction, props, union} from '@ngrx/store';
import {CryptoSearchItem} from '../../models/CryptoSearchItem';

export const searchCrypto = createAction(
  '[Search] Search Crypto',
  props<{ query: string }>()
);


export const mapCrypto = createAction(
  '[Search] Map Crypto',
  props<{}>()
);

export const cryptoMapActionSuccess = createAction(
  '[Search] Map Success',
  props<{ items: CryptoSearchItem[] }>()
);

export const cryptoMapActionError = createAction(
  '[Search] Map Failure',
  props<{ errorMsg: string }>()
);


const all = union({cryptoMapActionSuccess, cryptoMapActionError, searchCrypto, mapCrypto});

export type CryptoSearchActions = typeof all;
