import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {CryptoSearchEffects} from './effects/crypto-search.effects';
import {CryptoSearchContainerComponent} from './components/crypto-search-container.component';
import {CryptoSearchComponent} from './components/crypto-search/crypto-search.component';
import {reducers} from './reducers';


@NgModule({
  declarations: [
    CryptoSearchContainerComponent,
    CryptoSearchComponent
  ],
  imports: [
    CommonModule,
    StoreModule.forFeature('cryptoSearch', reducers),
    EffectsModule.forFeature([CryptoSearchEffects]),
    NgZorroAntdModule
  ],
  exports: [
    CryptoSearchContainerComponent
  ]
})
export class CryptoSearchModule {
}
