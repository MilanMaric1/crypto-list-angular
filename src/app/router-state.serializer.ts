import {RouterStateSerializer} from '@ngrx/router-store';
import {Params, RouterStateSnapshot} from '@angular/router';

export interface IRouterStateUrl {
  url: string;
  params: any;
  queryParams: any;
}

export class CustomSerializer implements RouterStateSerializer<IRouterStateUrl> {

  serialize(routerState: RouterStateSnapshot): IRouterStateUrl {
    const {url} = routerState;
    const queryParams = routerState.root.queryParams;

    let route = routerState.root;
    while (route.firstChild) {
      route = route.firstChild;
    }

    const params = route.params;
    return {url, params, queryParams};
  }
}
