import {Inject, Injectable, InjectionToken, Optional} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action, Store} from '@ngrx/store';
import {asyncScheduler, EMPTY as empty, Observable, of} from 'rxjs';
import {
  catchError,
  debounceTime, filter,
  map,
  skip,
  switchMap,
  takeUntil, withLatestFrom,
} from 'rxjs/operators';
import {CryptoApiActions, RankListActions} from '../crypto/actions';
import {CryptoItem} from '../models/CryptoItem';
import {CryptoService} from '../core/crypto.service';
import {SettingsActions} from '../actions';
import * as fromRoot from '../reducers';


@Injectable()
export class CryptoRankEffects {
  @Effect()
  search$ = ({debounce = 300, scheduler = asyncScheduler} = {}): Observable<Action> => {
    return this.actions$.pipe(
      filter((action) => action.type === RankListActions.listCrypto.type || action.type === SettingsActions.setCurrency.type),
      debounceTime(debounce, scheduler),
      withLatestFrom(this.store$.select(fromRoot.getCurrency)),
      switchMap(([{}, currency]) => {
        return this.cryptoService
          .listCrypto(currency)
          .pipe(
            map((items: CryptoItem[]) => CryptoApiActions.rankListSuccess({items})),
            catchError(err =>
              of(CryptoApiActions.rankListError({errorMsg: err}))
            )
          );
      })
    );
  };


  constructor(
    private actions$: Actions<RankListActions.CryptoRankListActions>,
    private cryptoService: CryptoService,
    private store$: Store<fromRoot.State>
  ) {
  }
}
